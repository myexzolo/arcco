<div class="loading none">
  <img src="../../images/logo.png">
</div>

<footer>

  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="box-col">
          <h3>ศูนย์ช่วยเหลือ</h3>
          <ul>
            <?php
              $sql    = "SELECT * FROM t_about_us WHERE id NOT IN(1,4) ORDER BY id ASC";
              $query  = DbQuery($sql,null);
              $json   = json_decode($query, true);
              if($json['dataCount'] > 0){
                foreach ($json['data'] as $value) {
            ?>
           <li><a href="../about/?id=<?=$value['id']?>"><?=$value['title_name']?></a></li>
           <?php }} ?>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="box-col">
          <h3>เกี่ยวกับเรา</h3>
          <ul>
            <?php
              $sql    = "SELECT * FROM t_about_us WHERE id IN(1) ORDER BY id ASC";
              $query  = DbQuery($sql,null);
              $json   = json_decode($query, true);
              if($json['dataCount'] > 0){
                foreach ($json['data'] as $value) {
            ?>
           <li><a href="../about/?id=<?=$value['id']?>"><?=$value['title_name']?></a></li>
           <?php }} ?>
           <li><a href="../service/">บริการ</a></li>
           <li><a href="../job_list/">ประกาศงาน</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="box-col">
          <h3>สินค้า/บริการ</h3>
          <ul>
            <?php
              $sql   = "SELECT * FROM t_category";
              $query = DbQuery($sql,null);
              $row   = json_decode($query,true);
              if($row['dataCount'] > 0){
                foreach ($row['data'] as $value) {
            ?>
            <li><a href="../shop/?category_id=<?=$value['category_id']?>"><?=$value['category_name_th']?></a></li>
            <?php }} ?>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="box-col">
          <h3>ติดต่อเรา</h3>
          <ul>
           <li><a onclick="loginModel()" href="javascript:void(0)">สมัครสมาชิก</a></li>
           <li><a href="../contact/">ติดต่อเรา</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="footer">
    <p>Arcco © Copyrights <?=date('Y')?> Design by ARCCO-TH</p>
  </div>
</footer>
