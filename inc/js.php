<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js?v=<?=rand();?>"></script>
<script src="../../js/smoke.js?v=<?=rand();?>"></script>
<script src="../../js/lightbox.min.js?v=<?=rand();?>"></script>
<script src="../../js/owl.carousel.min.js?v=<?=rand();?>"></script>
<script src="../../admin/dist/js/select2.min.js?v=<?=rand();?>"></script>
<script src="../../js/main.js?v=<?=rand();?>"></script>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v9.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="738928516523322"
  theme_color="#ff9801"
  logged_in_greeting="สวัสดีจ้าาา"
  logged_out_greeting="สวัสดีจ้าาา">
</div>

<?php
  if(isset($_GET['actFB'])){
    $status = $_GET['actFB']=="success"?"success":"danger";
?>
  <script type="text/javascript">
      $.smkAlert({text: "<?=$_GET['actFB']?>",type: "<?=$status?>"});
  </script>
<?php } ?>
