<?php
  include('../../admin/inc/function/connect.php');
?>
<div class="col-xs-12">
  <div class="form-group">
    <label>หมวดหมู่</label>
    <select class="form-control" onchange="checkTypeImage(this.value)" name="category_id" required>
      <?php
        $sql   = "SELECT * FROM t_category";
        $query = DbQuery($sql,null);
        $row   = json_decode($query,true);
        if($row['dataCount'] > 0){
          foreach ($row['data'] as $value) {
      ?>
      <option value="<?=$value['category_id']?>"><?=$value['category_name_th']?></option>
      <?php }} ?>
    </select>
  </div>
</div>
<div class="col-xs-12">
  <div class="form-group">
    <label>วุฒิการศึกษา</label>
    <select class="form-control" name="edu_id" required>
      <?php
        $sql   = "SELECT * FROM t_education";
        $query = DbQuery($sql,null);
        $row   = json_decode($query,true);
        if($row['dataCount'] > 0){
          foreach ($row['data'] as $value) {
      ?>
      <option value="<?=$value['edu_id']?>"><?=$value['edu_name']?></option>
      <?php }} ?>
    </select>
  </div>
</div>
<div class="tu_img">
  <div class="col-xs-12">
    <div class="form-group">
      <label>อัพโหลดใบประกอบวิชาชีพ</label>
      <input type="file" class="form-control" name="tu_img" required>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <label>อัพโหลดอแนบรูปถ่ายตนเอง</label>
      <input type="file" class="form-control" name="tu_Meimg" required>
    </div>
  </div>
</div>
<div class="col-xs-12">
  <div class="form-group">
    <label>อัพโหลดบัตรประชาชน</label>
    <input type="file" class="form-control" name="tu_IDCardimg" required>
  </div>
</div>
<div class="col-xs-12">
  <div class="form-group">
    <label>รายละเอียดข้อมูล</label>
    <textarea class="form-control" name="tu_detail" rows="3"></textarea>
  </div>
</div>
