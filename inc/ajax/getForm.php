<?php
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

include('../../admin/inc/function/mainFunc.php');
include('../../admin/inc/function/connect.php');

require_once('../../lib/Facebook/autoload.php');
require_once('../config/FB.php');

$type = $_POST['type'];

$redirectTo = "https://arcco-th.com/inc/service/loginFB.php";
$data = ['email'];
$fullURL = $handler->getLoginUrl($redirectTo, $data);


?>
<input type="hidden" id="type" name="type" value="<?=$type?>">
<?php if($type == 'login'){ ?>
<div class="col-xs-12">
  <div class="btn-bottom text-right btn-0">
    <button type="button" onclick="loginFB('<?=$fullURL?>')" class="btn btn-default btn-block btn-facebook">เข้าสู่ระบบด้วย facebook</button>
  </div>

  <div class="box-hr">
    <span>หรือ</span>
    <hr>
  </div>


</div>
<div class="col-xs-12">
  <div class="form-group">
    <input type="text" name="mem_username" class="form-control input-lg" placeholder="รหัสผู้ใช้งาน" required>
  </div>
</div>
<div class="col-xs-12">
  <div class="form-group">
    <input type="password" name="mem_password" class="form-control input-lg" placeholder="รหัสผ่าน" required>
  </div>
</div>
<div class="col-xs-12">
  <div class="btn-bottom text-right btn-0">
    <button type="submit" class="btn btn-default btn-block">เข้าสู่ระบบ</button>
    <a class="register forgot" onclick="getFrom('forgotpassword')" href="javascript:void(0)">ลืมรหัสผ่าน?</a>
  </div>
</div>
<?php }elseif($type == 'register'){ ?>
  <div class="col-xs-12">
    <div class="form-group">
      <input type="text" name="mem_username" class="form-control input-lg" placeholder="รหัสผู้ใช้งาน" required>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <input type="password" name="mem_password" id="password" class="form-control input-lg" placeholder="รหัสผ่าน" required>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <input type="password" id="conPassword" class="form-control input-lg" placeholder="ยืนยันรหัสผ่าน" required>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <input type="text" name="mem_fname" class="form-control input-lg" placeholder="ชื่อ" required>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <input type="text" name="mem_lname" class="form-control input-lg" placeholder="นามสกุล" required>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <input type="email" name="mem_email" class="form-control input-lg" placeholder="อีเมล์" required>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <input type="tel" name="mem_mobile" class="form-control input-lg" placeholder="โทรศัพท์มือถือ" required>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="checkType" value="1" onchange="checkTypeRegister(this)"> สมัครเป็นผู้รับงาน
        </label>
      </div>
    </div>
  </div>
  <div id="formType"></div>
  <div class="col-xs-12">
    <div class="btn-bottom text-right">
      <button type="submit" class="btn btn-default btn-block">ลงทะเบียน</button>
    </div>
  </div>
<?php }elseif($type == 'forgotpassword'){ ?>

  <div class="col-xs-12">
    <div class="form-group">
      <input type="email" name="mem_email" class="form-control input-lg" placeholder="อีเมล์" required>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="btn-bottom text-right">
      <button type="submit" class="btn btn-default btn-block">ขอรหัสผ่านใหม่</button>
    </div>
  </div>
<?php } ?>
