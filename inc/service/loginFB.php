<?php
require_once('../../lib/Facebook/autoload.php');
require_once('../config/FB.php');
include('../../admin/inc/function/mainFunc.php');
include('../../admin/inc/function/connect.php');

try {
    $accessToken = $handler->getAccessToken();
}catch(\Facebook\Exceptions\FacebookResponseException $e){
    // header('Content-Type: application/json');
    exit("<script>window.location='../../page/home/?actFB=Response_Exception:';</script>");

}catch(\Facebook\Exceptions\FacebookSDKException $e){
    // header('Content-Type: application/json');
    exit("<script>window.location='../../page/home/?actFB=SDK_Exception:';</script>");
}
if(!$accessToken){
  // header('Content-Type: application/json');
  exit("<script>window.location='../../page/home/?actFB=accessToken_Fail';</script>");
}
$oAuth2Client = $FBObject->getOAuth2Client();
if(!$accessToken->isLongLived())
    $accessToken = $oAuth2Client->getLongLivedAccesToken($accessToken);

    $response = $FBObject->get("/me?fields=id, first_name, last_name, email, picture.type(large)", $accessToken);
    $userData = $response->getGraphNode()->asArray();

    $arr['mem_fname'] = $userData['first_name'];
    $arr['mem_lname'] = $userData['last_name'];
    $arr['mem_email'] = $userData['email'];
    $arr['mem_type_register'] = 'FB';

    $sql   = "SELECT * FROM t_member WHERE mem_email = '{$arr['mem_email']}'";
    $query = DbQuery($sql,null);
    $row   = json_decode($query, true);
    if($row['dataCount'] == 1){
      $_SESSION['member'] = $row['data'][0];

      // header('Content-Type: application/json');
      exit("<script>window.location='../../page/home/?actFB=success';</script>");

    }else{

      $sql = DBInsertPOST($arr,'t_member');
      $query      = DbQuery($sql,null);
      $row        = json_decode($query, true);
      $errorInfo  = $row['errorInfo'];
      if(intval($row['errorInfo'][0]) == 0){
        $sql   = "SELECT * FROM t_member WHERE mem_email = '{$arr['mem_email']}'";
        $query = DbQuery($sql,null);
        $row   = json_decode($query, true);
        $_SESSION['member'] = $row['data'][0];
        // header('Content-Type: application/json');
        exit("<script>window.location='../../page/home/?actFB=success';</script>");
      }else{
        // header('Content-Type: application/json');
        exit("<script>window.location='../../page/home/?actFB=fail';</script>");
      }
    }


    // $_SESSION['userData'] = $userData;
    // $_SESSION['access_token'] = (string) $accessToken;
    // header('Location: index.php');
?>
