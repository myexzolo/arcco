<?php

include('../../admin/inc/function/mainFunc.php');
include('../../admin/inc/function/connect.php');
require_once '../../admin/PHPMailer/PHPMailerAutoload.php';
include('../../lib/line/notify.php');

$type = @$_POST['type'];
unset($_POST['type']);
if($type == 'login'){

  $mem_password = md5($_POST['mem_password']);
  $sql   = "SELECT * FROM t_member WHERE mem_username = '{$_POST['mem_username']}' AND mem_password = '$mem_password'";
  $query = DbQuery($sql,null);
  $row   = json_decode($query, true);
  if($row['dataCount'] == 1){
    $_SESSION['member'] = $row['data'][0];
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'username or password not macth')));
  }

}elseif($type == 'register'){

  $sql   = "SELECT mem_username FROM t_member WHERE mem_username = '{$_POST['mem_username']}' OR mem_email = '{$_POST['mem_email']}'";
  $query = DbQuery($sql,null);
  $row   = json_decode($query, true);
  if($row['dataCount'] > 0){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'username or email is used.')));
  }else{

    if(@$_POST['checkType'] == 1){
      $tu_img               = @$_FILES['tu_img'];
      $tu_IDCardimg         = @$_FILES['tu_IDCardimg'];
      $tu_Meimg             = @$_FILES['tu_Meimg'];
      $checkType            = $_POST['checkType'];
      $arr_2['tu_detail']   = $_POST['tu_detail'];
      $arr_2['category_id'] = $_POST['category_id'];

      if($tu_img['error'] == 0){
        $arr_2['tu_img'] = uploadfile($tu_img,'../../images/upgrade','upgrade_')['image'];
      }
      if($tu_IDCardimg['error'] == 0){
        $arr_2['tu_IDCardimg'] = uploadfile($tu_IDCardimg,'../../images/upgrade','upgradeIDCard_')['image'];
      }
      if($tu_Meimg['error'] == 0){
        $arr_2['tu_Meimg'] = uploadfile($tu_Meimg,'../../images/upgrade','upgradeMe_')['image'];
      }
      unset($_POST['checkType']);
      unset($_POST['tu_detail']);
      unset($_POST['category_id']);
      unset($_POST['tu_img']);
      unset($_POST['tu_IDCardimg']);
      unset($_POST['tu_Meimg']);
    }

    $_POST['mem_password'] = md5($_POST['mem_password']);
    $sql = DBInsertPOST($_POST,'t_member');
    $query      = DbQuery($sql,null);
    $row        = json_decode($query, true);
    $errorInfo  = $row['errorInfo'];
    if(intval($row['errorInfo'][0]) == 0){

      $sql   = "SELECT * FROM t_member WHERE mem_username = '{$_POST['mem_username']}' AND mem_password = '{$_POST['mem_password']}'";
      $query = DbQuery($sql,null);
      $row   = json_decode($query, true);
      $_SESSION['member'] = $row['data'][0];

      if(@$checkType == 1){
        $arr_2['mem_id']  = $row['data'][0]['mem_id'];
        $sql              = DBInsertPOST($arr_2,'t_upgrade_user');
        $query            = DbQuery($sql,null);
        $row              = json_decode($query, true);
        $errorInfo        = $row['errorInfo'];
        if(intval($row['errorInfo'][0]) == 0){

          $fullname   = $_SESSION['member']['mem_fname'].' '.$_SESSION['member']['mem_lname'];
          $mem_mobile = $_SESSION['member']['mem_mobile'];
          $mem_email  = $_SESSION['member']['mem_email'];

          $sql    = "SELECT * FROM t_category WHERE category_id = '{$arr_2['category_id']}'";
          $query  = DbQuery($sql,null);
          $row    = json_decode($query, true)['data'][0];
          $category_name_th = $row['category_name_th'];
          $message_data['message'] = "\r\nแจ้งอัพเกรด User\r\nเป็น $category_name_th\r\n----------------\r\nข้อมูลติดต่อ\r\n$fullname\r\n$mem_mobile\r\n$mem_email\r\n----------------\r\nตรวจสอบข้อมูล\r\nhttps://arcco-th.com/admin/pages/upgrade_user/";
          if(send_notify_message($message_data)['status'] == 200){
            header('Content-Type: application/json');
            exit(json_encode(array('status' => 'success','message' => 'success')));
          }else{
            header('Content-Type: application/json');
            exit(json_encode(array('status' => 'success','message' => 'success')));
          }

        }
      }

      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'success','message' => 'success')));
    }else{
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'danger','message' => 'Fail')));
    }

  }

}elseif($type == 'forgotpassword'){
  $sql   = "SELECT mem_id,mem_email, DATE_ADD(NOW(), INTERVAL 1 HOUR) AS DATE_EXP FROM t_member WHERE mem_email = '{$_POST['mem_email']}'";
  $query = DbQuery($sql,null);
  $row   = json_decode($query, true);
  if($row['dataCount'] == 1){
    $row = $row['data'][0];
    $randomString = password_hash(randomString(16,5), PASSWORD_DEFAULT);
    $arr['email'] = $_POST['mem_email'];
    $arr['title'] = '[ARCCO] Password reset fot ARCCO';
    $arr['message'] = '
    <p>Hello, This email has been send to you so you can reset your login accout password<br />
Plesae click the link below to complete the password reset process. This link will expire in 1 hour.</p>
<p><a href="https://arcco.onesittichok.co.th/authensupport/resetpassword/?tokenaccess='.$randomString.'">https://arcco.onesittichok.co.th/authensupport/resetpassword/?tokenaccess='.$randomString.'</a></p>
<p>Received thie message by arcco.<br />
You may have received this email in error<br />
because another customer entered this email address&nbsp; by&nbsp;arcco.<br />
if you&nbsp;received this message by&nbsp;arcco. please delete&nbsp; this email</p>
<p>________________</p>
<p>arcco Team.<br />
________________</p>
    ';

    if(mailsendMail($arr) == 200){
      $sql = "INSERT INTO log_forgotpassword(mem_id, mem_email, log_token,date_exp) VALUES ('{$row['mem_id']}','{$row['mem_email']}','$randomString','{$row['DATE_EXP']}')";
      $query      = DbQuery($sql,null);
      $row        = json_decode($query, true);
      $errorInfo  = $row['errorInfo'];
      if(intval($row['errorInfo'][0]) == 0){
        header('Content-Type: application/json');
        exit(json_encode(array('status' => 'success','message' => 'success')));
      }else{
        header('Content-Type: application/json');
        exit(json_encode(array('status' => 'danger','message' => "can't college log")));
      }
    }else{
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'danger','message' => 'Send mail Fail')));
    }
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }

}





?>
