<?php

function getActive($path){
  $onlyPath = str_replace($_SERVER['DOCUMENT_ROOT'], '', dirname($_SERVER['PHP_SELF']));
  return strpos($onlyPath,"page/$path")!==false?"active":"";
}

?>
<!-- <div class="header_top">
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <div class="full">
          <div class="topbar-left">
            <ul class="list-inline">
              <li> <span class="topbar-label"><i class="fa  fa-home"></i></span> <span class="topbar-hightlight">540 Lorem Ipsum New York, AB 90218</span> </li>
              <li> <span class="topbar-label"><i class="fa fa-envelope-o"></i></span> <span class="topbar-hightlight"><a href="mailto:info@yourdomain.com">info@yourdomain.com</a></span> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-sm-4 right_section_header_top">
        <div class="float-left">
          <div class="social_icon">
            <ul class="list-inline">
              <li><a class="fa fa-facebook" href="https://www.facebook.com/" title="Facebook" target="_blank"></a></li>
              <li><a class="fa fa-instagram" href="https://www.instagram.com" title="Instagram" target="_blank"></a></li>
            </ul>
          </div>
        </div>
        <div class="float-right member">
          <a class="white_btn" onclick="loginModel()" href="javascript:void(0)">sign up free</a>
        </div>
      </div>
    </div>
  </div>
</div> -->

<nav class="navbar navbar-default nav-menu navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="../../page/home/">
        <img class="logo" src="../../images/logo.png" alt="logo">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right ul-menu">
        <li class="<?=getActive("home")?>"><a href="../../page/home/">หน้าแรก</a></li>
        <li class="dropdown <?=getActive("about")?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">เกี่ยวกับเรา <span class="caret"></span></a>
          <ul class="dropdown-menu">
             <?php
               $sql    = "SELECT * FROM t_about_us WHERE id NOT IN(4) ORDER BY id ASC";
               $query  = DbQuery($sql,null);
               $json   = json_decode($query, true);
               if($json['dataCount'] > 0){
                 foreach ($json['data'] as $value) {
             ?>
            <li><a href="../about/?id=<?=$value['id']?>"><?=$value['title_name']?></a></li>
            <?php }} ?>
          </ul>
        </li>
        <li class="<?=getActive("service")?>"><a href="../../page/service/">บริการ</a></li>
        <li class="<?=getActive("job_list")?>"><a href="../../page/job_list/">ประกาศงาน</a></li>
        <li class="dropdown <?=getActive("shop")?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">สินค้า/บริการ <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php
              $sql   = "SELECT * FROM t_category";
              $query = DbQuery($sql,null);
              $row   = json_decode($query,true);
              if($row['dataCount'] > 0){
                foreach ($row['data'] as $value) {
            ?>
            <li><a href="../shop/?category_id=<?=$value['category_id']?>"><?=$value['category_name_th']?></a></li>
            <?php }} ?>
          </ul>
        </li>
        <li class="<?=getActive("checkout")?>"><a href="../../page/checkout/">checkout</a></li>

        <li class="<?=getActive("contact")?>"><a href="../../page/contact/">ติดต่อเรา</a></li>
        <li class="<?=getActive("search")?>">
          <a href="../search/"><span class="glyphicon glyphicon-search"></span></a>
        </li>
        <?php if(!isset($_SESSION['member'])){ ?>
        <li>
          <a onclick="loginModel()" href="javascript:void(0)">sign up & login</a>
        </li>
        <?php }else{ ?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">welcome <?=@$_SESSION['member']['mem_fname'].' '.@$_SESSION['member']['mem_lname']?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../project_detail/">profile</a></li>
            <li><a href="javascript:void(0)" onclick="logout('../../page/home/')">ออกจากระบบ</a></li>
          </ul>
        </li>
        <?php } ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div>
</nav>

<!-- Modal -->
<div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12">
            <div class="blog-form">

              <div class="header-login">
                <p class="p-form" onclick="getFrom('login')">เข้าสู่ระบบ</p>
                <p class="p-form active" onclick="getFrom('register')">ลงทะเบียน</p>
              </div>

              <div class="title-form">
                <p>
                  สำหรับ สถาปนิก วิศวกร และผู้รับเหมาทั้ง Freelance และบริษัทสามารถเปิดเพจและฝากผลงานของคุณได้ฟรี เพื่อเข้าถึงลูกค้าที่มากขึ้น
                </p>
              </div>
                <form class="formLogin" id="formLogin" novalidate></form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="mar-top"></div>
<!-- <div class="fb-customerchat"
 page_id="324140261309729">
</div> -->
