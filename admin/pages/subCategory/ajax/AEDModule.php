<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action               = @$_POST["action"];
$sub_category_id      = @$_POST["sub_category_id"];
$sub_category_name_th = @$_POST["sub_category_name_th"];
$sub_category_name_en = @$_POST["sub_category_name_en"];
$category_id          = @$_POST["category_id"];
$is_active            = @$_POST["is_active"];
$sub_category_path    = @$_FILES['sub_category_path'];

$pathUpload       = "../../../../images/subService";

$_POST['user_id'] = $_SESSION['member'][0]['user_id'];


  if(empty($id) && $action == "ADD"){

    unset($_POST["action"]);
    unset($_POST["sub_category_id"]);

    $_POST['category_id'] = @implode(",",$category_id);

    $c_img = uploadfile($sub_category_path,$pathUpload,"s_");
    $_POST['sub_category_path'] = $c_img['image'];

    $sql = DBInsertPOST($_POST,'t_sub_category');

  }else if($action == "EDIT"){
    unset($_POST["action"]);

    $_POST['category_id'] = @implode(",",$category_id);

    if($sub_category_path['error'] == 0){
      $sql = "SELECT sub_category_path FROM t_sub_category WHERE sub_category_id = '$sub_category_id'";
      $query   = DbQuery($sql,null);
      $row     = json_decode($query,true)['data'][0];
      @unlink($pathUpload."/".$row['sub_category_path']);
      $c_img = uploadfile($sub_category_path,$pathUpload,"s_");
      $_POST['sub_category_path'] = $c_img['image'];
    }else{
      unset($_POST["sub_category_path"]);
    }
    $sql = DBUpdatePOST($_POST,'t_sub_category','sub_category_id');
  }else if($action == "DEL")
  {
    $arr = array();
    $arr["is_active"] = 'D';
    $arr["sub_category_id"] = $sub_category_id;

    $sql = DBUpdatePOST($arr,'t_sub_category','sub_category_id');
  }

  //echo $sql;
  $query      = DbQuery($sql,null);
  $row        = json_decode($query, true);
  $errorInfo  = $row['errorInfo'];

  if(intval($row['errorInfo'][0]) == 0){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }


?>
