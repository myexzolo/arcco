<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$category_id = @$_POST['category_id'];

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>ลำดับ</th>
      <th>รูป</th>
      <th>ชื่อ ภาษาไทย</th>
      <th>ชื่อ ภาษาอังกฤษ</th>
      <th>หมวดหมู่</th>
      <th>สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
        <th style="width:70px;">Edit</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
        <th style="width:70px;">Del</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $con = "";
      if($category_id != ""){
        $con = " and FIND_IN_SET($category_id, category_id) > 0 ";
      }
      $sqls   = "SELECT * FROM t_sub_category where is_active <> 'D' $con ORDER BY date_update DESC";
      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];
    if($dataCount > 0){
    foreach ($rows as $key => $value) {
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><img class="img-item" src="../../../images/subService/<?=$value['sub_category_path'];?>" onerror="this.onerror='';this.src='../../image/no-image.jpg'" style="height:120px;"></td>
      <td><?=$value['sub_category_name_th'];?></td>
      <td><?=$value['sub_category_name_en'];?></td>
      <td>
        <?php
          $sqlb = "SELECT category_name_th FROM t_category WHERE category_id IN({$value['category_id']})";
          $queryb = DbQuery($sqlb,null);
          $jsonb   = json_decode($queryb, true);
          $countData = $jsonb['dataCount'];
          $rows     = $jsonb['data'];
          //echo $sqlb;
           $new_arr = array();
            foreach ($jsonb['data'] as $valueb) {
              $new_arr[] = $valueb['category_name_th'];
            }
            echo implode(", ",$new_arr);
        ?>
      </td>
      <td><?=$value['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['sub_category_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delModule('<?=$value['sub_category_id']?>','<?=$value['sub_category_name_th']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php } } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
