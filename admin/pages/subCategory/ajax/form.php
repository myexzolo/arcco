<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action       = @$_POST['action'];
$sub_category_id  = @$_POST['sub_category_id'];
$sub_category_name_th  = "";
$sub_category_name_en  = "";
$sub_category_path  = "";
$category_id = array();

$required = "required";

if($action == "EDIT")
{
  $required = "";

  $sqls   = "SELECT * FROM t_sub_category where sub_category_id = '$sub_category_id'";
  $querys = DbQuery($sqls,null);
  $json   = json_decode($querys, true);
  $counts = $json['dataCount'];
  $rows   = $json['data'];

  if($counts > 0){
    $sub_category_name_th  = $rows[0]['sub_category_name_th'];
    $sub_category_name_en  = $rows[0]['sub_category_name_en'];
    $sub_category_path    = $rows[0]['sub_category_path'];
    $category_id          = $rows[0]['category_id'];

    $category_id          = explode(",",$category_id);
  }
}

?>
<input type="hidden" id="action" name="action" value="<?= $action ?>">
<input type="hidden" name="sub_category_id" value="<?=$sub_category_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>ชื่อหมวดหมู่ย่อย ภาษาไทย</label>
        <input value="<?=$sub_category_name_th?>" name="sub_category_name_th" type="text" class="form-control" placeholder="Name TH" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>ชื่อหมวดหมู่ย่อย ภาษาอังกฤษ</label>
        <input value="<?=$sub_category_name_en?>" name="sub_category_name_en" type="text" class="form-control" placeholder="Name EN" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <label>หมวดหมู่</label>
        <select class="form-control select2" id="category_id" name="category_id[]" required multiple style="width:100%;">
          <?php
            $sqlc     = "SELECT * FROM t_category WHERE is_active = 'Y'";
            $queryc   = DbQuery($sqlc,null);
            $jsonc    = json_decode($queryc, true);
            $counts   = $jsonc['dataCount'];
            $rowsc    = $jsonc['data'];

            for($x=0; $x < $counts ; $x++)
            {
          ?>
          <option value="<?=$rowsc[$x]['category_id']?>" <?=in_array($rowsc[$x]['category_id'], $category_id)?"selected":""?>>
            <?=$rowsc[$x]['category_name_th']?>
          </option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Image</label>
        <input name="sub_category_path" onchange="readURL(this,'p_img')" type="file" class="form-control custom-file-input" <?=$required ?>>
      </div>
    </div>
    <div class="col-md-6">
      <div id="p_img">
        <?php if($action == 'EDIT'){ ?>
        <img width="100" src="../../../images/subService/<?=$sub_category_path?>" onerror="this.onerror='';this.src='../../image/no-image.jpg'">
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
<?php if($action != "SHOW"){ ?>
<button type="submit" style="width:100px;" class="btn btn-primary btn-flat">บันทึก</button>
<?php } ?>
</div>

<script>
  $('.select2').select2();
</script>
