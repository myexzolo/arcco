<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action         = @$_POST['value'];
$article_id     = @$_POST['id'];

$title        = "";
$content      = "";
$type_user    = "";
$is_active    = "";
// $article_img  = "";
$required = "required";
if($action == 'EDIT'){
  $btn = 'Update changes';
  $required = "";
  $sqls   = "SELECT * FROM  t_article WHERE article_id = '$article_id'";
  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];

  $title        = $rows[0]['title'];
  $content      = $rows[0]['content'];
  $type_user    = $rows[0]['type_user'];
  // $article_img  = $rows[0]['article_img'];
  $is_active    = $rows[0]['is_active'];


}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" id="action" name="action" value="<?= $action ?>">
<input type="hidden" id="article_id" name="article_id" value="<?= $article_id ?>">
<div class="modal-body" style="min-height:60vh;">
  <div class="row">
    <div class="col-md-7">
      <div class="form-group">
        <label>บทนำ</label>
        <input value="<?=$title?>" id="title" name="title" type="text" class="form-control" placeholder="" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ประเภทสมาชิก</label>
        <select name="type_user" id="type_user" class="form-control select2" style="width: 100%;" required>
          <option value=""></option>
          <option value="1" <?=$type_user ==1?"selected":""; ?>>ผู้ว่าจ้าง (Member)</option>
          <option value="2" <?=$type_user ==2?"selected":""; ?>>ผู้รับจ้าง (MemberJob)</option>
          <option value="3" <?=$type_user ==3?"selected":""; ?>>ผู้ว่าจ้าง (Admin)</option>
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" id="is_active" class="form-control" style="width: 100%;" required>
          <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
          <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>รายละเอียด</label>
        <div id="editor"><?=@$content?></div>
        <input type="hidden" id="content" name="content">
      </div>
    </div>
    <!-- <div class="col-md-6">
      <div class="form-group">
        <label>ภาพหน้าปก</label>
        <input name="article_img" onchange="readURL(this,'p_img')" type="file" class="form-control custom-file-input" >
      </div>
    </div>
    <div class="col-md-3">
      <div id="p_img">

        <img width="100" src="../../../images/article/" onerror="this.onerror='';this.src='../../image/no-image.jpg'">

      </div>
    </div> -->


    <div class="col-md-12">
      <!-- <div id="myDropzone" class="dropzone"></div> -->
      <label>Slideshow</label>
      <form action="ajax/AED.php" class="dropzone" id="myDropzone">
        <input type="hidden" id="id" name="id" value="">
      </form>
    </div>
   </div>
    <?php
      if($action == 'EDIT')
      {
        $sqls   = "SELECT * FROM t_article_gallery WHERE article_id = '$article_id' ORDER BY date_create DESC";
        $query      = DbQuery($sqls,null);
        $json       = json_decode($query, true);
        $rows       = $json['data'];
        $dataCount  = $json['dataCount'];

        if($dataCount > 0){
     ?>
     <br>
     <div class="row">
     <?php

          foreach ($rows as $value) {
            $url = "../../../images/article/".$value['image'];

    ?>
    <div class="col-md-2">
      <a href="<?=$url?>" data-lightbox="roadtrip">
        <div class="img-list" style="background-image: url(<?= $url ?>);"></div>
      </a>
      <label class="toggle" style="position: absolute;top: 0px;left: 15px;">
        <input class="toggle__input" name="remove[]" value="<?=$value['id']?>" type="checkbox">
        <span class="toggle__label">
          <span class="toggle__text"></span>
        </span>
      </label>
    </div>
    <?php } } } ?>

  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;"  data-dismiss="modal">ปิด</button>
<?php if($action != "SHOW"){ ?>
<button type="button" id="submit-all" style="width:100px;"  class="btn btn-primary btn-flat">บันทึก</button>
<?php } ?>
</div>


<script type="text/javascript">
$(function () {
  $('.select2').select2();
  init();
})
// Dropzone.autoDiscover = false;
var id = $('#id').val();
var myDropzone = new Dropzone("#myDropzone",
  {
    autoProcessQueue: false,
    addRemoveLinks:true,
    thumbnailWidth: 100,
    thumbnailHeight: 100,
    parallelUploads: 100,
    acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    dictDefaultMessage: "ลากและวางไฟล์รูปภาพ",
    init: function(){
       var submitButton = document.querySelector('#submit-all');
       var myDropzone = this;

       submitButton.addEventListener("click", function(){
          var arr = [];
          $('input[name="remove[]"]:checkbox:checked').each(function () {
            arr.push(this.value);
          });
          if(arr == ''){
            arr = '';
          }
         if ($('#formAddModule').smkValidate()) {
            var title         = $('#title').val();
            var type_user     = $('#type_user').val();
            var is_active     = $('#is_active').val();
            var content       = dataEditor.getData();
            var action        = $('#action').val();
            var article_id    = $('#article_id').val();
            //console.log(title+", "+type_user+", "+is_active+", "+content+", "+action+", "+article_id);
            $.post("ajax/AEDModuleGallery.php",{
              remove:arr,
              title:title,
              type_user:type_user,
              is_active:is_active,
              content:content,
              action:action,
              article_id:article_id
            })
              .done(function( data ) {
                $('#id').val(data.id);
                // console.log(data);
                if(myDropzone.getQueuedFiles().length == 0){
                  $.smkProgressBar({
                    element:'body',
                    status:'start',
                    bgColor: '#000',
                    barColor: '#fff',
                    content: 'Loading...'
                  });
                  setTimeout(function(){
                    $.smkProgressBar({status:'end'});
                    showTable();
                    showSlidebar();
                    $.smkAlert({text: 'success',type: 'success'});
                    $('#myModal').modal('toggle');
                  }, 1000);
                }
                myDropzone.processQueue();
            });
          }
       });
       this.on("complete", function(data){
         if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
          {
           var _this = this;
           _this.removeAllFiles();
           $.smkProgressBar({
             element:'body',
             status:'start',
             bgColor: '#000',
             barColor: '#fff',
             content: 'Loading...'
           });
           setTimeout(function(){
             $.smkProgressBar({status:'end'});
             showTable();
             showSlidebar();
             $.smkAlert({text: 'success',type: 'success'});
             $('#myModal').modal('toggle');
           }, 1000);
          }
       });
      },
  });

  // ClassicEditor
  //   .create( document.querySelector( '#editor' ), {
  //       image: {
  //         styles: [
  //           'alignLeft',
  //           'alignCenter',
  //           'alignRight',
  //         ],
  //         resizeOptions: [
  //               {
  //                   name: 'imageResize:original',
  //                   label: 'Original',
  //                   value: null
  //               },
  //               {
  //                   name: 'imageResize:50',
  //                   label: '50%',
  //                   value: '50'
  //               },
  //               {
  //                   name: 'imageResize:75',
  //                   label: '75%',
  //                   value: '75'
  //               }
  //           ],
  //         toolbar: [
  //               'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
  //               '|',
  //               'imageTextAlternative'
  //           ]
  //       },
  //       table: {
  //           contentToolbar: [
  //               'tableColumn', 'tableRow', 'mergeTableCells',
  //               'tableProperties', 'tableCellProperties'
  //           ]
  //       },
  //       ckfinder: {
  //         // eslint-disable-next-line max-len
  //         uploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
  //       }
  //   } )
  //   .then( editor => {
  //     dataEditor = editor;
  //   } )
  //   .catch( err => {
  //     console.error( err.stack );
  //   } );

</script>
