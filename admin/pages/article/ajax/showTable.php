<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:70px;">No</th>
      <th>บทนำ</th>
      <th>Slideshow</th>
      <th style="width:140px;">ประเภทสมาชิก</th>
      <th style="width:100px;">สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
        <th style="width:70px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
        <th style="width:70px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls  = "SELECT * FROM t_article where is_active = 'Y' ORDER BY date_create DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

    if($dataCount > 0)
    {
    $img= "";
    foreach ($rows as $key => $value)
    {
      $article_id   = $value['article_id'];
      $type_user    = $value['type_user'];

      if($type_user == "1")
      {
        $type_user = "ผู้ว่าจ้าง (Member)";
      }
      else if($type_user == "2")
      {
        $type_user = "ผู้รับจ้าง (MemberJob)";
      }
      else if($type_user == "3")
      {
        $type_user = "ผู้ว่าจ้าง (Admin)";
      }


      $sqls   = "SELECT * FROM t_article_gallery WHERE article_id = '$article_id' ORDER BY date_create DESC";
      $query      = DbQuery($sqls,null);
      $json       = json_decode($query, true);
      $row       = $json['data'];
      $dataCount  = $json['dataCount'];

      if($dataCount > 0){
        $img = "<div class=\"row\">";
        foreach ($row as $values)
        {
            $url = "../../../images/article/".$values['image'];
            $img .= "<div class=\"col-md-4\">";
            $img .= "<a href=\"$url\" data-lightbox=\"roadtrip\">";
            $img .= "<div class=\"img-list\" style=\"background-image: url($url);background-size: 100%;background-repeat: no-repeat;\"></div>";
            $img .= "</a>";
            $img .= "</div>";
        }
        $img .= "</div>";
      }
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><?=$value['title'];?></td>
      <td><?=$img;?></td>
      <td><?=$type_user;?></td>
      <td><?=$value['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$article_id ?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delModule('<?=$article_id ?>','<?=$value['title']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php } } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
