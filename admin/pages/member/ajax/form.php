<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$mem_fname      = "";
$mem_lname      = "";
$mem_email      = "";
$mem_mobile     = "";
$member_detail  = "";
$mem_username   = "";
$mem_password   = "";
$mem_type_register  = "";
$mem_type_user      = "";

$is_active  = "";

$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_member WHERE 	mem_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $mem_fname      = $row[0]['mem_fname'];
  $mem_lname      = $row[0]['mem_lname'];
  $mem_email      = $row[0]['mem_email'];
  $mem_mobile     = $row[0]['mem_mobile'];
  $member_detail  = $row[0]['member_detail'];
  $mem_username   = $row[0]['mem_username'];
  $mem_password   = $row[0]['mem_password'];
  $mem_type_register  = $row[0]['mem_type_register'];
  $mem_type_user      = $row[0]['mem_type_user'];

  $is_active          = $row[0]['is_active'];


}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="mem_id" value="<?=$id?>">
<div class="modal-body">
  <!-- <div class="box"> -->
    <!-- <div class="box-header with-border">
      <h3 class="box-title"><b>ข้อมูลส่วนตัว</b></h3>
    </div> -->
    <!-- /.box-header -->
      <!-- <div class="box-body"> -->
        <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>ชื่อ</label>
                <input type="text" <?= $disabled ?> value="<?= $mem_fname ?>" name="mem_fname" class="form-control"  data-smk-msg="ระบุชื่อ" placeholder="ชื่อ" required="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>นามสกุล</label>
                <input type="text" <?= $disabled ?> value="<?= $mem_lname ?>" name="mem_lname" class="form-control" data-smk-msg="ระบุนามสกุล" placeholder="นามสกุล" required="">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>เบอร์โทรศัพท์มือถือ</label>
                <input value="<?=@$mem_mobile?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="mem_mobile" type="tel" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="&nbsp;" class="form-control" placeholder="0xxxxxxxxx" required>
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <label>อีเมล์</label>
                <input value="<?=@$mem_email?>" <?= $disabled ?> name="mem_email" type="email" class="form-control" placeholder="อีเมล์" required data-smk-msg="&nbsp;">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>สถานะ</label>
                <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                  <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Username</label>
                <input value="<?=@$mem_username?>" <?= $disabled2 ?>  onblur="checkUserCode()" id="username" name="mem_username" type="text" class="form-control" placeholder="User Login" required data-smk-msg="&nbsp;">
              </div>
            </div>
            <?php if($action == 'ADD'){ ?>
            <div class="col-md-4">
              <div class="form-group">
                <label>Password</label>
                <input value="" name="mem_password" id="pass1" type="password" autocomplete="new-password" data-smk-msg="&nbsp;" class="form-control" placeholder="Password" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Confirm Password</label>
                <input value="" id="pass2" type="password" class="form-control" data-smk-msg="&nbsp;" placeholder="Confirm Password" required>
              </div>
            </div>
          <?php } ?>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
