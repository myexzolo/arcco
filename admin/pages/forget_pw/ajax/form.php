<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


//$strongPass = 'strong';
$strongPass = 'weak';


?>

<div class="row mar-btn-20" id="action">
</div>
<div class="form-group has-feedback">
  <input type="email" id="email" name="email" class="form-control" placeholder="Email ที่ลงทะเบียน" required>
  <span class="glyphicon glyphicon-envelope form-control-feedback icons"></span>
  <span style="font-size:17px;color:red;">** รหัสผู้ใช้งานหรือรหัสผ่าน ระบบจะส่งรหัส ผ่านช่องทาง Email ที่ลงทะเบียนเท่านั้น</span>
</div>
<div class="row">
  <!-- /.col -->
  <div class="col-xs-12">
    <button type="button" onclick="location.replace('../login/');" class="btn btn-flat pull-left" >เข้าสู่ระบบ</button>
    <button type="submit" id="sendForm" class="btn btn-primary btn-flat pull-right" >Send Email</button>
  </div>
  <!-- /.col -->
</div>
