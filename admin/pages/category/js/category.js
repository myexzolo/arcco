$(function () {
    showTable();
})

var dataEditor;


var init = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var editorElement = CKEDITOR.document.getById( 'editor' );
		// :(((
		// if ( isBBCodeBuiltIn ) {
		// 	editorElement.setHtml(
		// 		'Hello world!\n\n' +
		// 		'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
		// 	);
		// }

		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if ( wysiwygareaAvailable ) {
      dataEditor = CKEDITOR.replace('editor', {
        toolbar: [{
            name: 'clipboard',
            items: ['PasteFromWord', '-', 'Undo', 'Redo']
          },
          {
            name: 'basicstyles',
            items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
          },
          {
            name: 'links',
            items: ['Link', 'Unlink']
          },
          {
            name: 'paragraph',
            items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
          },
          {
            name: 'insert',
            items: ['Image', 'Table']
          },
          {
            name: 'editing',
            items: ['Scayt']
          },
          '/',
          {
            name: 'styles',
            items: ['Format', 'Font', 'FontSize']
          },
          {
            name: 'colors',
            items: ['TextColor', 'BGColor', 'CopyFormatting']
          },
          {
            name: 'align',
            items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
          },
          {
            name: 'document',
            items: ['Templates', 'PageBreak', 'Source']
          }
        ],
      // extraPlugins: 'uploadimage,image2,autogrow',
      // autoGrow_minHeight: 200,
      // autoGrow_maxHeight: 600,
      // autoGrow_bottomSpace: 50,
      // removePlugins: 'resize',
      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '../../ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '../../ckfinder/ckfinder.html?type=Images',
      // filebrowserUploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      //filebrowserUploadUrl: 'base64',
      filebrowserImageUploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

      // image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
			// image2_disableResizer: true
    });
		} else {
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'editor' );

			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}
		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();

function showTable(){
  $.get("ajax/showCategory.php")
  .done(function( data ) {
    $("#showTable").html(data);
     setTimeout(function(){
       $("#showTable").css("visibility", "visible");
     }, 500);
  });
}

function formSeqImage()
{
  var slideObj = $('input[name="slideID[]"]');
  var slideID  = [];
  if(slideObj.length > 0){
    slideObj.each(function() {
      slideID.push($(this).val());
    });
  }
  //console.log(slideID);
  $.post("ajax/seqImage.php",{slideID:slideID})
  .done(function( data ) {
    $.smkProgressBar({
      element:'body',
      status:'start',
      bgColor: '#000',
      barColor: '#fff',
      content: 'Loading...'
    });
    setTimeout(function(){
      $.smkProgressBar({status:'end'});
      showTable();
      showSlidebar();
      $.smkAlert({text: data.message,type: data.status});
    }, 1000);
  });
}



function deleteImage(id){

  $.post("ajax/deleteImage.php",{id:id})
    .done(function( data ) {
      showTable();
  });
}

function showForm(value){
  $.post("ajax/form.php",{value:value})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function editImage(id)
{
  $.post("ajax/form.php",{value:"EDIT",category_id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}


$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    var detail = dataEditor.getData();
    //console.log(detail);
    $('#category_detail').val(detail);
    $.ajax({
        url: 'ajax/AEDModule.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#000',
        barColor: '#fff',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
