<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action           = @$_POST["action"];
$category_id      = @$_POST["category_id"];
$category_name_th = @$_POST["category_name_th"];
$category_name_en = @$_POST["category_name_en"];
$is_active        = @$_POST["is_active"];
$category_path    = @$_FILES['category_path'];

$pathUpload       = "../../../../images/service";


$_POST['user_id'] = $_SESSION['member'][0]['user_id'];


  if(empty($id) && $action == "ADD"){

    $seq = 1;
    $sqlSeq = "SELECT MAX(category_seq) as category_seq  FROM t_category";
    $query  = DbQuery($sqlSeq,null);
    $json   = json_decode($query, true);
    $counts = $json['dataCount'];
    $rows   = $json['data'];
    if($counts > 0){
      $seq = isset($rows[0]['category_seq'])?$rows[0]['category_seq']:0;
      $seq++;
    }

    unset($_POST["action"]);
    unset($_POST["category_id"]);

    $_POST['category_seq'] = $seq;

    $c_img = uploadfile($category_path,$pathUpload,"c_");
    $_POST['category_path'] = $c_img['image'];

    $sql = DBInsertPOST($_POST,'t_category');

  }else if($action == "EDIT"){
    unset($_POST["action"]);
     // echo "---->>>>".$category_path['error'];
    if($category_path['error'] == 0){
      $sql = "SELECT category_path FROM t_category WHERE category_id = '$category_id'";
      $query   = DbQuery($sql,null);
      $row     = json_decode($query,true)['data'][0];
      @unlink($pathUpload."/".$row['category_path']);
      $c_img = uploadfile($category_path,$pathUpload,"c_");
      $_POST['category_path'] = $c_img['image'];
    }else{
      unset($_POST["category_path"]);
    }
    $sql = DBUpdatePOST($_POST,'t_category','category_id');
  }

  //echo $sql;
  $query      = DbQuery($sql,null);
  $row        = json_decode($query, true);
  $errorInfo  = $row['errorInfo'];

  if(intval($row['errorInfo'][0]) == 0){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }


?>
