<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action       = @$_POST['value'];
$category_id  = @$_POST['category_id'];
$category_name_th  = "";
$category_name_en  = "";
$category_path     = "";

$required = "required";

if($action == "EDIT")
{
  $required = "";

  $sqls   = "SELECT * FROM t_category where category_id = '$category_id'";
  $querys = DbQuery($sqls,null);
  $json   = json_decode($querys, true);
  $counts = $json['dataCount'];
  $rows   = $json['data'];

  if($counts > 0){
    $category_name_th  = $rows[0]['category_name_th'];
    $category_name_en  = $rows[0]['category_name_en'];
    $category_path  = $rows[0]['category_path'];
    $category_detail  = $rows[0]['category_detail'];
  }
}

?>
<input type="hidden" id="action" name="action" value="<?= $action ?>">
<input type="hidden" name="category_id" value="<?=$category_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>ชื่อหมวดหมู่ ภาษาไทย</label>
        <input value="<?=$category_name_th?>" name="category_name_th" type="text" class="form-control" placeholder="Name TH" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>ชื่อหมวดหมู่ ภาษาอังกฤษ</label>
        <input value="<?=$category_name_en?>" name="category_name_en" type="text" class="form-control" placeholder="Name EN" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>Image</label>
        <input name="category_path" onchange="readURL(this,'p_img')" type="file" class="form-control custom-file-input" <?=$required ?>>
      </div>
    </div>
    <div class="col-md-4">
      <div id="p_img">
        <?php if($action == 'EDIT'){ ?>
        <img width="100" src="../../../images/service/<?=$category_path?>">
        <?php } ?>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <div id="editor"><?=@$category_detail?></div>
        <input type="hidden" id="category_detail" name="category_detail">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
<?php if($action != "SHOW"){ ?>
<button type="submit" style="width:100px;" class="btn btn-primary btn-flat">บันทึก</button>
<?php } ?>
</div>
<script>
	init();
</script>
