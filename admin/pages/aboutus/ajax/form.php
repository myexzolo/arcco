<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$sqls   = "SELECT * FROM t_about_us where id = 1";
$querys = DbQuery($sqls,null);
$json   = json_decode($querys, true);
$counts = $json['dataCount'];
$rows   = $json['data'];

$title_name = $rows[0]['title_name'];
$path = $rows[0]['path'];
$detail = $rows[0]['detail'];

?>
<div class="col-md-12">
  <div class="box box-warning">
    <!-- /.box-header -->
    <div class="box-body" style="min-height: 200px;margin:20px;">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Title Name</label>
            <input value="<?=$title_name?>" name="title_name" type="text" class="form-control" placeholder="Title Name" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Image</label>
            <input name="path" onchange="readURL(this,'p_img')" type="file" class="form-control custom-file-input" >
          </div>
        </div>
        <div class="col-md-2">
          <div id="p_img">
            <img height="100" src="../../../images/about/<?=$path?>" onerror="this.onerror='';this.src='../../image/no-image.jpg'">
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>รายละเอียด</label>
            <div id="editor"><?=@$detail?></div>
            <input type="hidden" id="detail" name="detail">
          </div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" style="width:190px;height:40px;font-size:22px;" class="btn btn-primary btn-flat pull-right">
        <i class="fa fa-save"></i>&nbsp;
        บันทึก
      </button>
    </div>
  </div>
</div>
<script type="text/javascript">
$(function ()
{
  init();
})
</script>
