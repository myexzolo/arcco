<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');


$sql = "SELECT * FROM t_rider where is_active != 'D'";
//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];
$data       = "";

$path  = "../../../export/driver.csv";

if(is_file($path))
{
    unlink($path);
}


$fp = fopen($path,"w");


foreach ($rows as $key => $row) {
  //print_r($row);
  $line = '';
  foreach( $row as $value )
  {
      if ( ( !isset( $value ) ) || ( $value == "" ) )
      {
          $value = ",";
      }
      else
      {
          $value = str_replace( '"' , '""' , $value );
          $value = '"' . $value . '"' . ",";
      }
      $line .= $value;
  }
  $data .= trim( $line ) . "\n";
}
$data = str_replace( "\r" , "" , $data );
//echo $data;
fwrite($fp,$data);

if ($data != "")
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success', 'path'=> "../../export/driver.csv")));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'ไม่พบข้อมูล')));
}


?>
