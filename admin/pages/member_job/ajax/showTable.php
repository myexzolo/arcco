<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px">ลำดับ</th>
      <th>ชื่อ - สกุล</th>
      <th style="width:100px">เบอร์โทร</th>
      <th>อีเมล์</th>
      <th style="width:100px">UserName</th>
      <th style="width:150px">ประเภทลงทะเบียน</th>
      <th style="width:100px">สถานะ</th>
      <th style="width:50px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:90px;">Re Password</th>
      <th style="width:50px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT *
                 FROM t_member
                 where is_active not in ('D') and mem_type_user = 'CB'
                 ORDER BY date_create DESC";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {
          $memTypeRegister  = $value['mem_type_register'];
          $is_active        = $value['is_active'];
          // $mem_username     = $value['mem_username']?;

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";

          if($memTypeRegister == "FB")
          {
            $memTypeRegister = "Facebook";
            $disabled   = "disabled";
          }else
          {
            $memTypeRegister = "ปกติ";
            $onclick = "onclick=\"resetPass('{$value['mem_id']}')\"";
          }

          if($is_active == "Y")
          {
            $activeTxt = "ใช้งาน";
            $bg        = "bg-green-active";
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }else if($is_active == "W"){
            $activeTxt = "รออนุมัติ";
            $bg        = "bg-aqua-active";
          }

          $fulName = $value['mem_fname']." ".$value['mem_lname'];

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$fulName ?></td>
      <td ><?=$value['mem_mobile']?></td>
      <td align="left"><?=$value['mem_email']?></td>
      <td align="left"><?=$value['mem_username']?></td>
      <td align="center"><?=$memTypeRegister?></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['mem_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point text-yellow "><i class="fa fa-key <?=$disabled?>" <?=$onclick?>></i></a>
      </td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['mem_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['mem_id']?>','<?=$fulName?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
