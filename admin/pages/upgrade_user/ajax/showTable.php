<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px">ลำดับ</th>
      <th>ชื่อ - สกุล</th>
      <th style="width:100px">เบอร์โทร</th>
      <th>อีเมล์</th>
      <th style="width:250px">หมวดหมู่</th>
      <th style="width:100px">สถานะ</th>
      <th style="width:50px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:50px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT *
                 FROM t_upgrade_user u, t_member m, t_category c
                 where u.status not in ('D') and u.mem_id = m.mem_id and u.category_id = c.category_id
                 ORDER BY u.status, u.date_create DESC";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {
          $memTypeRegister  = $value['mem_type_register'];
          $status           = $value['status'];
          $tu_id            = $value['tu_id'];
          // $mem_username     = $value['mem_username']?;

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";

          if($memTypeRegister == "FB")
          {
            $memTypeRegister = "Facebook";
            $disabled   = "disabled";
          }else
          {
            $memTypeRegister = "ปกติ";
          }

          if($status == "Y")
          {
            $activeTxt = "อนุมัติ";
            $bg        = "bg-green-active";
          }if($status == "C")
          {
            $activeTxt = "ไม่อนุมัติ";
            $bg        = "bg-gray";
          }else if($status == "N"){
            $activeTxt = "รออนุมัติ";
            $bg        = "bg-orange-active ";
            $onclick   = "style=\"cursor: pointer;\" onclick=\"showForm('EDIT','$tu_id')\"";
          }

          $fulName = $value['mem_fname']." ".$value['mem_lname'];

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$fulName ?></td>
      <td ><?=$value['mem_mobile']?></td>
      <td align="left"><?=$value['mem_email']?></td>
      <td align="left"><?=$value['category_name_th']?></td>
      <td><div class="<?=$bg?>" <?=$onclick?>><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['tu_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['tu_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['tu_id']?>','<?=$fulName?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
