<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$mem_fname      = "";
$mem_lname      = "";
$mem_email      = "";
$mem_mobile     = "";
$member_detail  = "";
$mem_username   = "";
$mem_password   = "";
$mem_type_register  = "";
$mem_type_user      = "";


$category_name_th   = "";
$tu_img             = "";
$tu_detail          = "";
$category_id        = "";
$category_name_th   = "";

$tu_IDCardimg       = "";
$tu_Meimg           = "";

$status  = "";

$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT *
             FROM t_upgrade_user u, t_member m, t_category c
             where u.status not in ('D') and u.mem_id = m.mem_id and u.category_id = c.category_id and tu_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $mem_fname      = $row[0]['mem_fname'];
  $mem_lname      = $row[0]['mem_lname'];
  $mem_id         = $row[0]['mem_id'];
  $mem_email      = $row[0]['mem_email'];
  $mem_mobile     = $row[0]['mem_mobile'];
  $member_detail  = $row[0]['member_detail'];
  $mem_username   = $row[0]['mem_username'];
  $mem_password   = $row[0]['mem_password'];
  $mem_type_register  = $row[0]['mem_type_register'];
  $mem_type_user      = $row[0]['mem_type_user'];

  $category_name_th   = $row[0]['category_name_th'];
  $tu_img             = $row[0]['tu_img'];
  $tu_IDCardimg       = $row[0]['tu_IDCardimg'];
  $tu_Meimg           = $row[0]['tu_Meimg'];
  $tu_detail          = $row[0]['tu_detail'];
  $category_id        = $row[0]['category_id'];
  $category_name_th   = $row[0]['category_name_th'];

  $status          = $row[0]['status'];

  $mem_fname          = $mem_fname." ".$mem_lname;

  if($tu_img != "")
  {
      $pathImg = "../../../images/upgrade/".$tu_img;
  }
  if($tu_IDCardimg != "")
  {
      $pathIDCardimg = "../../../images/upgrade/".$tu_IDCardimg;
  }
  if($tu_Meimg != "")
  {
      $pathMeimg = "../../../images/upgrade/".$tu_Meimg;
  }
}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="tu_id" value="<?=$id?>">
<input type="hidden" name="mem_id" value="<?=$mem_id?>">
<input type="hidden" name="category_id" value="<?=$category_id?>">
<div class="modal-body">
  <!-- <div class="box"> -->
    <!-- <div class="box-header with-border">
      <h3 class="box-title"><b>ข้อมูลส่วนตัว</b></h3>
    </div> -->
    <!-- /.box-header -->
      <!-- <div class="box-body"> -->
        <div class="row">
            <div class="col-md-5">
              <div class="form-group">
                <label>ชื่อ - สกุล</label>
                <input type="text" disabled value="<?= $mem_fname ?>" class="form-control"  data-smk-msg="ระบุชื่อ" placeholder="ชื่อ" required="">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>เบอร์มือถือ</label>
                <input value="<?=@$mem_mobile?>" disabled OnKeyPress="return chkNumber(this)" type="tel" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="&nbsp;" class="form-control" placeholder="0xxxxxxxxx" required>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>อีเมล์</label>
                <input value="<?=@$mem_email?>" disabled  type="email" class="form-control" placeholder="อีเมล์" required data-smk-msg="&nbsp;">
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-group">
                <label>รายละเอียดข้อมูล</label>
                <textarea class="form-control" disabled rows="8"><?=$tu_detail ?></textarea>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>หมวดหมู่บริการ</label>
                <input value="<?=@$category_name_th?>" disabled  type="text" class="form-control" required data-smk-msg="&nbsp;">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>สถานะอนุมัติ</label>
                <select name="status" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$status=='Y'?"selected":""?>>อนุมัติ</option>
                  <option value="C" <?=@$status=='C'?"selected":""?>>ไม่อนุมัติ</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" id="showImg" style="margin-bottom:0px;margin-top: 15px;cursor:pointer;" onclick="postURL_blank('<?= $pathImg ?>')">
                  <img src="<?= $pathImg ?>"  onerror="this.onerror='';this.src='images/no-image.jpg'" style="height: 70px;">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" id="showImg" style="margin-bottom:0px;margin-top: 15px;cursor:pointer;" onclick="postURL_blank('<?= $tu_IDCardimg ?>')">
                  <img src="<?= $pathIDCardimg ?>"  onerror="this.onerror='';this.src='images/no-image.jpg'" style="height: 70px;">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" id="showImg" style="margin-bottom:0px;margin-top: 15px;cursor:pointer;" onclick="postURL_blank('<?= $tu_Meimg ?>')">
                  <img src="<?= $pathMeimg ?>"  onerror="this.onerror='';this.src='images/no-image.jpg'" style="height: 70px;">
              </div>
            </div>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
