<div class="box-body" style="height:500px;overflow-y: scroll;">
<ul class="products-list product-list-in-box">
<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$sql        = "SELECT * FROM pfit_t_media where is_active = 'Y' and type_media = 'E' ORDER BY update_date DESC";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$count      = $json['dataCount'];

for ($i=0; $i < $count ; $i++)
{
  $pathImg    = "../../image/media/".$rows[$i]['img'];
  $pathUpload = "../../upload/ebook/".$rows[$i]['path'];
?>
    <li class="item">
      <div class="product-img">
        <img src="<?=$pathImg ?>" alt="Ebook Image" style="height:auto;width:140px;">
      </div>
      <div class="product-info" style="margin-left: 150px;">
        <a href="<?=$pathUpload?>" class="product-title" target="_blank"> <?= $rows[$i]['title']; ?>
      </div>
    </li>
<?php
}
?>
</ul>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
</div>
