<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$slide_id     = isset($_POST['slide_id'])?$_POST['slide_id']:0;
$slide_link   = "";
$slide_parm   = "";
$slide_text   = "";

$sqls   = "SELECT * FROM t_slide where slide_id = $slide_id";
$querys = DbQuery($sqls,null);
$json   = json_decode($querys, true);
$counts = $json['dataCount'];
$rows   = $json['data'];

if($counts > 0)
{
  $slide_link  = $rows[0]['slide_link'];
  $slide_parm  = $rows[0]['slide_parm'];
  $slide_text  = $rows[0]['slide_text'];
}

?>
<input type="hidden" name="slide_id" value="<?=$slide_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>Text</label>
        <input value="<?=$slide_text?>" name="slide_text" type="text" class="form-control" placeholder="Text,message">
      </div>
    </div>
    <div class="col-md-8">
      <div class="form-group">
        <label>Link</label>
        <input value="<?=$slide_link?>" name="slide_link" type="text" class="form-control" placeholder="Link">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Parameter</label>
        <select class="form-control select2" id="slide_parm" name="slide_parm"  style="width:100%;">
          <option value="" >&nbsp;</option>
          <?php
            $sqlc     = "SELECT * FROM t_parm WHERE is_active = 'Y'";
            $queryc   = DbQuery($sqlc,null);
            $jsonc    = json_decode($queryc, true);
            $counts   = $jsonc['dataCount'];
            $rowsc    = $jsonc['data'];

            for($x=0; $x < $counts ; $x++)
            {
              $parmName = $rowsc[$x]['parm_name'];
          ?>
          <option value="<?= $parmName ?>" <?=@$slide_parm == $parmName ?"selected":""?>><?= $parmName ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
<button type="submit" style="width:100px;" class="btn btn-primary btn-flat">บันทึก</button>
</div>
<script>
  $('.select2').select2();
</script>
