<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
 .img-item {
   height: 200px;
   width: auto;
 }
 .btn-remove {
   color: #3b3b3b;
   background: rgba(232, 235, 233, 0.7);
   border-width: 0px !important;
 }

 .btn-remove:hover {
   color: #5a635d;
   background: rgba(232, 235, 233, 0.9);
 }
</style>
<form id="formSeqImage" novalidate enctype="multipart/form-data">
<div class="my-container">
<?php
$sqls   = "SELECT * FROM t_slide ORDER BY cast(slide_seq as unsigned)";
$querys = DbQuery($sqls,null);
$json   = json_decode($querys, true);
$counts = $json['dataCount'];
$rows   = $json['data'];
if($counts > 0){
  foreach ($rows as $key => $value) {
?>

  <div class="my-item">
    <img class="img-item" src="../../../images/slide/<?=$value['slide_path'];?>">
    <div style="margin-top:-26px;">
      <button type="button"
       style="height:24px;width:49%; font-size:16px;line-height: 1px;"
       class="btn btn-flat btn-remove" onclick="editLink('<?=$value['slide_id'];?>')" >
        <i class="fa fa-link"></i>
      </button>
      <button type="button"
       style="height:24px;width:49%; font-size:16px;line-height: 1px;"
       class="btn btn-primary btn-flat btn-remove" onclick="deleteImage('<?=$value['slide_id'];?>')" >
        <i class="fa fa-trash-o"></i>
      </button>
    </div>
    <input type="hidden" name="slideID[]" value="<?=$value['slide_id'];?>">
  </div>
<?php } }?>
</div>
</form>
<script>
$('.my-container').sortablePhotos({
  selector: '> .my-item',
  sortable: true,
  padding: 5
});
</script>
