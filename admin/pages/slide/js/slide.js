$(function () {
    showTable();
})


function showTable(){
  $.get( "ajax/showSlide.php")
  .done(function( data ) {
    $("#showTable").html( data );
    setTimeout(function(){
      $("#showTable").css("visibility", "visible");
    }, 500);
  });
}

function formSeqImage()
{
  var slideObj = $('input[name="slideID[]"]');
  var slideID  = [];
  if(slideObj.length > 0){
    slideObj.each(function() {
      slideID.push($(this).val());
    });
  }
  //console.log(slideID);
  $.post("ajax/seqImage.php",{slideID:slideID})
  .done(function( data ) {
    $.smkProgressBar({
      element:'body',
      status:'start',
      bgColor: '#000',
      barColor: '#fff',
      content: 'Loading...'
    });
    setTimeout(function(){
      $.smkProgressBar({status:'end'});
      showTable();
      showSlidebar();
      $.smkAlert({text: data.message,type: data.status});
    }, 1000);
  });
}

function deleteImage(id){

  $.post("ajax/deleteImage.php",{id:id})
    .done(function( data ) {
      showTable();
  });
}

function showForm(value){
  $.post("ajax/form.php",{value:value})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function editLink(slide_id){
  $.post("ajax/formlink.php",{slide_id:slide_id})
    .done(function( data ) {
      $('#myModal2').modal({backdrop:'static'});
      $('#show-form2').html(data);
  });
}


$('#formAddModule').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddModule').smkValidate()) {
    $.ajax({
        url: 'ajax/AEDModule.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#000',
        barColor: '#fff',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAddModule').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});

$('#formLink').on('submit', function(event) {
  event.preventDefault();
  if ($('#formLink').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#000',
        barColor: '#fff',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formLink').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal2').modal('toggle');
      }, 1000);
    });
  }
});
