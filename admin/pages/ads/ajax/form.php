<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action   = @$_POST['action'];
$b_id     = @$_POST['b_id'];
$b_img    = "";
$b_target = "";
$b_type   = "";
$b_url    = "";
$b_datestart = "";
$b_dateend   = "";
$b_seq   = "";

$disabled = "";
$required = "required";
$bg       = "background-color:#fff;";
$display  = "";
$required = "required";

if($action == "EDIT" || $action == 'VIEW')
{
  $required = "";
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display  = "display:none;";
  }


  $sqls   = "SELECT * FROM t_ads where b_id = '$b_id'";
  $querys = DbQuery($sqls,null);
  $json   = json_decode($querys, true);
  $counts = $json['dataCount'];
  $rows   = $json['data'];

  if($counts > 0){
    $b_img    = $rows[0]['b_img'];
    $b_target = $rows[0]['b_target'];
    $b_type   = $rows[0]['b_type'];
    $b_url    = $rows[0]['b_url'];
    $b_datestart = $rows[0]['b_datestart'];
    $b_dateend   = $rows[0]['b_dateend'];
    $b_seq   = $rows[0]['b_seq'];
  }
}

?>
<input type="hidden" id="action" name="action" value="<?= $action ?>">
<input type="hidden" name="b_id" value="<?=$b_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-9">
      <div class="form-group">
        <label>Url</label>
        <input value="<?=$b_url?>" name="b_url" type="text" class="form-control" placeholder="" data-smk-msg="&nbsp;" required <?=$disabled ?>>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Target</label>
        <select name="b_target" class="form-control select2" style="width: 100%;" required <?=$disabled ?> required>
          <option value="" >&nbsp;</option>
          <option value="_blank" <?=@$b_target=='_blank'?"selected":""?>>blank</option>
          <option value="_self" <?=@$b_target=='_self'?"selected":""?>>self</option>
          <option value="_parent" <?=@$b_target=='_parent'?"selected":""?>>parent</option>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Type</label>
        <select name="b_type" class="form-control select2" style="width: 100%;" required <?=$disabled ?> required>
          <option value="">&nbsp;</option>
          <option value="F" <?=@$b_type=='F'?"selected":""?>>Full</option>
          <option value="T" <?=@$b_type=='T'?"selected":""?>>Thumbnail</option>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่เริ่ม</label>
        <input data-smk-msg="&nbsp;" <?= $disabled ?> class="form-control datepicker" value="<?= @DateThai($b_datestart) ?>" required name="b_datestart" type="text" data-provide="datepicker" data-date-language="th-th"  style="<?=$bg ?>">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่สิ้นสุด</label>
        <input data-smk-msg="&nbsp;" <?= $disabled ?> class="form-control datepicker" value="<?= @DateThai($b_dateend) ?>" required name="b_dateend" type="text" data-provide="datepicker" data-date-language="th-th"  style="<?=$bg ?>">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ลำดับ</label>
        <input value="<?=@$b_seq?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="b_seq" type="text" data-smk-msg="&nbsp;" class="form-control" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required <?=$disabled ?>>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Image</label>
        <input name="b_img" onchange="readURL(this,'p_img')" type="file" class="form-control custom-file-input" <?=$required ?> <?=$disabled ?>>
      </div>
    </div>
    <div class="col-md-3">
      <div id="p_img">
        <?php if($action == 'EDIT'|| $action == 'VIEW'){ ?>
        <img width="100" src="../../../images/ads/<?=$b_img?>" onerror="this.onerror='';this.src='../../image/no-image.jpg'">
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
<?php if($action != "VIEW"){ ?>
<button type="submit" style="width:100px;" class="btn btn-primary btn-flat">บันทึก</button>
<?php } ?>
</div>

<script>
  $('.select2').select2();
</script>
