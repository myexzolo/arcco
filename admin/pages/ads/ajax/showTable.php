<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px">ลำดับ</th>
      <th style="width:250px">รูปภาพ</th>
      <th>URL</th>
      <th style="width:60px">Target</th>
      <th style="width:70px">Type</th>
      <th style="width:140px">วันที่เริ่ม</th>
      <th style="width:140px">วันที่สิ้นสุด</th>
      <th style="width:120px">ลำดับการแสดง</th>
      <th style="width:100px">สถานะ</th>
      <th style="width:50px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:50px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT *
                 FROM  t_ads
                 where is_active not in ('D')
                 ORDER BY date_create DESC";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";

          $is_active  = $value['is_active'];


          if($is_active == "Y")
          {
            $activeTxt = "ใช้งาน";
            $bg        = "bg-green-active";
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }else if($is_active == "W"){
            $activeTxt = "รออนุมัติ";
            $bg        = "bg-aqua-active";
          }

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><img class="img-item" src="../../../images/ads/<?=$value['b_img'];?>" onerror="this.onerror='';this.src='../../image/no-image.jpg'" style="width:120px;"></td>
      <td align="left"><?=$value['b_url']?></td>
      <td align="center"><?=$value['b_target']?></td>
      <td align="center"><?=$value['b_type']?></td>
      <td align="center"><?=DateThai($value['b_datestart'])?></td>
      <td align="center"><?=DateThai($value['b_dateend'])?></td>
      <td align="center"><?=$value['b_seq']?></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['b_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['b_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['b_id']?>','')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
