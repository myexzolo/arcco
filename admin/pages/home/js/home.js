$(function () {
  // $('#dateRageShow').hide();
  // $('.connectedSortable').sortable({
  //   containment         : $('section.content'),
  //   placeholder         : 'sort-highlight',
  //   connectWith         : '.connectedSortable',
  //   handle              : '.box-header, .nav-tabs',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  // $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');
  //
  // // jQuery UI sortable for the todo list
  // $('.todo-list').sortable({
  //   placeholder         : 'sort-highlight',
  //   handle              : '.handle',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  //


  checkUpgrade();
  numUserUpgrade();
  numProject();
  numMember();
  numMemberJob();
  showProjectInstallment();
  showComfirmTransfer();
  showAlertTransfer();

  setInterval(function(){
    checkUpgrade();
    numUserUpgrade();
    numProject();
    numMember();
    numMemberJob();
  }, 3000);

  $('#daterage').daterangepicker(
    {
      locale: {
        format: 'DD/MM/YYYY',
        daysOfWeek: [
           "อา",
           "จ",
           "อ",
           "พ",
           "พฤ",
           "ศ",
           "ส"
       ],
       monthNames: [
           "มกราคม",
           "กุมภาพันธ์",
           "มีนาคม",
           "เมษายน",
           "พฤษภาคม",
           "มิถุนายน",
           "กรกฎาคม",
           "สิงหาคม",
           "กันยายน",
           "ตุลาคม",
           "พฤศจิกายน",
           "ธันวาคม"
       ]
      }
    }
  );

  // searchProject();
});


// var gdpData = {
//   "TH-57": 200,
//   "TH-56": 100,
//   "TH-55": 50,
//   "TH-54": 200
// };


function showProjectInstallment()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/showProjectInstallment.php")
     .done(function( data ) {
       $("#showProjectInstallment").html(data);
   });
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAED').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      console.log(data);
      setTimeout(function(){
        showProjectInstallment();
        showComfirmTransfer();
        showAlertTransfer();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});


$('#formTransfer').on('submit', function(event) {
  event.preventDefault();
  if ($('#formTransfer').smkValidate()) {
    $.ajax({
        url: 'ajax/AEDT.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      // console.log(data);
      setTimeout(function(){
        showAlertTransfer();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal2').modal('toggle');
      }, 1000);
    });
  }
});

function showForm(id){
  $.post("ajax/form.php",{action:'EDIT',id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function showFormTransfer(id){
  $.post("ajax/formTransfer.php",{action:'EDIT',id:id})
    .done(function( data ) {
      $('#myModal2').modal({backdrop:'static'});
      $('#show-form2').html(data);
  });
}

function showComfirmTransfer()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/showComfirmTransfer.php")
     .done(function( data ) {
       $("#showComfirmTransfer").html(data);
   });
}



function showAlertTransfer()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/showAlertTransfer.php")
     .done(function( data ) {
       $("#showAlertTransfer").html(data);
   });
}

function checkUpgrade()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/checkUpgrade.php")
     .done(function( data ) {
       $("#checkUpgrade").html(data.count);
   });
}

function numUserUpgrade()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/numUserUpgrade.php")
     .done(function( data ) {
       $("#numUserUpgrade").html(data.count);
   });
}

function numProject()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/numProject.php")
     .done(function( data ) {
       $("#numProject").html(data.count);
   });
}

function numMember()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/numMember.php")
     .done(function( data ) {
       $("#numMember").html(data.count);
   });
}

function numMemberJob()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/numMemberJob.php")
     .done(function( data ) {
       $("#numMemberJob").html(data.count);
   });
}

function searchProject(){
  var daterage    = $('#daterage').val();
  var userLogin  = $('#user_login').val();
  var role  = $('#role').val();
  var res = daterage.split("-");
  var dateStart = dateThToEn(res[0].trim(),"dd/mm/yyyy","/");
  var dateEnd = dateThToEn(res[1].trim(),"dd/mm/yyyy","/");


  $.post("ajax/showProjectList.php",{dateStart:dateStart,dateEnd:dateEnd,userLogin:userLogin,role:role})
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function getMap()
{
  // $.post("ajax/class.php",{week:week})
  //   .done(function( data ) {
  //     $("#showClass").html( data );
  // });
  //ปกติ #00b16a
  //กลาง #00b16a
  //เยอะ #d91e18
  $('#map').vectorMap({
    map: 'th_mill',
    backgroundColor: 'transparent',
    regionStyle: {
                    initial: {
                      fill: '#8d8d8d'
                    }
                  },
    series: {
      regions: [{
        values: gdpData,
        scale: {"200": "#d91e18","100": "#f5e51b","50": "#00b16a"},
        normalizeFunction: 'polynomial'
      }]
    },
    onRegionTipShow: function(e, el, code){
      el.html(el.html()+' (จำนวน '+gdpData[code]+' ราย)');
    }

  });
}

function showClass(week)
{
  $.post("ajax/class.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}

function showClass1(week)
{
  $.post("ajax/class1.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}


function showClass2(week)
{
  $.post("ajax/class2.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}
