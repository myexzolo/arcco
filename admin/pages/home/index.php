<!DOCTYPE html>
<?php
$pageName     = "หน้าหลัก";
$pageCode     = "Home";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบ Back Office - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php
          include("../../inc/sidebar.php");
          include('../../inc/function/mainFunc.php');
          $role_list  = $_SESSION['member'][0]['role_list'];
          $user_login = $_SESSION['member'][0]['user_login'];
          $roleArr   = explode(",",$role_list);
          $noDisplayVendor = "";
          $venDorID = "";

          $scol1 = "col-md-5";
          $scol2 = "col-md-4";
          $scol3 = "col-md-3";

          $RoleAdmin = false;
          $roleCodeArr  = explode(",",$_SESSION['ROLE_USER']['role_code']);
          // print_r($_SESSION['member']);
          if (in_array("ADM", $roleCodeArr) || in_array("SADM", $roleCodeArr)|| in_array("PFIT", $roleCodeArr)){
            $RoleAdmin = true;
          }
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>หน้าหลัก</h1>
            <ol class="breadcrumb">
              <li><a href=""><i class="fa fa-home"></i> Home</a></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-3 mb-4">
                <div class="card border-left-primary shadow h-100 py-2" onclick="gotoPage('../member')">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col-md-12 mr-2">
                          <div class="text-xs font-weight-bold text-primary mb-1">ผู้ว่าจ้าง</div>
                          <div class="h5 mb-0 font-weight-bold text-gray-800" id="numMember">0</div>
                      </div>
                      <div class="icon">
                        <i class="ion ion-person fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 mb-4">
                <div class="card border-left-success shadow h-100 py-2" onclick="gotoPage('../member_job')">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col-md-12 mr-2">
                          <div class="text-xs font-weight-bold text-success mb-1">ผู้รับจ้าง</div>
                          <div class="h5 mb-0 font-weight-bold text-gray-800" id="numMemberJob">0</div>
                      </div>
                      <div class="icon">
                        <i class="ion ion-ios-contact fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 mb-4">
                <div class="card border-left-info shadow h-100 py-2" onclick="gotoPage('../project')">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col-md-12 mr-2">
                          <div class="text-xs font-weight-bold text-info mb-1">Project</div>
                          <div class="h5 mb-0 font-weight-bold text-gray-800" id="numProject">0</div>
                      </div>
                      <div class="icon">
                        <i class="ion ion-ios-bookmarks fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 mb-4">
                <div class="card border-left-warning shadow h-100 py-2" onclick="gotoPage('../upgrade_user')">
                  <span class="badge bg-yellow" id="checkUpgrade">0</span>
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col-md-12 mr-2">
                          <div class="text-xs font-weight-bold text-warning mb-1">อัพเกรด User</div>
                          <div class="h5 mb-0 font-weight-bold text-gray-800" id="numUserUpgrade">0</div>
                      </div>
                      <div class="icon">
                        <i class="ion ion-person-add fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <section class="col-lg-6 connectedSortable ui-sortable">
          <!-- Custom tabs (Charts with tabs)-->
                 <div class="box">
                   <div class="box-header with-border">
                     <h3 class="box-title">Project</h3>
                   </div>
                   <!-- /.box-header -->
                   <div id="showProjectInstallment"></div>
                </div>
            </section>
            <section class="col-lg-6 connectedSortable ui-sortable">
          <!-- Custom tabs (Charts with tabs)-->
                 <div class="box box-warning">
                   <div class="box-header with-border">
                     <h3 class="box-title">อนุมัติการโอนเงิน</h3>
                   </div>
                   <!-- /.box-header -->
                   <div id="showComfirmTransfer"></div>
                </div>
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">แจ้งโอนเงิน (ผู้รับจ้าง)</h3>
                  </div>
                  <!-- /.box-header -->
                  <div id="showAlertTransfer"></div>
               </div>
            </section>
              <!-- /.row -->
        </section>
          <!-- /.content -->
        </div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">อนุมัติการโอนเงิน</h4>
              </div>
              <form id="formAED" novalidate enctype="multipart/form-data">
              <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                <div id="show-form"></div>
              </form>
            </div>
          </div>
        </div>

        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">แจ้งโอนเงิน (ผู้รับจ้าง)</h4>
              </div>
              <form id="formTransfer" novalidate enctype="multipart/form-data">
              <!-- <form novalidate enctype="multipart/form-data" action="ajax/AEDT.php" method="post"> -->
                <div id="show-form2"></div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js"></script>
      <script>
          $(".select2").select2();
      </script>
    </body>
  </html>
