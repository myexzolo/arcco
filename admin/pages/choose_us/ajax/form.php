<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action   = @$_POST['action'];
$tc_id    = @$_POST['tc_id'];
$tc_text  = "";
$tc_img   = "";
$tc_seq   = "";

$disabled = "";
$required = "required";
$bg       = "background-color:#fff;";
$display  = "";
$required = "required";

if($action == "EDIT" || $action == 'VIEW')
{
  $required = "";
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display  = "display:none;";
  }


  $sqls   = "SELECT * FROM t_choose where tc_id = '$tc_id'";
  $querys = DbQuery($sqls,null);
  $json   = json_decode($querys, true);
  $counts = $json['dataCount'];
  $rows   = $json['data'];

  if($counts > 0){
    $tc_text  = $rows[0]['tc_text'];
    $tc_img   = $rows[0]['tc_img'];
    $tc_seq   = $rows[0]['tc_seq'];
  }
}

?>
<input type="hidden" id="action" name="action" value="<?= $action ?>">
<input type="hidden" name="tc_id" value="<?=$tc_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-10">
      <div class="form-group">
        <label>ข้อความ</label>
        <input value="<?=$tc_text?>" name="tc_text" type="text" class="form-control" placeholder="" data-smk-msg="&nbsp;" required <?=$disabled ?>>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ลำดับ</label>
        <input value="<?=@$tc_seq?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="tc_seq" type="text" data-smk-msg="&nbsp;" class="form-control" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required <?=$disabled ?>>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Image</label>
        <input name="tc_img" onchange="readURL(this,'p_img')" type="file" class="form-control custom-file-input" <?=$required ?> <?=$disabled ?>>
      </div>
    </div>
    <div class="col-md-3">
      <div id="p_img">
        <?php if($action == 'EDIT' || $action == 'VIEW'){ ?>
        <img width="100" src="../../../images/choose/<?=$tc_img?>" onerror="this.onerror='';this.src='../../image/no-image.jpg'">
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
<?php if($action != "VIEW"){ ?>
<button type="submit" style="width:100px;" class="btn btn-primary btn-flat">บันทึก</button>
<?php } ?>
</div>

<script>
  $('.select2').select2();
</script>
