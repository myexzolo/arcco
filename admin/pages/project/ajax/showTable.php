<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px">ลำดับ</th>
      <th>Project</th>
      <th>Owner</th>
      <th>Service</th>
      <th>Project Type</th>
      <th style="width:185px">Quotation</th>
      <th style="width:215px">Installment</th>
      <th style="width:90px">สถานะ</th>
      <th style="width:40px;">Chat</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:40px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:40px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
    $sql = "SELECT * FROM t_job j , data_mas_add_province p, t_category c , t_type_build tb , t_type_project tp
            WHERE j.PROVINCE_CODE = p.PROVINCE_CODE
            AND j.category_id = c.category_id
            AND j.ttb_id = tb.ttb_id
            AND j.ttp_id = tp.ttp_id AND j.is_acive = 'Y' Order By date_kickoff";

    $query = DbQuery($sql,null);
    $row = json_decode($query,true);


    if($row['dataCount'] > 0){

      foreach ($row['data'] as $key => $value) {


        if(!empty($value['img_ref'])){
            $img_ref = explode('|',$row['data'][0]['img_ref']);
        }

        $Quotation = "";
        $installmentInvoice = "";

        $tj_id =  $value['tj_id'];
        $tj_quotation =  $value['tj_quotation'];
        if($tj_quotation != ""){
          $Quotation = "<a href=\"../../../pdf/output/$tj_quotation\" target=\"_blank\">$tj_quotation</a> (รหัส {$value['password_admin']})";
        }

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";

          // if($memTypeRegister == "FB")
          // {
          //   $memTypeRegister = "Facebook";
          //   $disabled   = "disabled";
          // }else
          // {
          //   $memTypeRegister = "ปกติ";
          //   $onclick = "onclick=\"resetPass('{$value['mem_id']}')\"";
          // }
          //
          // if($is_active == "Y")
          // {
          //   $activeTxt = "ใช้งาน";
          //   $bg        = "bg-green-active";
          // }else if($is_active == "N"){
          //   $activeTxt = "ไม่ใช้งาน";
          //   $bg        = "bg-gray";
          // }else if($is_active == "W"){
          //   $activeTxt = "รออนุมัติ";
          //   $bg        = "bg-aqua-active";
          // }
          //
          // $fulName = $value['mem_fname']." ".$value['mem_lname'];


          $str = $value['tj_status']!='A'?"AND status in ('Y','A')":"";
          $sqlo = "SELECT * FROM orders od , order_detail odd , t_member tm
                  WHERE od.o_id = odd.o_id AND odd.mem_id = tm.mem_id
                  AND od.tj_id = '{$value['tj_id']}' $str";
          $queryo = DbQuery($sqlo,null);
          $rowo = json_decode($queryo,true);
          if($rowo['dataCount'] > 0){
            foreach ($rowo['data'] as $valueo) {
              $status = $valueo['status'];
              $od_id  = $valueo['od_id'];
              $fullname = $valueo['mem_fname'].' '.$valueo['mem_lname'];

              if($status == "A")
              {
                $sqli = "SELECT * FROM order_installment
                         WHERE od_id = $od_id and (status_pay is null or status_pay in ('P','Y'))
                         order by installment_no ASC";

               $queryi = DbQuery($sqli,null);
               $rowoi = json_decode($queryi,true);

               if($rowoi['dataCount'] > 0)
               {
                 $installmentInvoice = '';
                 foreach ($rowoi['data'] as $keyoi => $valueoi) {
                   $installment_no = $valueoi['installment_no'];
                   $installment_id = $valueoi['installment_id'];
                   $installment_invoice = $valueoi['installment_invoice'];

                   if($valueoi['installment_invoice'] != ''){
                     $pathPay = "../../../page/pay/index.php?installment_id=$installment_id";
                     $status_pay = $valueoi['status_pay']=='Y'?"ชำระเงินเรียบร้อย":"รอการชำระเงิน";
                     $installmentInvoice .= "งวดที่  $installment_no ($status_pay)"."<br /><a href=\"../../../pdf/output/$installment_invoice\" target=\"_blank\">$installment_invoice</a> (รหัส {$valueoi['password_admin']})<br />";
                   }
                 }
                 // if($rowoi['data'][0]['installment_invoice'] != "")
                 // {
                 //   $installment_no = $rowoi['data'][0]['installment_no'];
                 //   $installment_id = $rowoi['data'][0]['installment_id'];
                 //   $installment_invoice = $rowoi['data'][0]['installment_invoice'];
                 //
                 //   $pathPay = "../../page/pay/index.php?installment_id=$installment_id";
                 //   $status_pay = $rowoi['data'][0]['status_pay']=='Y'?"ชำระเงินเรียบร้อย":"รอการชำระเงิน";
                 //   $installmentInvoice = "<b>งวดที่  $installment_no ($status_pay)</b>"."<br /><a href=\"../../pdf/output/$installment_invoice\" target=\"_blank\">$installment_invoice</a>";
                 //   $installmentInvoice .= "<br /><a onclick=\"postURL('$pathPay')\" style=\"cursor:pointer\"><b>ชำระเงิน</b></a>";
                 // }
               }
              }
            }
          }


          $text = '';
          $date = '';
          switch ($value['tj_status']) {
            case 'A':
              $text = 'Request Person';
              break;
            case 'S':
              $text = 'Start Project';
              break;
            case 'E':
              $text = 'Close Project';
              break;
            default:
              $text = 'No response';
              break;
          }


    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$value['tj_name']."<br> (".$value['PROVINCE_NAME'].")";?></td>
      <td align="left"><?=@$fullname?></td>
      <td align="left"><?=$value['category_name_th']?></td>
      <td align="left"><?=$value['ttp_id']=='6'?$value['tj_type_name']:$value['ttp_name']."<br> (".$value['ttb_name'].")";?></td>
      <td align="left"><?=$Quotation?></td>
      <td align="left"><?=$installmentInvoice?></td>
      <td align="left"><?=$text.' '.$date?></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-wechat" onclick="showChat('<?=$value['tj_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['tj_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['tj_id']?>','<?=$value['tj_name']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
