<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$tj_id  = isset($_POST['tj_id'])?$_POST['tj_id']:"";

?>
<div class="modal-body">
<div class="chat" id="chat">
  <div class="header-chat">
    <p>chat</p>
  </div>
  <div class="body-chat" id="bodyChat">
    <?php
        $sqll = "SELECT * FROM t_event_log WHERE tj_id = '$tj_id' ORDER BY date_create ASC";
        $queryl = DbQuery($sqll,null);
        $rowl = json_decode($queryl,true);

        //echo $sqll;

        if($rowl['dataCount'] > 0){

          foreach ($rowl['data'] as $valuel) {

              $is_active  = $valuel['is_active'];
              $bgActive   = "";
              $faClass    = "fa-eye-slash";
              $textColor  = "text-aqua";
              $valHiden   = "N";
              if($is_active == 'N')
              {
                $bgActive   = "opacity: 0.5";
                $faClass    = "fa-eye";
                $textColor  = "text-green";
                $valHiden   = "Y";
              }

            ?>

    <div class="body-chats <?=$valuel['te_user_type']?>" style="<?=$bgActive ?>">
      <div class="<?=$valuel['te_user_type']?>-chat">
        <?=ltrim($valuel['te_text'])?>
        <?php if(isset($valuel['te_img']) && !empty($valuel['te_img'])){ $te_img = $valuel['te_img']; ?>
          <h6 class="file-text">
            <?php if(strpos($te_img,".pdf") > 0){ ?>
              <a target="_blank" href="../../../images/slipt/<?=$valuel['te_img']?>">
                <?=$valuel['te_img']?>
              </a>
            <?php }else{ ?>
              <a href="../../../images/slipt/<?=$valuel['te_img']?>" data-lightbox="file">
                <?=$valuel['te_img']?>
              </a>
            <?php }?>
          </h6>
        <?php } ?>
        <span><i><?=$valuel['date_create']?></i></span>
      </div>
    </div>
    <div class="h_<?=$valuel['te_user_type']." ".$textColor ?> h_chat" onclick="setHidenChat('<?=$valuel['te_id']?>','<?=$valHiden?>','<?=$tj_id?>');"><i class="fa <?= $faClass ?>"></i></div>
    <?php
      }
    } ?>
  </div>
</div>
<div class="chat-form">
  <div class="form-group">
    <textarea name="te_text" id="te_text" class="form-control" rows="3" required></textarea>
    <input type="hidden" id="te_user_type" value="admin">
    <input type="hidden" id="tj_id" value="<?=$tj_id?>">
  </div>
  <!-- <div class="form-group">
    <label>อัพโหลดไฟล์</label>
    <input type="file" accept="image/png,image/jpeg" class="form-control" name="te_img" onchange="readURL(this,'showImage')">
  </div>
  <div id="showImage"></div> -->

    <div class="text-right">
      <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary btn-flat" onclick="sendMessage()">send message</button>
    </div>
</div>
</div>
