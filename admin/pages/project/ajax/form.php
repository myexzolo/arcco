<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$tj_name        = "";
$tj_detail      = "";
$category_id    = "";
$tj_budget_min  = "";
$tj_budget_max  = "";
$tj_status      = "";
$is_acive       = "";

$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql = "SELECT * FROM t_job WHERE tj_id = $id";


  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $tj_name        = $row[0]['tj_name'];
  $tj_detail      = $row[0]['tj_detail'];
  $category_id      = $row[0]['category_id'];
  $tj_budget_min     = $row[0]['tj_budget_min'];
  $tj_budget_max  = $row[0]['tj_budget_max'];
  $tj_status   = $row[0]['tj_status'];
  $is_acive   = $row[0]['is_acive'];



}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="tj_id" value="<?=$id?>">
<div class="modal-body">
  <!-- <div class="box"> -->
    <!-- <div class="box-header with-border">
      <h3 class="box-title"><b>ข้อมูลส่วนตัว</b></h3>
    </div> -->
    <!-- /.box-header -->
      <!-- <div class="box-body"> -->
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Project</label>
                <input type="text" <?= $disabled ?> value="<?= $tj_name ?>" name="tj_name" class="form-control"  data-smk-msg="&nbsp;" placeholder="" required="">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>รายละเอียดงาน</label>
                <textarea name="tj_detail" id="tj_detail" class="form-control" rows="3" required><?=$tj_detail ?></textarea>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>ราคาขั้นต่ำ</label>
                <input value="<?=@$tj_budget_min?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="tj_budget_min" type="text" data-smk-msg="&nbsp;" class="form-control" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>ราคาสูงสุด</label>
                <input value="<?=@$tj_budget_max?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="tj_budget_max" type="text" data-smk-msg="&nbsp;" class="form-control" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>สถานะ Project</label>
                <select name="tj_status" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="A" <?=@$tj_status=='A'?"selected":""?>>Request Person</option>
                  <option value="S" <?=@$tj_status=='S'?"selected":""?>>Start Project</option>
                  <option value="E" <?=@$tj_status=='E'?"selected":""?>>Close Project</option>
                  <option value="" <?=@$tj_status==''?"selected":""?>>No responset</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>สถานะ</label>
                <select name="is_acive" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$is_acive=='Y'?"selected":""?>>ใช้งาน</option>
                  <option value="N" <?=@$is_acive=='N'?"selected":""?>>ไม่ใช้งาน</option>
                </select>
              </div>
            </div>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
