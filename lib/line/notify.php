<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	date_default_timezone_set("Asia/Bangkok");

	function getAccessToken(){
	  return "uSed+uJkrkAg4CBsgXxx7gybmZDSrsvEjm4MFUoJiK1gLxVhji1MIQ8R7EljKnDt5G0LPlVxp+SSurX2/8O795t9Nw+uqIX350YvVOMsSdpY1phfSGicKbKnUlT5YJlyuDQKTb+RAnhvkAkFje7xygdB04t89/1O/w1cDnyilFU=";
	}

	function send_notify_message($message_data){

	   $access_token = "a957Cy9rzvc4t2RnZDRb5y9vmRAylN9iuPdGdd5qtzE";
	   $headers = array('Method: POST', 'Content-type: multipart/form-data', 'Authorization: Bearer '.$access_token );

	   $ch = curl_init();
	   curl_setopt($ch, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
	   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	   curl_setopt($ch, CURLOPT_POSTFIELDS, $message_data);
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	   $result = curl_exec($ch);
	   // Check Error
	   if(curl_error($ch))
	   {
	      $return_array = array( 'status' => '000: send fail', 'message' => curl_error($ch) );
	   }
	   else
	   {
	      $return_array = json_decode($result, true);
	   }
	   curl_close($ch);
	 return $return_array;
	}

	function getFormatTextMessage($text){
	  $datas = [];
	  $datas['type'] = 'text';
	  $datas['text'] = $text;
	  return $datas;
	}

	function sentMessage($encodeJson,$datas){
	  $datasReturn = [];
	  $curl = curl_init();
	  curl_setopt_array($curl, array(
	    CURLOPT_URL => $datas['url'],
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_POSTFIELDS => $encodeJson,
	    CURLOPT_HTTPHEADER => array(
	      "authorization: Bearer ".$datas['token'],
	      "cache-control: no-cache",
	      "content-type: application/json; charset=UTF-8",
	    ),
	  ));

	  $response = curl_exec($curl);
	  $err = curl_error($curl);

	  curl_close($curl);

	  if ($err) {
	      $datasReturn['result'] = 'E';
	      $datasReturn['message'] = $err;
	  } else {
	      if($response == "{}"){
	    $datasReturn['result'] = 'S';
	    $datasReturn['message'] = 'Success';
	      }else{
	    $datasReturn['result'] = 'E';
	    $datasReturn['message'] = $response;
	      }
	  }

	  return $datasReturn;
	}

	function lineCurlgetProfile($datas){
	  $datasReturn = [];
	  $curl = curl_init();
	  curl_setopt_array($curl, array(
	    CURLOPT_URL => $datas['url'],
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    // CURLOPT_CUSTOMREQUEST => "POST",
	    // CURLOPT_POSTFIELDS => $encodeJson,
	    CURLOPT_HTTPHEADER => array(
	      "authorization: Bearer ".$datas['token'],
	      "cache-control: no-cache",
	      "content-type: application/json; charset=UTF-8",
	    ),
	  ));

	  $response = curl_exec($curl);
	  $err = curl_error($curl);

	  curl_close($curl);

	  if ($err) {
	      $datasReturn['result'] = 'E';
	      $datasReturn['message'] = $err;
	  } else {
	      if($response == "{}"){
	    $datasReturn['result'] = 'S';
	    $datasReturn['message'] = 'Success';
	      }else{
	    $datasReturn['result'] = 'E';
	    $datasReturn['message'] = $response;
	      }
	  }

	  return $datasReturn;
	}

	function sendLineMessages($userId,$messages){
	  $access_token = getAccessToken();

	  $json[0]['type'] = "text";
	  $json[0]['text'] = $messages;

	  $post = json_encode(array(
	      'to' => $userId,
	      'messages' => $json,
	  ));

	  // $url = 'https://api.line.me/v2/bot/message/multicast';
	  $url = 'https://api.line.me/v2/bot/message/push';
	  $headers = array('Content-Type: application/json', 'Authorization: Bearer '.$access_token);
	  $ch = curl_init($url);
	  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	  $result = curl_exec($ch);
		return $result;
	}

?>
