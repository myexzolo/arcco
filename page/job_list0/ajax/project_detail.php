<section class="blog pdd-t-0">

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active">
              <a href="#findjob" aria-controls="Create Job" role="tab" data-toggle="tab">
                Find Job
              </a>
            </li>    
            <li role="presentation" onclick="goCreateJob();">
              <a href="#project" aria-controls="project" role="tab" data-toggle="tab">
                Create Job
              </a>
            </li>
          </ul>
          <!-- Find Job -->
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="findjob">
              <div class="col-md-12">
                <div class="row">
                <form class="form-inline">
                   <div class="form-group" id="jobtitle" >
                    <input type="jobtitle" class="form-control" id="jobtitle" placeholder="Job Title">
                   </div>
                   <select class="form-control" id="alljob">
                    <option>Buiding & Construction (All)</option>
                    <option>Architectural Services</option>
                    <option>Buiding & Construction / QS</option>
                    <option>Civil / Structural</option>
                  </select>
                  <select class="form-control select2" id="alllocation">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      <option>All Locations</option>
                      <?php
                        $sql   = "SELECT * FROM data_mas_add_province
                                WHERE PROVINCE_CODE != 00";
                        $query = DbQuery($sql,null);
                        $row   = json_decode($query,true);
                        if($row['dataCount'] > 0){
                          foreach ($row['data'] as $value) {
                      ?>
                      <option value="<?=$value['PROVINCE_CODE']?>"><?=$value['PROVINCE_NAME']?></option>
                      <?php }} ?>
                    </select>
                    <!-- Btn Find Job -->
                    <button class="btn btn-primary" type="button" id="btnfindjob">
                      <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                      </svg>
                      <i class="bi bi-search"></i> Find Job
                      </button>
                      <!-- Alert -->
                      <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>ไม่พบงานที่ค้นหา!</strong> กรุณากรอกข้อมูลที่ต้องการค้นหางาน
                      </div>
                  
                    <!-- Range -->
                    <div style="margin-top:50px;" >
                      <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="300000" data-slider-step="5" data-slider-value="[0,300000]" id="sl2" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="salaryrange">Salary: 0 - 300,000 (฿)</b><br />    
                    </div>

                   
                    <!-- job list -->
                    </p>
                    
                      <div class="card card-body">

                      <div class="row">
                        <div class="jumbotron">
                        <img src="../../images/c1.jpg">
                        <h3>GAYSORN GROUP</h3>
                        <hr>
                        <p>Building & Property</p>
                        <p>Chonburi</p>
                        <p>Salary: 20,00 - 50,000 Baht</p>
                        <br>
                        <p><a class="btn btn-primary btn-lg" href="#" role="button" id="btnjd">อ่านเพิ่มเติม</a></p>
                        </div>
                      </div>  
                      <div class="row">
                        <div class="jumbotron">
                        <img src="../../images/c2.png">
                        <h3>GAYSORN GROUP</h3>
                        <hr>
                        <p>Building & Property</p>
                        <p>Chonburi</p>
                        <p>Salary: 20,00 - 50,000 Baht</p>
                        <br>
                        <p><a class="btn btn-primary btn-lg" href="#" role="button" id="btnjd">อ่านเพิ่มเติม</a></p>
                      </div>
                      </div>

                      <div class="row">
                        <div class="jumbotron">
                        <img src="../../images/c3.png">
                        <h3>GAYSORN GROUP</h3>
                        <hr>
                        <p>Building & Property</p>
                        <p>Chonburi</p>
                        <p>Salary: 20,00 - 50,000 Baht</p>
                        <br>
                        <p><a class="btn btn-primary btn-lg" href="#" role="button" id="btnjd">อ่านเพิ่มเติม</a></p>
                      </div>
                    </div>                   
                  </div>
                  <!-- job list -->

                  <!-- Collapse -->
                  <a class="btnmore" role="button" data-toggle="collapse" href="#collapse" aria-expanded="false" aria-controls="collapse">
                    แสดงเพิ่มเติม..
                  </a>
            
                  <div class="collapse" id="collapse">
                    <div class="row">
                    <div class="jumbotron">
                        <img src="../../images/c1.jpg">
                        <h3>GAYSORN GROUP</h3>
                        <hr>
                        <p>Building & Property</p>
                        <p>Chonburi</p>
                        <p>Salary: 20,00 - 50,000 Baht</p>
                        <br>
                        <p><a class="btn btn-primary btn-lg" href="#" role="button" id="btnjd">อ่านเพิ่มเติม</a></p>
                      </div>
                    </div>
                  </div>
                  <!-- Close Collapse -->
                </form>
              </div>
            </div>
          </div>

        
          <!-- Create Job -->
            <div role="tabpanel" class="tab-pane" id="project">
              <div class="row">

                <div class="col-xs-12">
                  <div class="tab-info create-section">
                    <p>Create New Job
                      <a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Click here</a>
                    </p>
                  </div>

                  <div class="collapse" id="collapseExample">
                    <div class="well">
                      <div class="row">
                        <form class="formLogin createJob" id="create-Job" novalidate></form>
                      </div>
                    </div>
                  </div>

                </div>
 

                
                </div>
                <hr>

                <div class="col-md-6 col-xs-12">
                <img class="img-full" src="../../images/c3.png">
                </div>
                <div class="col-md-6 col-xs-12">
                <div class="about-content">
                <h3>Architect Officer/สถาปนิก <a class="more" href="../../page/ww/ajax/view.php">ดูเพิ่มเติม</a></h3>
                    <p>STOREHOUSE CO., LTD.</p>
                    <p>ลาดพร้าว</p>
                    <p>THB 13,000 - THB 20,000 /เดือน</p>
                

                    <span class="badge badge-dark">ลงประกาศเมื่อ</span>
                    <span class="badge badge-secondary">28 Feb 2021</span>
                    <input class="inputtoogle" type="checkbox" id="switch" /><label class="Toggle1" for="switch">Toggle</label>
                    
                  </div>
                
                </div>

                <div class="row">
                
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
                </div>
         
           
                
            

            






            <div role="tabpanel" class="tab-pane" id="blog">
              <div class="row" id="data-blog"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>


