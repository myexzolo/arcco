<?php

include('../../../admin/inc/function/connect.php');

$sql   = "SELECT * FROM t_memberjob WHERE mem_id = '{$_SESSION['member']['mem_id']}'";
$query = DbQuery($sql,null);
$row   = json_decode($query,true)['data'][0];

$memberJob_portfolio = array_diff(explode("|", $row['memberJob_portfolio']),array(""));

?>

<input type="hidden" name="memberJob_id" value="<?=@$row['memberJob_id']?>">

<div class="col-md-12">
  <div class="form-group">
    <label>เกี่ยวกับเรา</label>
    <div id="editor"><?=@$row['memberJob_detail']?></div>
    <input type="hidden" id="memberJob_detail" name="memberJob_detail">
  </div>
</div>

<div class="col-md-6 col-xs-12">
  <div class="form-group">
    <label>ภาพแบรนเนอร์</label>
    <input type="file" name="memberJob_banner" onchange="readURL(this,'showImg')" <?=empty($row['memberJob_banner'])?"required":""?>>
  </div>
  <div id="showImg">
    <?php if(!empty($row['memberJob_banner'])){ ?>
    <img width="100px" src="../../images/memberJob/imgThum/<?=@$row['memberJob_banner']?>">
    <?php } ?>
  </div>
</div>

<div class="col-md-6 col-xs-12">
  <div class="form-group">
    <label>ภาพผลงานของเรา</label>
    <input type="file" name="memberJob_portfolio[]" multiple>
  </div>
  <?php
    foreach ($memberJob_portfolio as $value_slide) {
  ?>
    <div class="" style="padding:0px;position: relative;display: inline-block;">
      <img width="100" class="img-responsive img-border" src="../../images/memberJob/portfolio/<?=$value_slide?>">
      <label class="toggle" style="position: absolute;top: 0px;left: 0px;">
        <input class="toggle__input" name="remove[]" value="<?=$value_slide?>" type="checkbox">
        <span class="toggle__label">
          <span class="toggle__text"></span>
        </span>
      </label>
    </div>
  <?php } ?>
</div>

<div class="col-xs-12">
  <div class="btn-bottom text-center" style="margin-top: 50px;">
    <button type="submit" class="btn btn-default">แก้ไขโปรไฟล์โปรเจค</button>
  </div>
</div>


<script type="text/javascript">
  var dataEditor;
  ClassicEditor
    .create( document.querySelector( '#editor' ), {
        image: {
          styles: [
            'alignLeft',
            'alignCenter',
            'alignRight',
          ],
          resizeOptions: [
                {
                    name: 'imageResize:original',
                    label: 'Original',
                    value: null
                },
                {
                    name: 'imageResize:50',
                    label: '50%',
                    value: '50'
                },
                {
                    name: 'imageResize:75',
                    label: '75%',
                    value: '75'
                }
            ],
          toolbar: [
                'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                '|',
                'imageTextAlternative'
            ]
        },
        table: {
            contentToolbar: [
                'tableColumn', 'tableRow', 'mergeTableCells',
                'tableProperties', 'tableCellProperties'
            ]
        },
        ckfinder: {
          // eslint-disable-next-line max-len
          uploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
        }
    } )
    .then( editor => {
      dataEditor = editor;
    } )
    .catch( err => {
      console.error( err.stack );
    } );
</script>
