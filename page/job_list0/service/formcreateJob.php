<?php

include('../../../admin/inc/function/connect.php');

?>
<input type="hidden" name="mem_id" value="<?=$_SESSION['member']['mem_id']?>">
<div class="col-xs-6">
  <div class="form-group">
    <label>ชื่อตำแหน่งงาน</label>
    <input type="text" name="tj_name" class="form-control" required>
  </div>
</div>
<div class="col-xs-6">
  <div class="form-group">
    <label>สถานที่ทำงาน</label>
    <input type="number" name="tj_date" class="form-control" required>
  </div>
</div>
<div class="col-xs-6">
  <div class="form-group">
    <label>ประเภทงาน</label>
    <select class="form-control" name="category_id" required>
      <option value="">เลือกประเภทงาน</option>
      <?php
        $sqlc = "SELECT * FROM t_category";
        $queryc = DbQuery($sqlc,null);
        $rowc = json_decode($queryc,true);
          if($rowc['dataCount'] > 0){
            foreach ($rowc['data'] as $valuec) {
      ?>
      <option value="<?=$valuec['category_id']?>"><?=$valuec['category_name_th']?></option>
      <?php }} ?>
    </select>
  </div>
</div>
<div class="col-xs-3">
  <div class="form-group">
    <label>เงินเดือน</label>
    <input type="text" name="tj_budget_min" data-smk-type="currency" class="form-control" required>
  </div>
</div>
<div class="col-xs-3">
  <div class="form-group">
    <label>ถึง</label>
    <input type="text" name="tj_budget_max"  data-smk-type="currency" class="form-control" required>
  </div>
</div>
<div class="col-xs-6">
  <div class="form-group">
    <label>อีเมล</label>
    <input type="text" name="tj_budget_max"  data-smk-type="currency" class="form-control" required>
  </div>
</div>
<div class="col-xs-3">
  <div class="form-group">
    <label>Line ID</label>
    <input type="text" name="tj_budget_max"  data-smk-type="currency" class="form-control" required>
  </div>
</div>
<div class="col-xs-3">
  <div class="form-group">
    <label>เบอร์โทรศัพท์</label>
    <input type="text" name="tj_budget_max"  data-smk-type="currency" class="form-control" required>
  </div>
</div>
<div class="col-xs-12">
  <div class="form-group">
    <label>รายละเอียดงาน</label>
    <div id="editor2"></div>
    <input type="hidden" id="tj_detail" name="tj_detail" required>
  </div>
</div>

<!--Job Type-->
<div class="col-sm-12">
          <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#" id="jobtype1">Full Time</a></li>
            <li role="presentation" class="active"><a href="#" id="jobtype2">Part Time</a></li>
            <li role="presentation" class="active"><a href="#" id="jobtype3">Contract</a></li>
            <li role="presentation" class="active"><a href="#" id="jobtype4">Internship</a></li>
            <div class="checkbox1">
            <label>
              <input type="checkbox"> Work from home/Remote
            </label>
            </div>
          </ul>
        </div>



<div class="col-xs-12">
  <div class="form-group">
    <label>ภาพประกอบ</label>
    <input type="file" name="tj_img" onchange="readURL(this,'showImg')"  required>
    <p class="help-block">Example block-level help text here.</p>
  </div>
  <div id="showImg"></div>
</div>
<div class="col-xs-12">
  <div class="btn-bottom text-center">
    <button type="submit" class="btn btn-default">ลงประกาศงาน</button>
  </div>
</div>

<script type="text/javascript">
  var dataEditor2;
  ClassicEditor
    .create( document.querySelector( '#editor2' ), {
        image: {
          styles: [
            'alignLeft',
            'alignCenter',
            'alignRight',
          ],
          resizeOptions: [
                {
                    name: 'imageResize:original',
                    label: 'Original',
                    value: null
                },
                {
                    name: 'imageResize:50',
                    label: '50%',
                    value: '50'
                },
                {
                    name: 'imageResize:75',
                    label: '75%',
                    value: '75'
                }
            ],
          toolbar: [
                'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                '|',
                'imageTextAlternative'
            ]
        },
        table: {
            contentToolbar: [
                'tableColumn', 'tableRow', 'mergeTableCells',
                'tableProperties', 'tableCellProperties'
            ]
        },
        ckfinder: {
          // eslint-disable-next-line max-len
          uploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
        }
    } )
    .then( editor => {
      dataEditor2 = editor;
    } )
    .catch( err => {
      console.error( err.stack );
    } );

</script>
