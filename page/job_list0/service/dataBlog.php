<?php
include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');
?>
<div class="col-md-3 col-sm-4 col-xs-12 mar-30">
  <div class="create-blog" data-mh="my-group" onclick="openModelcontent('ADD')">
    <span class="glyphicon glyphicon-plus"></span>
    <p>Add Blog</p>
  </div>
</div>

<?php
  $sql   = "SELECT * FROM t_blog WHERE user_id = '{$_SESSION['member']['mem_id']}' ORDER BY blog_id DESC";
  $query = DbQuery($sql,null);
  $row   = json_decode($query,true);
  if($row['dataCount'] > 0){
    foreach ($row['data'] as $value) {
?>
<div class="col-md-3 col-sm-4 col-xs-12 mar-30">
  <a class="a-content" onclick="openModelcontent('EDIT','<?=$value['blog_id']?>')" href="javascript:void(0)">
    <div class="blog-content" data-mh="my-group">
      <img class="img-responsive" src="../../images/blog/<?=$value['blog_img']?>">
      <div class="content">
        <div class="post_time">
          <p><i class="fa fa-clock-o"></i> <?=date('F j, Y',strtotime($value['date_create']));?></p>
        </div>
        <h4><?=$value['blog_name']?></h4>
        <p class="eps"><?=strip_tags($value['blog_detail'])?></p>
      </div>
    </div>
  </a>
</div>
<?php }} ?>
