<?php
include('../../../admin/inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action  = @$_POST['value'];
$id      = @$_POST['id'];
$required = 'required';
if($action == 'EDIT'){
  $btn = 'Update changes';
  $required = '';
  $sqls       = "SELECT * FROM t_blog WHERE blog_id = '$id'";
  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];

  $blog_id      = $rows[0]['blog_id'];
  $blog_img     = $rows[0]['blog_img'];
  $blog_name    = $rows[0]['blog_name'];
  $blog_detail  = $rows[0]['blog_detail'];
  $is_active    = $rows[0]['is_active'];


}
if($action == 'ADD'){
 $btn = 'Save changes';
}

?>

<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="blog_id" value="<?=$id?>">
<input type="hidden" name="user_id" value="<?=$_SESSION['member']['mem_id']?>">
<div class="modal-body" style="min-height:60vh;">
  <div class="row">
    <div class="col-md-9">
      <div class="form-group">
        <label>ชื่อบทความ</label>
        <input value="<?=@$blog_name?>" name="blog_name" type="text" class="form-control" placeholder="ชื่อบทความ" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control" style="width: 100%;" required>
          <option value="Y" <?= (@$is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
          <option value="N" <?= (@$is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>รายละเอียด</label>
        <div id="editorBlog"><?=@$blog_detail?></div>
        <input type="hidden" id="blog_detail" name="blog_detail">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>IMAGE</label>
        <input type="file" class="form-control" name="blog_img" onchange="readURL(this,'showImage')" <?=$required?>>
      </div>
    </div>
    <div class="col-md-6">
      <h4><strong>IMAGE</strong></h4>
      <div id="showImage">
        <?php if($action == 'EDIT'){ ?>
        <img width="300" src="../../images/blog/<?=$blog_img?>">
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;"  data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
</div>

<script type="text/javascript">
  var dataEditorBlog;
  ClassicEditor
    .create( document.querySelector( '#editorBlog' ), {
        image: {
          styles: [
            'alignLeft',
            'alignCenter',
            'alignRight',
          ],
          resizeOptions: [
                {
                    name: 'imageResize:original',
                    label: 'Original',
                    value: null
                },
                {
                    name: 'imageResize:50',
                    label: '50%',
                    value: '50'
                },
                {
                    name: 'imageResize:75',
                    label: '75%',
                    value: '75'
                }
            ],
          toolbar: [
                'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                '|',
                'imageTextAlternative'
            ]
        },
        table: {
            contentToolbar: [
                'tableColumn', 'tableRow', 'mergeTableCells',
                'tableProperties', 'tableCellProperties'
            ]
        },
        ckfinder: {
          // eslint-disable-next-line max-len
          uploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
        }
    } )
    .then( editor => {
      dataEditorBlog = editor;
    } )
    .catch( err => {
      console.error( err.stack );
    } );
</script>
