showData();
function showData(job="",location=""){
  $.post("service/showData.php",{job:job,location:location})
    .done(function( data ) {
      $('#show-data').html(data);
  });
}

$('#btnfindjob').on('click', function(event) {
  event.preventDefault();
  var alljob = $('#alljob').val();
  var alllocation = $('#alllocation').val();
  showData(alljob,alllocation);
});
