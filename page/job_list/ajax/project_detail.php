<section class="blog pdd-t-0">

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
        <form class="form-inline">
          <select class="form-control" id="alljob">
            <option value="">All</option>
            <?php
              $sql   = "SELECT * FROM t_category";
              $query = DbQuery($sql,null);
              $row   = json_decode($query,true);
              if($row['dataCount'] > 0){
                foreach ($row['data'] as $value) {
            ?>
            <option value="<?=$value['category_id']?>"><?=$value['category_name_th']?></option>
            <?php }} ?>
          </select>
          <select class="form-control select2" id="alllocation">
            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              <option value="">All Locations</option>
              <?php
                $sql   = "SELECT * FROM data_mas_add_province
                        WHERE PROVINCE_CODE != 00";
                $query = DbQuery($sql,null);
                $row   = json_decode($query,true);
                if($row['dataCount'] > 0){
                  foreach ($row['data'] as $value) {
              ?>
              <option value="<?=$value['PROVINCE_CODE']?>"><?=$value['PROVINCE_NAME']?></option>
              <?php }} ?>
          </select>
          <!-- Btn Find Job -->
          <button class="btn btn-primary" type="button" id="btnfindjob">
            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
              <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
            </svg>
            <i class="bi bi-search"></i> Find Job
          </button>
          <!-- Range -->
          <!-- <div style="margin-top:50px;" >
            <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="300000" data-slider-step="5" data-slider-value="[0,300000]" id="sl2" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="salaryrange">Salary: 0 - 300,000 (฿)</b><br />
          </div> -->
        </form>

        <div id="show-data"></div>




      </div>
    </div>
  </div>

</section>
