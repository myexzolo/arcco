<?php

include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');
  $str = '';
  $str .= $_POST['job']!=''?" AND j.category_id = '{$_POST['job']}'":"";
  $str .= $_POST['location']!=''?" AND j.PROVINCE_CODE = '{$_POST['location']}'":"";
  $sql = "SELECT * FROM t_job j , data_mas_add_province p , t_category c WHERE j.PROVINCE_CODE = p.PROVINCE_CODE AND j.category_id = c.category_id AND j.tj_status = 'A' AND j.is_acive = 'Y' $str";
  $query = DbQuery($sql,null);
  $row = json_decode($query,true);
  if($row['dataCount'] > 0){
?>
  <div class="card card-body">

  <?php
      foreach ($row['data'] as $value) {
  ?>
  <div class="row">
    <div class="jumbotron">
    <img src="../../images/imgJob/<?=$value['tj_img']?>">
    <h3><?=$value['tj_name']?></h3>
    <hr>
    <p><?=$value['PROVINCE_NAME']?></p>
    <p>budget: <?=$value['tj_budget_min'].' - '.$value['tj_budget_max']?> Baht</p>
    <p>หมวดหมู่: <?=$value['category_name_th']?></p>
    <br>
    <p><a class="btn btn-primary btn-lg" href="../job_view/?id=<?=$value['tj_id']?>" role="button" id="btnjd">อ่านเพิ่มเติม</a></p>
    </div>
  </div>
  <?php } ?>
</div>
</div>

<a class="btnmore" href="#">แสดงเพิ่มเติม..</a>
<?php }else{ ?>
<div class="alert alert-success">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>ไม่พบงานที่ค้นหา!</strong> กรุณากรอกข้อมูลที่ต้องการค้นหางาน
</div>
<?php } ?>
