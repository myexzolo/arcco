<section class="about">

  <div class="container">

    <div class="row">
      <div class="col-md-9 col-xs-12">
        <img class="img-full" src="../../images/service/post-01.jpg" alt="service">
        <div class="about-content">
          <h3 class="title">Data recovery</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>


        <div class="tab_bar_section">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Description</a>
            </li>
            <li role="presentation">
              <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Reviews (2)</a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">

              <div class="about-content mar-t">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac elementum elit. Morbi eu arcu ipsum. Aliquam lobortis accumsan quam ac convallis. Fusce elit mauris, aliquet at odio vel, convallis vehicula nisi. Morbi vitae porttitor dolor. Integer eget metus sem. Nam venenatis mauris vel leo pulvinar, id rutrum dui varius. Nunc ac varius quam, non convallis magna. Donec suscipit commodo dapibus.</p>
                <p>Vestibulum et ullamcorper ligula. Morbi bibendum tempor rutrum. Pelle tesque auctor purus id molestie ornare.Donec eu lobortis risus. Pellentesque sed aliquam lorem. Praesent pulvinar lorem vel mauris ultrices posuere. Phasellus elit ex, gravida a semper ut, venenatis vitae diam. Nullam eget leo massa. Aenean eu consequat arcu, vitae scelerisque quam. Suspendisse risus turpis, pharetra a finibus vitae, lobortis et mi.</p>
              </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
              <div class="card">
                <h4>Reviews (2)</h4>
              </div>
              <div class="product_review">
                <div class="commant-text row">
                  <div class="col-lg-2 col-md-2 col-sm-4">
                    <div class="profile"> <img class="img-responsive img-center" src="../../images/review/client1.png" alt="#"> </div>
                  </div>
                  <div class="col-lg-10 col-md-10 col-sm-8">
                    <h5>David</h5>
                    <p><span class="c_date">March 2, 2018</span> | <span><a rel="nofollow" class="comment-reply-link" href="#">Reply</a></span></p>
                    <span class="rating">
                      <i class="glyphicon glyphicon-star"></i>
                      <i class="glyphicon glyphicon-star"></i>
                      <i class="glyphicon glyphicon-star"></i>
                      <i class="glyphicon glyphicon-star"></i>
                      <i class="glyphicon glyphicon-star-empty"></i> </span>
                    <p class="msg">ThisThis book is a treatise on the theory of ethics, very popular during the Renaissance.
                      The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet.. </p>
                  </div>
                </div>
                <div class="commant-text row">
                  <div class="col-lg-2 col-md-2 col-sm-4">
                    <div class="profile"> <img class="img-responsive img-center" src="../../images/review/client1.png" alt="#"> </div>
                  </div>
                  <div class="col-lg-10 col-md-10 col-sm-8">
                    <h5>David</h5>
                    <p><span class="c_date">March 2, 2018</span> | <span><a rel="nofollow" class="comment-reply-link" href="#">Reply</a></span></p>
                    <span class="rating">
                      <i class="glyphicon glyphicon-star"></i>
                      <i class="glyphicon glyphicon-star"></i>
                      <i class="glyphicon glyphicon-star"></i>
                      <i class="glyphicon glyphicon-star"></i>
                      <i class="glyphicon glyphicon-star-empty"></i> </span>
                    <p class="msg">ThisThis book is a treatise on the theory of ethics, very popular during the Renaissance.
                      The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet.. </p>
                  </div>
                </div>

                <div class="btn-bottom text-right">
                  <button type="button" class="btn btn-default">Leave a Review</button>
                </div>

              </div>
            </div>
          </div>

        </div>

      </div>
      <div class="col-md-3 col-xs-12">
        <div class="about-content">
          <h3>ADD TO CART</h3>
          <div class="btn-bottom text-right">
            <button type="button" class="btn btn-default btn-block">ADD TO CART</button>
          </div>
        </div>
      </div>

    </div>
  </div>

</section>
