<section class="about">

  <div class="container">

    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#findjob" aria-controls="Find Job" role="tab" data-toggle="tab">
                Find Job
              </a>
            </li>
            <li role="presentation">
              <a href="#createjob" aria-controls="Create Job" role="tab" data-toggle="tab">
                Create Job
              </a>
            </li>
          </ul>
          <!-- Find Job -->
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="findjob">
              <div class="col-md-12">
                <div class="row">
                  <form class="form-inline">
                   <div class="form-group" id="jobtitle" >
                    <input type="jobtitle" class="form-control" id="jobtitle" placeholder="Job Title">
                   </div>
                  <select class="form-control" id="alljob">
                    <option>Buiding & Construction (All)</option>
                    <option>Architectural Services</option>
                    <option>Buiding & Construction / QS</option>
                    <option>Civil / Structural</option>
                  </select>
                  <select class="form-control select2" id="alllocation">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      <option>All Locations</option>
                      <?php
                        $sql   = "SELECT * FROM data_mas_add_province
                                WHERE PROVINCE_CODE != 00";
                        $query = DbQuery($sql,null);
                        $row   = json_decode($query,true);
                        if($row['dataCount'] > 0){
                          foreach ($row['data'] as $value) {
                      ?>
                      <option value="<?=$value['PROVINCE_CODE']?>"><?=$value['PROVINCE_NAME']?></option>
                      <?php }} ?>
                    </select>

                    <!-- Btn Find Job -->
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" id="btnfindjob">
                      <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                      </svg>
                      <i class="bi bi-search"></i> Find Job
                      </button>

                    <!-- Range -->
                    <div class="range-wrap">
                      <label for="formControlRange">Salary</label>
                      <input type="range" class="form-control-range" id="formControlRange" onInput="$('#rangeval').html($(this).val())">
                      <span id="rangeval">10,000<!-- Default value --></span>
                    </div>

                    <!-- collapse -->
                    </p>
                    <div class="collapse" id="collapseExample">
                      <div class="card card-body">

                      <div class="row">
                        <div class="jumbotron">
                        <img src="../../images/c1.jpg">
                        <h3>GAYSORN GROUP</h3>
                        <hr>
                        <p>Building & Property</p>
                        <p>Chonburi</p>
                        <p>Salary: 20,00 - 50,000 Baht</p>
                        <br>
                        <p><a class="btn btn-primary btn-lg" href="#" role="button" id="btnjd">อ่านเพิ่มเติม</a></p>
                      </div>
                      </div>

                      <div class="row">
                        <div class="jumbotron">
                        <img src="../../images/c2.png">
                        <h3>GAYSORN GROUP</h3>
                        <hr>
                        <p>Building & Property</p>
                        <p>Chonburi</p>
                        <p>Salary: 20,00 - 50,000 Baht</p>
                        <br>
                        <p><a class="btn btn-primary btn-lg" href="#" role="button" id="btnjd">อ่านเพิ่มเติม</a></p>
                      </div>
                      </div>

                      <div class="row">
                        <div class="jumbotron">
                        <img src="../../images/c3.png">
                        <h3>GAYSORN GROUP</h3>
                        <hr>
                        <p>Building & Property</p>
                        <p>Chonburi</p>
                        <p>Salary: 20,00 - 50,000 Baht</p>
                        <br>
                        <p><a class="btn btn-primary btn-lg" href="#" role="button" id="btnjd">อ่านเพิ่มเติม</a></p>
                      </div>
                      </div>


                    </div>
                  </div>
                    <!-- Close collapse -->

                  </form>
                </div>
              </div>
            </div>

            <!-- Create Job -->
            <div role="tabpanel" class="tab-pane" id="createjob">
              <div class="col-sm-6">
                <div class="form-group">
                 <input type="jobtitle" class="form-control" id="jobtitle2" placeholder="Job Title">
                </div>
              </div>
            <div class="col-sm-6">
              <div class="form-group">
               <select class="form-control" id="joblocation">
                <option>Job Location</option>
                <option>Accounting</option>
                <option>Admin & HR</option>
                <option>Media & Advertising</option>
                <option>Banking/Finance</option>
               </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="input-group">
               <span class="input-group-addon">Salary (฿)</span>
                <select class="form-control" id="salary1">
                  <option>10,000</option>
                  <option>20,000</option>
                  <option>30,000</option>
                  <option>40,000</option>
                  <option>50,000</option>
                  <option>More than 50,000</option>
                </select>
              </div>
            </div>
            <div class="col-sm-6">
             <div class="form-group">
              <select class="form-control" id="salary2">
                <option>10,000</option>
                <option>20,000</option>
                <option>30,000</option>
                <option>40,000</option>
                <option>50,000</option>
                <option>More than 50,000</option>
              </select>
             </div>
            </div>
        <div class="col-sm-12">
          <form>
            <div class="form-group">
              <textarea class="form-control" rows="5" id="comment" placeholder="Job description.."></textarea>
            </div>
          </form>
        </div>

        <!--Job Type-->
        <div class="col-sm-12">
          <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#" id="jobtype1">Full Time</a></li>
            <li role="presentation" class="active"><a href="#" id="jobtype2">Part Time</a></li>
            <li role="presentation" class="active"><a href="#" id="jobtype3">Contract</a></li>
            <li role="presentation" class="active"><a href="#" id="jobtype4">Internship</a></li>
            <div class="checkbox">
            <label>
              <input type="checkbox"> Work from home/Remote
            </label>
            </div>
          </ul>
        </div>

        <!--Choose img-->
        <div class="col-sm-12">
          <div class="form-group">
            <label>ภาพประกอบ</label>
              <input type="file">
          </div>
        </div>
        <div class="col-sm-12">
          <button type="button" class="btn btn-default" id="btnsubmit" data-toggle="modal" data-target="#myModal" onclick="alertD()">SUBMIT</button>
        </div>



            </div>

          </div>
        </div>
      </div>
    </div>
  </div>


</section>
