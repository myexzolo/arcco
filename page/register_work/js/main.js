
  // Alert Completed Create Job
  function alertD(){
    Swal.fire({
      title: 'Completed!',
      text: 'Your Job is Completed! ',
      icon: 'success',
      confirmButtonText: 'Homepage'
    })
  }

// Upload Pic Profile
  $(document).ready(function(){
        $("#wizard-picture").change(function(){
            readURL(this);
        });
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


//Range Salary
const range = document.getElementById("range"),
rangeV = document.getElementById("rangeV"),
setValue = () => {
  const newValue = Number(
      ((range.value - range.min) * 100) / (range.max - range.min)
    ),
    newPosition = 10 - newValue * 0.2;
  rangeV.innerHTML = `<span>${range.value}</span>`;
  rangeV.style.left = `calc(${newValue}% + (${newPosition}px))`;
};
document.addEventListener("DOMContentLoaded", setValue);
range.addEventListener("input", setValue);
  



