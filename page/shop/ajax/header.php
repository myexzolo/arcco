<div class="banner_section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="title-holder">
            <div class="title-holder-cell text-left">

              <?php
                $category_id = @$_GET['category_id'];
                $category_name_th = "ทีมงานเรา";
                $category_name_en = "service";
                if(isset($category_id)){
                  $sql = "SELECT * FROM t_category WHERE category_id = '$category_id'";
                  $query = DbQuery($sql,null);
                  $row =  json_decode($query,true)['data'][0];
                  $category_name_th = $row['category_name_th'];
                  $category_name_en = $row['category_name_en'];
                }
              ?>
              <h1 class="page-title"><?=$category_name_th?></h1>
              <p class="message-title"><?=$category_name_en?></p>
              <ol class="breadcrumb">
                <li><a class="link-write" href="../home/">หน้าแรก</a> </li>
                <li class="link-disable"><?=$category_name_th?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
