<section class="blog">

  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-xs-12">
        <div class="row">
          <?php
            $sql = "SELECT * FROM t_member tm , t_memberjob tj
                    WHERE tm.mem_id = tj.mem_id
                    AND tm.mem_type_user != 'C'";
            $query = DbQuery($sql,null);
            $row   = json_decode($query,true);
            if($row['dataCount'] > 0){
              foreach ($row['data'] as $value) {
                if(!empty($_GET['category_id'])){
                  $arr = explode(",",$value['category_id']);
                  if(in_array($_GET['category_id'],$arr)){
          ?>
          <div class="col-sm-4 col-xs-12">
            <a class="content-link" href="../shop_detail/?mem_id=<?=$value['mem_id']?>">
              <div class="blog-content">
                <div class="image-box">
                  <img class="img-responsive-tt" src="../../images/memberJob/imgThum/<?=empty($value['memberJob_banner'])?"post-01.jpg":$value['memberJob_banner']?>">
                </div>
                <h4><?=!empty($value['mem_nickname'])?$value['mem_nickname']:$value['mem_fname'].' '.$value['mem_lname']?></h4>
                <p>฿<?=number_format($value['rate_min'])?> – ฿<?=number_format($value['rate_max'])?></p>
              </div>
            </a>
          </div>
          <?php
              }
            } else{
          ?>
          <div class="col-sm-4 col-xs-12">
            <a class="content-link" href="../shop_detail/?mem_id=<?=$value['mem_id']?>">
              <div class="blog-content">
                <img class="img-responsive" src="../../images/blog/post-01.jpg">
                <h4><?=$value['mem_fname']?> <?=$value['mem_lname']?></h4>
                <p>฿<?=number_format($value['rate_min'])?> – ฿<?=number_format($value['rate_max'])?></p>
              </div>
            </a>
          </div>
          <?php }}} ?>
        </div>
      </div>
      <div class="col-sm-3 col-xs-12">
        <div class="about-content">
          <h3>TAG</h3>
          <div class="side_bar_blog">
            <div class="tags">
              <ul>
                <?php
                  $sql = "SELECT * FROM t_category ORDER BY category_seq ASC";
                  $query = DbQuery($sql,null);
                  $row =  json_decode($query,true);
                  if($row['dataCount'] > 0){
                    foreach ($row['data'] as $value) {
                ?>
                <li><a class="<?=@$category_id==$value['category_id']?"tag-active":""?> " href="../shop/?category_id=<?=$value['category_id']?>"><?=$value['category_name_th']?></a></li>
                <?php }} ?>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

</section>
