<section class="about">

  <div class="container">

    <div class="row">
      <div class="col-md-3 col-xs-12">
        <div class="order">
          <h3>Order list</h3>

          <div class="box">

            <div class="message">
              <h3>ProjectName <span class="pull-right">(รอยืนยัน)</span></h3>
              <p>Price</p>
            </div>
            <div class="message">
              <h3>ProjectName2 <span class="pull-right">(รอยืนยัน)</span></h3>
              <p>Price</p>
            </div>
            <div class="message">
              <h3>ProjectName3 <span class="pull-right">(รอยืนยัน)</span></h3>
              <p>Price</p>
            </div>

            <a href="#" class="view">view all</a>

          </div>

        </div>
      </div>
      <div class="col-md-9 col-xs-12">
        <div class="order">
          <h3>Order Detail</h3>
        </div>
        <div class="about-content">
          <h3>Project name</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
            standard dummy text ever since the 1500s, when an unknown printer took a galley..</p>
        </div>

        <div class="">
          <div class="btn-bottom pull-left">
            <a data-toggle="modal" data-target="#myModal" class="btn btn-default blue">PROJECT INFORMATION</a>
          </div>
          <div class="btn-bottom text-right">
            <a href="../shop_detail/" class="btn btn-default">PROFILE OWNER</a>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">PROJECT INFORMATION</h4>
              </div>
              <div class="modal-body">

                <h5>Project name</h5>

              </div>
              <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <table class="table">
          <thead>
            <tr>
              <td>No.</td>
              <td>Status.</td>
              <td>Detail.</td>
              <td>Date stamp.</td>
              <td>Tool</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>ปิดโปรเจค</td>
              <td>ปิดโปรเจค .......</td>
              <td>11/11/2563 16.30</td>
              <td><span class="glyphicon glyphicon-edit"></span></td>
            </tr>
            <tr>
              <td>2</td>
              <td>ชำระเงินงวดที่ 1</td>
              <td>ตรวจสอบงานโดย .......</td>
              <td>11/11/2563 16.30</td>
              <td><span class="glyphicon glyphicon-edit"></span></td>
            </tr>
            <tr>
              <td>3</td>
              <td>รับมอบงานงวดที่ 1</td>
              <td>รับมอบงานงวดที่ .......</td>
              <td>11/11/2563 16.30</td>
              <td><span class="glyphicon glyphicon-edit"></span></td>
            </tr>
            <tr>
              <td>4</td>
              <td>ส่งรายละเอียดเพิ่มเติม</td>
              <td>เพิ่มเติม .......</td>
              <td>11/11/2563 16.30</td>
              <td><span class="glyphicon glyphicon-edit"></span></td>
            </tr>
          </tbody>
        </table>

      </div>
    </div>
  </div>

</section>
