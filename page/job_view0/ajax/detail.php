<section class="blog pdd-t-0">

  <div class="container">
    <div class="row">
      <div class="col-md-12">
       
        
         
                    
                      <div class="card card-body">

                      <div class="row">
                        <div class="jumbotron">
                        <img src="../../images/c1.jpg">
                        <h3>Architect Officer/สถาปนิก</h3>
                        <hr>
                        <p>
                        <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> 
                        <span class="pp">ประเภทงาน:</span>
                        <span class="p3">สถาปนิก</span>
                        
                        <p>
                        <span class="glyphicon glyphicon-xbt" aria-hidden="true"></span> 
                        <span class="pp">เงินเดือน:</span>
                        <span class="p3">20,000 - 50,000</span> 

                        <p>      
                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>    
                        <span class="pp">สถานที่ปฏิบัติงาน:</span>
                        <span class="p3">เขตภาษีเจริญ กรุงเทพมหานคร</span>

                        <p class="p1">รายละเอียดงาน</p>
                          <ol>
                            <li>ออกแบบงานสถาปัตยกรรม งานบ้านพักอาศัย</li>
                            <li>ปรับแบบให้เหมาะสมกับความต้องการของลูกค้า</li>
                            <li>เคลียร์แบบ ขออนุญาตก่อสร้าง แบบสำหรับก่อสร้าง</li>
                  
                            <li>อายุ 25 ปี ขึ้นไป</li>
                            <li>วุฒิการศึกษา ปริญญาตรี สาขาสถาปัตยกรรมศาสตร์</li>
                            <li>ประสบการณ์ 2 ปีขึ้นไป</li>
                            <li>ทัศนคติและจรรยาบรรณที่ดีต่อวิชาชีพ มีบุคลิกและมนุษยสัมพันธ์ที่ดี</li>
                            <li>อดทน ตรงต่อเวลา รับผิดชอบต่องานและทีมอย่างเต็มที่</li>
                            <li>มีทักษะการใช้งานโปรแกรม Sketch up , AutoCAD , Microsoft Office และโปรแกรมที่เกี่ยวข้องกับงานออกแบบได้เป็นอย่างดี</li>

                            <li>วันทำงาน 5 วันทำงาน</li>
                            
                          </ol>

                          
                          <p class="p1">ติดต่อ</p>
                          <ul>
                            <li>ฝ่ายบุคคล</li>
                            <li>บริษัท เอส ซี แกรนด์ จำกัด</li>
                            <li>51,753 ถนนเพชรเกษม (ใกล้กับสถานีรถไฟฟ้าสถานีภาษีเจริญ และสถานีบางแค)</li>
                            <li>แขวงบางหว้า เขตภาษีเจริญ กรุงเทพมหานคร 10160</li>
                            <li>โทรศัพท์ : 02-066-6663</li>
                            <li>Line ID: scgrand_02</li>
                            <li>อีเมล : scgrand_02@gmail.com (อีเมลนี้ใช้สำหรับการติดต่อและสมัครงาน)</li>
                          </ul>   

                          


                        </div>
                      </div>  
                        </div>
                      </div>  
                      

                

        
          
      </div>
    </div>
  </div>

</section>


