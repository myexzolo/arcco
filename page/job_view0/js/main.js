$(function(){
    $('.content > p').dotdotdot({
        ellipsis: '...',
        wrap : 'word',
        watch : true
    });
});

function openModelcontent(value,id=""){
  $.post("service/formBlog.php",{value:value,id:id})
    .done(function( data ) {
      $('#myModalBlog').modal({backdrop:'static'});
      $('#show-formBlog').html(data);
  });
}

function goAboutme(){
  $.post("service/aboutMe.php")
    .done(function( data ) {
      $('#form-aboutMe').html(data);
  });
}

function dataBlog(){
  $.post("service/dataBlog.php")
    .done(function( data ) {
      $('#data-blog').html(data);
      $('.content > p').dotdotdot({
          ellipsis: '...',
          wrap : 'word',
          watch : true
      });
  });
}

function goCreateJob(){
  $.post("service/formcreateJob.php")
    .done(function( data ) {
      $('#create-Job').html(data);
  });
}

$('#formAddModuleBlog').on('submit', function(event) {
  $('#blog_detail').val(dataEditorBlog.getData());
  event.preventDefault();
  if ($('#formAddModuleBlog').smkValidate()) {
    $.ajax({
        url: 'service/blog.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        $('#myModalBlog').modal('toggle');
        dataBlog();
      }
    });
  }
});


$('.editProfile').on('submit', function(event) {
  event.preventDefault();
  if ($('.editProfile').smkValidate()) {
    $.ajax({
        url: 'service/editProfile.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        reloadLocation();
      }
    });
  }
});


$('.editProfileproject').on('submit', function(event) {
  $('#memberJob_detail').val(dataEditor.getData());
  event.preventDefault();
  if ($('.editProfileproject').smkValidate()) {
    $.ajax({
        url: 'service/editProfileproject.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        goAboutme();
      }
    });
  }
});

$('.createJob').on('submit', function(event) {
  $('#tj_detail').val(dataEditor2.getData());
  event.preventDefault();
  if ($('.createJob').smkValidate()) {
    $.ajax({
        url: 'service/createJob.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        goCreateJob();
      }
    });
  }
});



