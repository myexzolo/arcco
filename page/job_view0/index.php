<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PROLICE | ARCCO</title>

    <?php include "../../inc/css.php" ?>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/toggle.css">
    <link rel="stylesheet" href="css/timeline.css">
    <link rel="stylesheet" href="css/price-range.css">
    <link rel="stylesheet" href="css/toggle-button.css"> 

  </head>
  <body>
    <?php include "../../inc/nav.php" ?>

    <!-- body function -->
    <?php include "ajax/header.php" ?>
    <?php include "ajax/detail.php" ?>



    <?php //include "ajax/detail.php" ?>
    <!-- End body function -->

    <?php include "../../inc/footer.php" ?>
    <?php include "../../inc/js.php" ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.dotdotdot/4.1.0/dotdotdot.js" integrity="sha512-y3NiupaD6wK/lVGW0sAoDJ0IR2f3+BWegGT20zcCVB+uPbJOsNO2PVi09pCXEiAj4rMZlEJpCGu6oDz0PvXxeg==" crossorigin="anonymous"></script>
    <script src="../../js/matchHeight.js"></script>
    <script src="../../admin/ckeditor/ckeditor.js"></script>
    <script src="js/main.js"></script>
    <script src="js/price-range.js"></script>
    <script type="text/javascript">
      $('#sl2').slider();
    </script>
  
  </body>
</html>
