$('#image-pro').on('click', function() {
    $('#mem_image').trigger('click');
});

$('#image-bg').on('click', function() {
    $('#mem_image_bg').trigger('click');
});

function openModelUpgrade(value,id=""){
  $.post("service/formUpgrade.php")
    .done(function( data ) {
      $('#myModalUpgrade').modal({backdrop:'static'});
      $('#formAddModuleUpgrade').html(data);
  });
}

function openModelcontent(value,id=""){
  $.post("service/formBlog.php",{value:value,id:id})
    .done(function( data ) {
      $('#myModalBlog').modal({backdrop:'static'});
      $('#show-formBlog').html(data);
  });
}

function chkNumber(ele)
{
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
}

function cal()
{
  var installment_price = $("#installment_price").val();
  if($('#installment_price').smkValidate())
  {
      var installment_num   = $("#installment_num").val();
      if(installment_num == 1)
      {
        $("#installment_per_0").val("100");
        $("#installment_price_0").val(installment_price);

        // console.log($("#installment_per_"+x).val());
      }
      else
      {
        var pa = 0;
        for(var x=0; x < installment_num; x++)
        {
          var installment_per   = $("#installment_per_"+x).val();
          // console.log(installment_per);
          // if(installment_per != "")
          //  {
          //    installment_per = 0;
          //  }
          if((x+1) == installment_num && pa > 0)
          {
            // console.log(installment_price +", "+ pa);
            var pc = installment_price - pa;
            $("#installment_price_"+x).val(parseFloat(pc).toFixed(2));
            // console.log(pc);
            var pcc = (pc/installment_price)*100;
            $("#installment_per_"+x).val(parseInt(pcc));
          }else{
            var p = (installment_per * installment_price)/100;
            // console.log(installment_per +", "+ installment_price);
            pa += p;
            p = parseFloat(p).toFixed(2);
            $("#installment_price_"+x).val(p);
          }
        }
      }
      // var price_all = 0;
      // $( ".installmentPprice" ).each(function() {
      //    var price = $(this).val();
      //    if(isNaN(price))
      //    {
      //      price_all += price;
      //    }
      // });
  }
  else
  {
    $('#installment_price').focus();
  }
}

function setInstallment()
{
  var installment_num = $("#installment_num").val();
  var od_id = $("#od_id").val();

  $.post("service/installment.php",{od_id:od_id,installment_num:installment_num})
    .done(function( data ) {
      $('#show-installment').html(data);
      cal();
  });
}

function openModelDetail(id,type){
  $.post("service/formView.php",{id:id,te_user_type:type})
    .done(function( data ) {
      $('#myModalView').modal({backdrop:'static'});
      $('#show-view').html(data);
  });
}

function goAboutme(){
  $.post("service/aboutMe.php")
    .done(function( data ) {
      $('#form-aboutMe').html(data);
  });
}

function dataBlog(){
  $.post("service/dataBlog.php")
    .done(function( data ) {
      $('#data-blog').html(data);
      $('.content > p').dotdotdot({
          ellipsis: '...',
          wrap : 'word',
          watch : true
      });
  });
}
goCreateJob();
function goCreateJob(){
  $.post("service/formcreateJob.php")
    .done(function( data ) {
      $('#create-Job').html(data);
      projectListOne();
  });
}


$('#formInstallment').on('submit', function(event) {
  event.preventDefault();
  if ($('#formInstallment').smkValidate()) {
    $.ajax({
        url: 'service/approve.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      console.log(data);
      if(data.status == 'success'){
        $('#myModalInstallment').modal('toggle');
         dataProject();
      }
    });
  }
});


function postURL(url, multipart) {
  //alert("url:" + url);
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function postURL_blank(url) {
 var form = document.createElement("FORM");
 form.method = "POST";
 form.target = "_blank";
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

$('#formApprovePerson').on('submit', function(event) {
  event.preventDefault();
  if ($('#formApprovePerson').smkValidate()) {
    $.ajax({
        url: 'service/approvePerson.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        $('#myModalApprovePerson').modal('toggle');
        console.log(data);
        createInvoice(data.installment_id);
        createQuotation(data.tj_id);
        projectListOne();

      }
    });
  }
});

function createInvoice(installment_id)
{
  $.post("../../pdf/invoice/index.php",{installment_id:installment_id})
    .done(function( data ) {
      console.log(data);
  });
}

function createQuotation(tj_id)
{
  $.post("../../pdf/quotation/index.php",{tj_id:tj_id})
    .done(function( data ) {
      console.log(data);
  });
}

$('#formAddModuleBlog').on('submit', function(event) {
  $('#content').val(dataEditorBlog.getData());
  event.preventDefault();
  if ($('#formAddModuleBlog').smkValidate()) {
    $.ajax({
        url: 'service/blog.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        $('#myModalBlog').modal('toggle');
        dataBlog();
      }
    });
  }
});


$('.editProfile').on('submit', function(event) {
  event.preventDefault();
  if ($('.editProfile').smkValidate()) {
    $.ajax({
        url: 'service/editProfile.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        reloadLocation();
      }
    });
  }
});


$('.editProfileproject').on('submit', function(event) {
  $('#memberJob_detail').val(dataEditor.getData());
  event.preventDefault();
  if ($('.editProfileproject').smkValidate()) {
    $.ajax({
        url: 'service/editProfileproject.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        goAboutme();
      }
    });
  }
});

$('.createJob').on('submit', function(event) {
  // $('#tj_detail').val(dataEditor2.getData());
  event.preventDefault();
  if ($('.createJob').smkValidate()) {
    $.ajax({
        url: 'service/createJob.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        goCreateJob();
      }
    });
  }
});
dataProject();
function dataProject(){
  $.post("service/dataProject.php")
    .done(function( data ) {
      $('#data-project').html(data);
  });
}

projectListOne();
function projectListOne(){
  $.post("service/projectListOne.php")
    .done(function( data ) {
      $('#project-list-one').html(data);
  });
}

function openModeljob(id){
  $.post("service/previewJob.php",{id:id})
    .done(function( data ) {
      $('#myModalViewJob').modal({backdrop:'static'});
      $('#show-viewJob').html(data);
  });
}

// function approve(id){
//   $.smkConfirm({
//     text:'ต้องการรับโปรเจ็คนี้?',
//     accept:'ยืนยัน',
//     cancel:'ยกเลิก'
//   },function(res){
//     // Code here
//     if (res) {
//       $.post("service/approve.php",{id:id})
//         .done(function( data ) {
//           dataProject();
//           $.smkAlert({text: data.message,type: data.status});
//       });
//     }
//   });
// }



function approve(id){
  $.post("service/formInstallment.php",{id:id})
    .done(function( data ) {
      $('#myModalInstallment').modal({backdrop:'static'});
      $('#show-Installment').html(data);
  });
}


function getPersonJob(tj_id,mem_id,status,tj_status){
  $.post("service/showApprovePerson.php",{tj_id:tj_id,mem_id:mem_id})
    .done(function( data ) {
      $('#myModalApprovePerson').modal({backdrop:'static'});
      $('#show-ApprovePerson').html(data);
  });
}

function checkApprove()
{
  if($('#cbApprove').prop('checked'))
  {
    $('#smApprove').prop("disabled", false);
  }
  else
  {
    $('#smApprove').prop("disabled", true);
  }
}

// function getPersonJob(tj_id,mem_id,status,tj_status){
//   if(status == 'Y' && tj_status == 'A'){
//     $.smkConfirm({
//       text:'คุณต้องการเลือกคนนี้?',
//       accept:'ยืนยัน',
//       cancel:'ยกเลิก'
//     },function(res){
//       // Code here
//       if (res) {
//         $.post("service/approvePerson.php",{tj_id:tj_id,mem_id:mem_id})
//           .done(function( data ) {
//             projectListOne();
//             $.smkAlert({text: data.message,type: data.status});
//         });
//       }
//     });
//   }
// }

$('#formAddModuleView').on('submit', function(event) {
  // $('#te_text').val(dataEditorView.getData());
  event.preventDefault();
  if ($('#formAddModuleView').smkValidate()) {
    $.ajax({
        url: 'service/saveEvent.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      console.log(data);
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        $('#myModalView').modal('toggle');
        projectListOne();
      }
    });
  }
});


$('#formAddModuleUpgrade').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddModuleUpgrade').smkValidate()) {
    $.ajax({
        url: 'service/saveUpgrade.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      console.log(data);
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        $('#myModalUpgrade').modal('toggle');
        // projectListOne();
      }
    });
  }
});

function getInvoince(installment_id,te_text,te_user_type,tj_id){
  $.post("service/saveEvent.php",{ te_text:te_text, te_user_type:te_user_type, tj_id:tj_id })
    .done(function( data ) {
      createInvoice(installment_id);
      $('#myModalViewJob').modal('toggle');
  });
}
