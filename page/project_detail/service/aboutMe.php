<?php

include('../../../admin/inc/function/connect.php');

$sql   = "SELECT * FROM t_memberjob WHERE mem_id = '{$_SESSION['member']['mem_id']}'";
$query = DbQuery($sql,null);
$row   = json_decode($query,true)['data'][0];

$memberJob_portfolio = array_diff(explode("|", $row['memberJob_portfolio']),array(""));

?>

<input type="hidden" name="memberJob_id" value="<?=@$row['memberJob_id']?>">
<div class="col-md-3 col-xs-6">
  <div class="form-group">
    <label>ค่าจ้างขั้นต่ำ</label>
    <input type="text" name="rate_min" value="<?=@$row['rate_min']?>" OnKeyPress="return chkNumber(this)" class="form-control" style="text-align:right;" required>
  </div>
</div>
<div class="col-md-3 col-xs-6">
  <div class="form-group">
    <label>ค่าจ้างสูงสุด</label>
    <input type="text" name="rate_max" value="<?=@$row['rate_max']?>" OnKeyPress="return chkNumber(this)" class="form-control" style="text-align:right;" required>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label>เกี่ยวกับเรา</label>
    <div id="editor"><?=@$row['memberJob_detail']?></div>
    <input type="hidden" id="memberJob_detail" name="memberJob_detail">
  </div>
</div>
<div class="col-md-6 col-xs-12">
  <div class="form-group">
    <label>ภาพแบรนเนอร์</label>
    <input type="file" name="memberJob_banner" accept="image/png,image/jpeg" onchange="readURL(this,'showImg')" <?=empty($row['memberJob_banner'])?"required":""?>>
  </div>
  <div id="showImg">
    <?php if(!empty($row['memberJob_banner'])){ ?>
    <img width="100px" src="../../images/memberJob/imgThum/<?=@$row['memberJob_banner']?>">
    <?php } ?>
  </div>
</div>

<div class="col-md-6 col-xs-12">
  <div class="form-group">
    <label>ภาพผลงานของเรา</label>
    <input type="file" name="memberJob_portfolio[]" accept="image/png,image/jpeg" multiple>
  </div>
  <?php
    foreach ($memberJob_portfolio as $value_slide) {
  ?>
    <div class="" style="padding:0px;position: relative;display: inline-block;">
      <img width="100" class="img-responsive img-border" src="../../images/memberJob/portfolio/<?=$value_slide?>">
      <label class="toggle" style="position: absolute;top: 0px;left: 0px;">
        <input class="toggle__input" name="remove[]" value="<?=$value_slide?>" type="checkbox">
        <span class="toggle__label">
          <span class="toggle__text"></span>
        </span>
      </label>
    </div>
  <?php } ?>
</div>

<div class="col-xs-12">
  <div class="btn-bottom text-center" style="margin-top: 50px;">
    <button type="submit" class="btn btn-default">บันทึกแก้ไขโปรไฟล์โปรเจค</button>
  </div>
</div>


<script type="text/javascript">
  $(function () {
      init();
  })

  var dataEditor;

  var init = ( function() {
    var wysiwygareaAvailable = isWysiwygareaAvailable(),
      isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

    return function() {
      var editorElement = CKEDITOR.document.getById( 'editor' );
      if ( wysiwygareaAvailable ) {
        dataEditor = CKEDITOR.replace('editor', {
          toolbar: [{
              name: 'clipboard',
              items: ['Undo', 'Redo']
            },
            {
              name: 'basicstyles',
              items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
            },
            {
              name: 'links',
              items: ['Link', 'Unlink']
            },
            {
              name: 'paragraph',
              items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
            },
            {
              name: 'insert',
              items: ['Table']
            },
            {
              name: 'styles',
              items: ['Format', 'Font', 'FontSize']
            },
            {
              name: 'colors',
              items: ['TextColor', 'BGColor']
            },
            {
              name: 'align',
              items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
            }
          ],
      });
      } else {
        editorElement.setAttribute( 'contenteditable', 'true' );
        CKEDITOR.inline( 'editor' );
      }
    };

    function isWysiwygareaAvailable() {
      if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
        return true;
      }
      return !!CKEDITOR.plugins.get( 'wysiwygarea' );
    }
  } )();
</script>
