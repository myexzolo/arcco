<?php

include('../../../admin/inc/function/connect.php');

$mem_id =  $_SESSION['member']['mem_id'];

$sqlc = "SELECT * FROM t_memberjob WHERE mem_id = '$mem_id'";
$queryc = DbQuery($sqlc,null);
$rowc = json_decode($queryc,true);
if($rowc['dataCount'] > 0){
  $str = "AND category_id NOT IN ({$rowc['data'][0]['category_id']})";
}else{
  $str = '';
}

?>
<div class="modal-body">
<div class="row">
  <input type="hidden" name="mem_id" value="<?=$_SESSION['member']['mem_id']?>">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label>หมวดหมู่</label>
      <select class="form-control" name="category_id" required>
        <?php
          $sql   = "SELECT * FROM t_category WHERE 1=1 $str";
          $query = DbQuery($sql,null);
          $row   = json_decode($query,true);
          if($row['dataCount'] > 0){
            foreach ($row['data'] as $value) {
              $disabled = '';
              $sqls   = "SELECT * FROM t_upgrade_user WHERE category_id = '{$value['category_id']}' and mem_id = '$mem_id' and status = 'N' ";
              $querys = DbQuery($sqls,null);
              $rows   = json_decode($querys,true);
              if($rows['dataCount'] > 0){
                $disabled = 'disabled';
              }
        ?>
        <option <?=$disabled?> value="<?=$value['category_id']?>"><?=$value['category_name_th']?> <?=!empty($disabled)?"(กำลังตรวจสอบ)":""?></option>
        <?php }} ?>
      </select>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>อัพโหลดใบประกอบวิชาชีพ</label>
      <input type="file" class="form-control" name="tu_img">
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>อัพโหลดบัตรประชาชน</label>
      <input type="file" class="form-control" name="tu_IDCardimg">
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>อัพโหลดอแนบรูปถ่ายตนเอง</label>
      <input type="file" class="form-control" name="tu_Meimg">
    </div>
  </div>
  <div class="col-xs-12">
    <div class="form-group">
      <label>รายละเอียดข้อมูล</label>
      <textarea class="form-control" name="tu_detail" rows="8"></textarea>
    </div>
  </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;"  data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat">Upgrade user</button>
</div>
