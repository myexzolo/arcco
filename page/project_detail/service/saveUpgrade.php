<?php

include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');
include('../../../lib/line/notify.php');

$tu_img       = @$_FILES['tu_img'];
$tu_IDCardimg = @$_FILES['tu_IDCardimg'];
$tu_Meimg     = @$_FILES['tu_Meimg'];

if($tu_img['error'] == 0){
  $_POST['tu_img'] = uploadfile($tu_img,'../../../images/upgrade','upgrade_')['image'];
}
if($tu_IDCardimg['error'] == 0){
  $_POST['tu_IDCardimg'] = uploadfile($tu_IDCardimg,'../../../images/upgrade','upgradeIDCard_')['image'];
}
if($tu_Meimg['error'] == 0){
  $_POST['tu_Meimg'] = uploadfile($tu_Meimg,'../../../images/upgrade','upgradeMe_')['image'];
}

$sql        = DBInsertPOST($_POST,'t_upgrade_user');
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];
if(intval($row['errorInfo'][0]) == 0){

  $fullname   = $_SESSION['member']['mem_fname'].' '.$_SESSION['member']['mem_lname'];
  $mem_mobile = $_SESSION['member']['mem_mobile'];
  $mem_email  = $_SESSION['member']['mem_email'];

  $sql    = "SELECT * FROM t_category WHERE category_id = '{$_POST['category_id']}'";
  $query  = DbQuery($sql,null);
  $row    = json_decode($query, true)['data'][0];
  $category_name_th = $row['category_name_th'];
  $message_data['message'] = "\r\nแจ้งอัพเกรด User\r\nเป็น $category_name_th\r\n----------------\r\nข้อมูลติดต่อ\r\n$fullname\r\n$mem_mobile\r\n$mem_email\r\n----------------\r\nตรวจสอบข้อมูล\r\nhttps://arcco-th.com/admin/pages/upgrade_user/";
  if(send_notify_message($message_data)['status'] == 200){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'success')));
  }

}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
