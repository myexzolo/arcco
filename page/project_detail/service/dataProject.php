<?php
include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');
?>
<div class="table-responsive">
  <table class="table table-bordered">
  <thead>
    <tr class="text-center">
      <td>No.</td>
      <td>Img</td>
      <td>Img Ref</td>
      <td>Name</td>
      <td>BUDGET</td>
      <td>KICK OFF</td>
      <td>TOOL</td>
    </tr>
  </thead>
  <tbody>
    <?php
      $sql = "SELECT * FROM orders od , order_detail odd , t_member tm , t_job tjob
              WHERE od.o_id = odd.o_id AND od.tj_id = tjob.tj_id AND odd.mem_id = tm.mem_id
              AND odd.mem_id = '{$_SESSION['member']['mem_id']}' ORDER BY od.o_id DESC";
      $query = DbQuery($sql,null);
      $row = json_decode($query,true);
      if($row['dataCount'] > 0){
        foreach ($row['data'] as $key => $value) {
          if(!empty($value['img_ref'])){
              $img_ref = explode('|',$value['img_ref']);
          }
    ?>
    <tr class="text-center">
      <td><?=$key+1?></td>
      <td>
        <a href="../../images/imgJob/<?=$value['tj_img']?>" data-lightbox="ref">
          <img width="100" src="../../images/imgJob/<?=$value['tj_img']?>"/>
        </a>
      </td>
      <td style="max-width:150px">
        <?php if(!empty($img_ref)){ ?>
        <div class="owl-carousel owl-theme">
          <?php foreach ($img_ref as $keyref => $valueref) {?>
          <div class="item">
            <a href="../../images/imgJob/<?=$valueref?>" data-lightbox="ref">
              <img height="65" width="100" src="../../images/imgJob/<?=$valueref?>"/>
            </a>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
      </td>
      <td><?=$value['tj_name']?></td>
      <td><?=$value['tj_budget_min'].' - '.$value['tj_budget_max']?></td>
      <td><?=$value['tj_date_start_plan']?></td>
      <td>
        <button type="button" onclick="openModeljob(<?=$value['tj_id']?>)" class="btn btn-flat btn-info">view</button>
        <?php
          $sqlt = "SELECT * FROM t_job tj , orders od
                    WHERE tj.tj_id = od.tj_id
                    AND tj.mem_id = '{$_SESSION['member']['mem_id']}'
                    AND od.o_id = '{$value['o_id']}'
                    AND tj.tj_status = 'S'";
          $queryt = DbQuery($sqlt,null);
          $rowt = json_decode($queryt,true);
          if($rowt['dataCount'] > 0){
        ?>
        <button type="button" onclick="openModelDetail(<?=$rowt['data'][0]['tj_id']?>,'own')" class="btn btn-flat btn-warning">chat</button>
      <?php }else{ ?>
        <button type="button" onclick="openModelDetail(<?=$value['tj_id']?>,'own')" class="btn btn-flat btn-warning">chat</button>
        <button type="button" onclick="approve(<?=$value['od_id']?>)" <?=$value['status']!='N'?"disabled":""?> class="btn btn-flat btn-success">Approve</button>
      <?php } ?>
      </td>
    </tr>
  <?php
      }
    }else{
  ?>
  <tr>
    <td class="text-center" colspan="7">ไม่พบข้อมูล</td>
  </tr>
  <?php } ?>
  </tbody>
</table>
</div>

<script type="text/javascript">
$('.owl-carousel').owlCarousel({
    margin:5,
    stagePadding: 15,
    loop:false,
    dots:false,
    items:1
})
</script>
