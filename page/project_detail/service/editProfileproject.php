<?php
include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');

$memberJob_banner = @$_FILES['memberJob_banner'];
$memberJob_portfolio = @$_FILES['memberJob_portfolio'];
$remove       = @$_POST['remove'];

if(!empty($remove)){

  $sqls   = "SELECT memberJob_portfolio FROM t_memberjob WHERE memberJob_id = '{$_POST['memberJob_id']}'";
  $querys = DbQuery($sqls,null);
  $rows   = json_decode($querys, true)['data'][0];
  $memberJob_portfolio_r = array_diff(explode("|", $rows['memberJob_portfolio']),array(""));
  foreach ($remove as $value) {
    if (($key = array_search($value, $memberJob_portfolio_r)) !== false) {
      @unlink("../../../images/memberJob/portfolio/$value");
      unset($memberJob_portfolio_r[$key]);
    }
  }
  $str = implode("|",$memberJob_portfolio_r);
  $sql = "UPDATE t_memberjob SET memberJob_portfolio = '$str' WHERE memberJob_id = '{$_POST['memberJob_id']}'";
  DbQuery($sql,null);
}
unset($_POST['remove']);


if($memberJob_banner['error'] == 0){
  $sql = "SELECT memberJob_banner FROM t_memberjob WHERE memberJob_id = '{$_POST['memberJob_id']}'";
  $query = DbQuery($sql,null);
  $row = json_decode($query,true)['data'][0]['memberJob_banner'];
  @unlink("../../../images/memberJob/imgThum/$row");
  $_POST['memberJob_banner'] = uploadfileImage($memberJob_banner,"../../../images/memberJob/imgThum","imgThum_")['image'];
}

if(count($memberJob_portfolio) > 0 && $memberJob_portfolio['error'][0] == 0){
  $sql = "SELECT memberJob_portfolio FROM t_memberjob WHERE memberJob_id = '{$_POST['memberJob_id']}'";
  $query = DbQuery($sql,null);
  $row = json_decode($query,true)['data'][0]['memberJob_portfolio'];
  $l = empty($row)?"":$row."|";

  $_POST['memberJob_portfolio'] = $l.uploadfileMulti($memberJob_portfolio,"../../../images/memberJob/portfolio","port_")['image'];
}
$sql        = DBUpdatePOST($_POST,'t_memberjob','memberJob_id');
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];
if(intval($row['errorInfo'][0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
