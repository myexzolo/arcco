<?php

include('../../../admin/inc/function/connect.php');
$od_id   = isset($_POST['od_id'])?$_POST['od_id']:"";
$installment_num  = isset($_POST['installment_num'])?$_POST['installment_num']:"";


for($x=0; $x < $installment_num; $x++)
{
?>

<div class="col-md-12">
  <h4 class="text-addon">งวดงานที่ <?= $x+1?></h4>
  <input type="hidden" name="installment_no[]" value="<?=$x+1?>">
</div>
<div class="col-md-2">
  <div class="form-group">
    <label>แบ่งจ่าย (%)</label>
    <input type="text" id="installment_per_<?=$x?>" name="installment_per[]" class="form-control" data-smk-min="0" data-smk-max="100"  OnKeyPress="return chkNumber(this);" onkeyup="cal()" data-smk-msg="&nbsp;" style="text-align:right;" required>
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label>ราคา</label>
    <input type="text" id="installment_price_<?=$x?>" name="installment_price[]" class="form-control" value="" style="text-align:right;" readonly>
  </div>
</div>
<div class="col-md-7">
  <div class="form-group">
    <label>รายละเอียด</label>
    <textarea class="form-control" id="installment_detail_<?=$x?>" rows="5" name="installment_detail[]"></textarea>
  </div>
</div>

<script type="text/javascript">
  $('#installment_detail_<?=$x?>').keypress(function(event) {
    if (event.which == 13) {
      event.preventDefault();
      this.value = this.value + "\n";
    }
  });
</script>
<?php }?>
