<?php
include('../../../admin/inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action  = @$_POST['value'];
$id      = @$_POST['id'];
$required = 'required';
if($action == 'EDIT'){
  $btn = 'Update changes';
  $required = '';
  $sqls       = "SELECT * FROM t_article WHERE article_id = '$id'";
  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];

  $article_id  = $rows[0]['article_id'];
  $article_img = $rows[0]['article_img'];
  $title       = $rows[0]['title'];
  $content     = $rows[0]['content'];
  $is_active   = $rows[0]['is_active'];

}
if($action == 'ADD'){
 $btn = 'Save changes';
}

?>

<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="article_id" value="<?=$id?>">
<input type="hidden" name="user_id" value="<?=$_SESSION['member']['mem_id']?>">
<div class="modal-body" style="min-height:60vh;">
  <div class="row">
    <div class="col-md-9">
      <div class="form-group">
        <label>ชื่อบทความ</label>
        <input value="<?=@$title?>" name="title" type="text" class="form-control" placeholder="ชื่อบทความ" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control" style="width: 100%;" required>
          <option value="Y" <?= (@$is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
          <option value="N" <?= (@$is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>รายละเอียด</label>
        <div id="editorBlog"><?=@$content?></div>
        <input type="hidden" id="content" name="content">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>IMAGE</label>
        <input type="file" class="form-control" name="article_img" accept="image/png,image/jpeg" onchange="readURL(this,'showImage')" <?=$required?>>
      </div>
    </div>
    <div class="col-md-6">
      <h4><strong>IMAGE</strong></h4>
      <div id="showImage">
        <?php if($action == 'EDIT'){ ?>
        <img width="300" src="../../images/article/<?=$article_img?>">
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;"  data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
</div>

<script type="text/javascript">
  var dataEditorBlog;
  $(function () {
      init();
  })
  var init = ( function() {
    var wysiwygareaAvailable = isWysiwygareaAvailable(),
      isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

    return function() {
      var editorElement = CKEDITOR.document.getById( 'editorBlog' );
      if ( wysiwygareaAvailable ) {
        dataEditorBlog = CKEDITOR.replace('editorBlog', {
          toolbar: [{
              name: 'clipboard',
              items: ['PasteFromWord', '-', 'Undo', 'Redo']
            },
            {
              name: 'basicstyles',
              items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
            },
            {
              name: 'links',
              items: ['Link', 'Unlink']
            },
            {
              name: 'paragraph',
              items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
            },
            {
              name: 'insert',
              items: ['Image', 'Table']
            },
            {
              name: 'editing',
              items: ['Scayt']
            },
            '/',
            {
              name: 'styles',
              items: ['Format', 'Font', 'FontSize']
            },
            {
              name: 'colors',
              items: ['TextColor', 'BGColor', 'CopyFormatting']
            },
            {
              name: 'align',
              items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
            },
            {
              name: 'document',
              items: ['Templates', 'PageBreak', 'Source']
            }
          ],
        filebrowserBrowseUrl: '../../admin/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: '../../admin/ckfinder/ckfinder.html?type=Images',
        filebrowserImageUploadUrl: '../../admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
      });
      } else {
        editorElement.setAttribute( 'contenteditable', 'true' );
        CKEDITOR.inline( 'editorBlog' );
      }
    };

    function isWysiwygareaAvailable() {
      if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
        return true;
      }
      return !!CKEDITOR.plugins.get( 'wysiwygarea' );
    }
  } )();

</script>
