<?php

  include('../../../admin/inc/function/connect.php');

?>
<div class="col-xs-12">
  <div class="table-responsive" style="margin-top:20px;">
    <table class="table table-bordered">
    <thead>
      <tr class="text-center">
        <td>No.</td>
        <td>Img</td>
        <td>Img Ref</td>
        <td>Name</td>
        <td>Budget</td>
        <td>Kick off</td>
        <td>Owner (ดูใบเสนอราคา)</td>
        <td>View</td>
      </tr>
    </thead>
    <tbody>
  <?php
    $sql = "SELECT * FROM t_job j , data_mas_add_province p, t_category c , t_type_build tb , t_type_project tp
            WHERE j.PROVINCE_CODE = p.PROVINCE_CODE
            AND j.category_id = c.category_id
            AND j.ttb_id = tb.ttb_id
            AND j.ttp_id = tp.ttp_id
            AND j.cus_id = '{$_SESSION['member']['mem_id']}'";
    $query = DbQuery($sql,null);
    $row = json_decode($query,true);
    if($row['dataCount'] > 0){
      foreach ($row['data'] as $key => $value) {

        if(!empty($value['img_ref'])){
            $img_ref = explode('|',$row['data'][0]['img_ref']);
        }
  ?>
  <tr class="text-center">
    <td><?=$key+1?></td>
    <td>
      <a href="../../images/imgJob/<?=$value['tj_img']?>" data-lightbox="ref">
        <img width="100" src="../../images/imgJob/<?=$value['tj_img']?>"/>
      </a>
    </td>
    <td style="max-width:150px">
      <?php if(!empty($img_ref)){ ?>
      <div class="owl-carousel owl-theme">
        <?php foreach ($img_ref as $keyref => $valueref) {?>
        <div class="item">
          <a href="../../images/imgJob/<?=$valueref?>" data-lightbox="ref">
            <img height="65" width="100" src="../../images/imgJob/<?=$valueref?>"/>
          </a>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
    </td>
    <td><?=$value['tj_name']?></td>
    <td><?=$value['tj_budget_min'].' - '.$value['tj_budget_max']?></td>
    <td><?=$value['tj_date_start_plan']?></td>
    <td>
      <?php
        $str = $value['tj_status']!='A'?"AND status in ('Y','A')":"";
        $sqlo = "SELECT * FROM orders od , order_detail odd , t_member tm
                WHERE od.o_id = odd.o_id AND odd.mem_id = tm.mem_id
                AND od.tj_id = '{$value['tj_id']}' $str";
        $queryo = DbQuery($sqlo,null);
        $rowo = json_decode($queryo,true);
        if($rowo['dataCount'] > 0){
          foreach ($rowo['data'] as $valueo) {
      ?>
      <img width="50px" height="50px"
           onclick="getPersonJob('<?=$valueo['tj_id']?>','<?=$valueo['mem_id']?>','<?=$valueo['status']?>','<?=$value['tj_status']?>')"
           class="person <?=$valueo['status']=='N'?"overlay":""?>"
           src="../../images/profile/<?=$valueo['mem_image']?>">
      <?php }} ?>
    </td>
    <td>
      <a target="_blank" href="../project?tj_id=<?=$value['tj_id']?>" class="btn btn-flat btn-info">view</button>
    </td>
  </tr>
  <?php
      }
    }else{
  ?>
  <tr>
    <td class="text-center" colspan="8">ไม่พบข้อมูล</td>
  </tr>
  <?php } ?>
  </tbody>
  </table>
  </div>
</div>

<script type="text/javascript">
$('.owl-carousel').owlCarousel({
    margin:5,
    stagePadding: 15,
    loop:false,
    dots:false,
    items:1
})
</script>
