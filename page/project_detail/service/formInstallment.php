<?php

include('../../../admin/inc/function/connect.php');

$od_id   = isset($_POST['id'])?$_POST['id']:"";
$display = "";
?>
<style>
.box {
    margin-bottom: 30px;
    padding: 20px;
    box-shadow: 0px 0px 2px 0px #c2c2c2;
}

.header-title {
    position: relative;
}

.header-title h4 {
    color: #058;
    margin: 10px 0px 30px 0px;
    border-bottom: 3px solid;
    padding-bottom: 5px;
    font-weight: bold;
}

.text-addon {
    margin: 0;
    /* margin-top: -15px; */
    margin-bottom: 10px;
    font-size: 16px;
    font-weight: bold;
    text-decoration: underline;
}

.has-feedback .form-control {
    padding-right: 12px;
}

</style>
<input type="hidden" name="od_id" id="od_id" value="<?=$od_id?>">
<div class="modal-body">
  <div class="box">
    <div class="row">
      <div class="col-xs-12">
        <div class="header-title">
          <h4>ข้อมูลการเสนอราคา</h4>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">
        <div class="form-group">
          <label>ราคาที่เสนอ</label>
          <input type="text" name="installment_price_total" id="installment_price" data-smk-msg="&nbsp;" OnKeyPress="return chkNumber(this)" onkeyup="cal()" style="text-align:right;" class="form-control" required>
        </div>
      </div>
      <div class="col-md-3 col-xs-6">
        <div class="form-group">
          <label>งวดงาน</label>
          <select class="form-control" name="installment_num"  id="installment_num" onchange="setInstallment();" required>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
          </select>
        </div>
      </div>
    </div>
  </div>

  <div class="box">
    <div class="row">
      <div class="col-xs-12">
        <div class="header-title">
          <h4>ข้อมูลงวดงาน</h4>
        </div>
      </div>
      <div id="show-installment"></div>
    </div>
  </div>

  <div class="box">
    <div class="row">
      <div class="col-xs-12">
        <div class="header-title">
          <h4>ข้อมูลเพิ่มเติม</h4>
        </div>
      </div>
      <div class="col-md-12 col-xs-12">
        <div class="form-group">
          <textarea class="form-control" name="od_remark" rows="6"></textarea>
        </div>
      </div>
    </div>
  </div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>

<script>
$(function () {
  setInstallment();
})
</script>
