<?php
  include('../../../admin/inc/function/connect.php');
?>
<div class="modal-body" style="min-height:60vh;">
  <div class="row">
    <div class="col-md-5">
      <div class="chat">
        <div class="header-chat">
          <p>chat</p>
        </div>
        <div class="body-chat">
          <?php
            $sqll = "SELECT * FROM t_event_log WHERE tj_id = '{$_POST['id']}' ORDER BY date_create ASC";
            $queryl = DbQuery($sqll,null);
            $rowl = json_decode($queryl,true);
            if($rowl['dataCount'] > 0){
          ?>
          <?php foreach ($rowl['data'] as $valuel) { ?>

          <div class="body-chats <?=$valuel['te_user_type']?>">
            <div class="<?=$valuel['te_user_type']?>-chat">
              <?=ltrim($valuel['te_text'])?>
              <?php if(!empty($valuel['te_img'])){?>
                <h6 class="file-text">
                  <?php if(count(strpos($valuel['te_img'],".pdf")) > 0){ ?>
                    <a target="_blank" href="../../images/slipt/<?=$valuel['te_img']?>">
                      <?=$valuel['te_img']?>
                    </a>
                  <?php }else{ ?>
                    <a href="../../images/slipt/<?=$valuel['te_img']?>" data-lightbox="file">
                      <?=$valuel['te_img']?>
                    </a>
                  <?php }?>
                </h6>
              <?php } ?>
              <span><i><?=$valuel['date_create']?></i></span>
            </div>
          </div>
          <?php }} ?>
        </div>
      </div>

    </div>
    <div class="col-md-7">
      <?php
        $sql = "SELECT * FROM t_job WHERE tj_id = '{$_POST['id']}'";
        $query = DbQuery($sql,null);
        $row = json_decode($query,true)['data'][0];
      ?>
      <div class="form-group">
        <label>สนทนา</label>
        <div class="form-group">
          <textarea name="te_text" class="form-control" rows="6" required <?=$row['tj_status']=='E'?"disabled":""?>></textarea>
        </div>
        <!-- <input type="hidden" id="te_text" name="te_text"> -->
        <input type="hidden" name="te_user_type" value="<?=$_POST['te_user_type']?>">
        <input type="hidden" name="tj_id" value="<?=$_POST['id']?>">
      </div>
      <div class="form-group">
        <label>อัพโหลดไฟล์</label>
        <input type="file" accept="image/png,image/jpeg,application/pdf" <?=$row['tj_status']=='E'?"disabled":""?> class="form-control" name="te_img" onchange="readURL(this,'showImage')">
      </div>
      <div id="showImage"></div>

      <div class="timeline">
        <div class="sessions">
          <ul>
            <?php
              $sqll = "SELECT * FROM t_event_log WHERE tj_id = '{$_POST['id']}' ORDER BY date_create ASC";
              $queryl = DbQuery($sqll,null);
              $rowl = json_decode($queryl,true);
              if($rowl['dataCount'] > 0){
            ?>
            <?php
              foreach ($rowl['data'] as $valuel) {
                $text = 'ระบบแอดมิน';
                if($valuel['te_user_type'] == 'sys-own'){
                  $text = 'ผู้ว่าจ้าง';
                }else if($valuel['te_user_type'] == 'sys-cus'){
                  $text = 'เจ้าของโปรเจค';
                }
            ?>
            <li>
              <p>
                <?=ltrim($valuel['te_text'])?> (<?=$text?>)
                <?php if(count(strpos($valuel['te_img'],".pdf")) > 0){ ?>
                  <br />
                  <a target="_blank" href="../../images/slipt/<?=$valuel['te_img']?>">
                    <?=$valuel['te_img']?>
                  </a>
                <?php }else{ ?>
                  <br />
                  <a href="../../images/slipt/<?=$valuel['te_img']?>" data-lightbox="file">
                    <?=$valuel['te_img']?>
                  </a>
                <?php }?>
              </p>
              <span class="time"><?=$valuel['date_create']?></span>
            </li>
          <?php } ?>
          <?php } ?>
          </ul>
        </div>
      </div>

    </div>

  </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;"  data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat" <?=$row['tj_status']=='E'?"disabled":""?>>save changes</button>
</div>
