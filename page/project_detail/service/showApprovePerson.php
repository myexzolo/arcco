<?php

include('../../../admin/inc/function/connect.php');

$tj_id    = isset($_POST['tj_id'])?$_POST['tj_id']:"";
$mem_id   = isset($_POST['mem_id'])?$_POST['mem_id']:"";


$sql = "SELECT * FROM orders o, order_detail od
        WHERE o.tj_id = $tj_id AND o.o_id = od.o_id and mem_id = '$mem_id'";

$query = DbQuery($sql,null);
$json = json_decode($query,true);
$row  = $json['data'];

$status =  $row[0]['status'];
$o_id   =  $row[0]['o_id'];
$od_remark   =  $row[0]['od_remark'];

$display = "";
?>
<style>
.box {
    margin-bottom: 30px;
    padding: 20px;
    box-shadow: 0px 0px 2px 0px #c2c2c2;
}

.header-title {
    position: relative;
}

.header-title h4 {
    color: #058;
    margin: 10px 0px 30px 0px;
    border-bottom: 3px solid;
    padding-bottom: 5px;
    font-weight: bold;
}

.text-addon {
    margin: 0;
    /* margin-top: -15px; */
    margin-bottom: 10px;
    font-size: 16px;
    font-weight: bold;
    text-decoration: underline;
}

.has-feedback .form-control {
    padding-right: 12px;
}

</style>
<input type="hidden" name="tj_id" value="<?=$tj_id?>">
<input type="hidden" name="mem_id" value="<?=$mem_id?>">
<input type="hidden" name="o_id" value="<?=$o_id?>">
<div class="modal-body">
  <div class="box">
    <div class="row">
      <div class="col-xs-12">
        <div class="header-title">
          <h4>โปรไฟล์ผู้รับจ้าง</h4>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">
        <?php
          $sqlm = "SELECT * FROM t_member WHERE mem_id = '$mem_id'";
          $querym = DbQuery($sqlm,null);
          $row = json_decode($querym,true)['data'][0];
        ?>
        <div style="width: 100px;
    height: 100px;
    overflow: hidden;">
          <a href="../shop_detail/?mem_id=<?=$mem_id?>" target="_blank">
            <img class="img-full" style="margin: 0;" src="../../images/profile/<?=$row['mem_image']?>">
          </a>
        </div>

      </div>
    </div>
  </div>
  <div class="box">
    <div class="row">
      <div class="col-xs-12">
        <div class="header-title">
          <h4>ข้อมูลการเสนอราคา</h4>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">
        <div class="form-group">
          <label>ราคาที่เสนอ</label>
          <input type="text" readonly value="<?= @number_format($row[0]['installment_price_total'],2) ?>" style="text-align:right;" class="form-control" required>
        </div>
      </div>
      <div class="col-md-3 col-xs-6">
        <div class="form-group">
          <label>งวดงาน</label>
          <input type="text" readonly value="<?= @$row[0]['installment_num'] ?>" style="text-align:right;" class="form-control" required>
        </div>
      </div>
    </div>
  </div>
  <div class="box">
    <div class="row">
      <div class="col-xs-12">
        <div class="header-title">
          <h4>ข้อมูลงวดงาน</h4>
        </div>
      </div>
      <?php
          $od_id = @$row[0]['od_id'];

          $sqls = "SELECT * FROM order_installment
                  WHERE od_id = $od_id Order by installment_no ASC";

          $querys = DbQuery($sqls,null);
          $jsons = json_decode($querys,true);
          $rows  = $jsons['data'];
          $dataCount  = $jsons['dataCount'];

          for($x=0; $x < $dataCount; $x++)
          {
            $installment_id = $rows[$x]['installment_id'];
            $installment_no = $rows[$x]['installment_no'];
            if($installment_no == 1)
            {
              echo "<input type=\"hidden\" name=\"installment_id\" value=\"$installment_id\">";
            }
          ?>

          <div class="col-md-12">
            <h4 class="text-addon">งวดงานที่ <?= $rows[$x]['installment_no'] ?></h4>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label>แบ่งจ่าย (%)</label>
              <input type="text" readonly class="form-control" value="<?= $rows[$x]['installment_per'] ?>" style="text-align:right;" required>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>ราคา</label>
              <input type="text" readonly value="<?= @number_format($rows[$x]['installment_price'],2) ?>" style="text-align:right;" class="form-control" required>
            </div>
          </div>
          <div class="col-md-7">
            <div class="form-group">
              <label>รายละเอียด</label>
              <textarea class="form-control" rows="5" readonly><?= $rows[$x]['installment_detail'] ?></textarea>
            </div>
          </div>
          <?php }?>
          <div class="col-xs-12">
            <div class="header-title">
              <h4>ข้อมูลเพิ่มเติม</h4>
            </div>
          </div>
          <div class="col-md-12 col-xs-12">
            <div class="form-group">
              <textarea class="form-control" name="od_remark" rows="6" readonly><?=$od_remark?></textarea>
            </div>
          </div>
    </div>
  </div>
<div class="modal-footer">
  <?php if($status == "A"){ ?>
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
  <?php }else {?>
    <div class="checkbox pull-left">
      <label>
        <input type="checkbox" value="Y" id="cbApprove" onchange="checkApprove()"> ยืนยันการเลือกผู้รับจ้าง
      </label>
    </div>
    <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
    <button type="submit" class="btn btn-primary btn-flat" disabled id="smApprove" style="width:100px;" >บันทึก</button>
  <?php }?>
</div>
