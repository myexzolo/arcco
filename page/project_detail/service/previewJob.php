<?php
  include('../../../admin/inc/function/connect.php');

  $sql = "SELECT * FROM t_job j , data_mas_add_province p, t_category c , t_type_build tb , t_type_project tp
          WHERE j.PROVINCE_CODE = p.PROVINCE_CODE
          AND j.category_id = c.category_id
          AND j.ttb_id = tb.ttb_id
          AND j.ttp_id = tp.ttp_id
           AND j.tj_id = '{$_POST['id']}'";
  $query = DbQuery($sql,null);
  $row = json_decode($query,true);
  if($row['dataCount'] > 0){
    foreach ($row['data'] as $value) {

      $Quotation = "";
      $installmentInvoice = "";
      $tj_id =  $value['tj_id'];
      $tj_quotation =  $value['tj_quotation'];
      if($tj_quotation != ""){
        $Quotation = "<a href=\"../../pdf/output/$tj_quotation\" target=\"_blank\">$tj_quotation</a>";
      }
  ?>
  <div class="modal-body" style="min-height:60vh;">
    <div class="row">

      <div class="col-md-5 col-xs-12">
        <img class="img-full" src="../../images/imgJob/<?=$value['tj_img']?>"/>

        <div class="sub-img">

        </div>

        <?php
          $str = $value['tj_status']!='A'?"AND status in ('Y','A')":"";
          $sqlo = "SELECT * FROM orders od , order_detail odd , t_member tm
                  WHERE od.o_id = odd.o_id AND odd.mem_id = tm.mem_id
                  AND od.tj_id = '{$value['tj_id']}' $str";
          $queryo = DbQuery($sqlo,null);
          $rowo = json_decode($queryo,true);
          if($rowo['dataCount'] > 0){
            foreach ($rowo['data'] as $valueo) {
              $status = $valueo['status'];
              $od_id  = $valueo['od_id'];
              $fullname = $valueo['mem_fname'].' '.$valueo['mem_lname'];

              if($status == "A")
              {
                $sqli = "SELECT * FROM order_installment
                         WHERE od_id = $od_id and (status_pay is null or status_pay in ('P','Y'))
                         order by installment_no ASC";

               $queryi = DbQuery($sqli,null);
               $rowoi = json_decode($queryi,true);

               if($rowoi['dataCount'] > 0)
               {
                 $installmentInvoice = '';
                 foreach ($rowoi['data'] as $keyoi => $valueoi) {
                   $installment_no = $valueoi['installment_no'];
                   $installment_id = $valueoi['installment_id'];
                   $installment_invoice = $valueoi['installment_invoice'];

                   $pathPay = "../../page/pay/index.php?installment_id=$installment_id";
                   $status_pay = $valueoi['status_pay']=='Y'?"ชำระเงินเรียบร้อย":"รอการชำระเงิน";
                   $installmentInvoice .= "<b>งวดที่  $installment_no ($status_pay)</b>"."<br /><a href=\"../../pdf/output/$installment_invoice\" target=\"_blank\">$installment_invoice</a><br />";
                   // $installmentInvoice .= "<br /><a onclick=\"postURL('$pathPay')\" style=\"cursor:pointer\"><b>ชำระเงิน</b></a><br />";
                 }
                // }
                 // if($rowoi['data'][0]['installment_invoice'] != "")
                 // {
                 //   $installment_no = $rowoi['data'][0]['installment_no'];
                 //   $installment_id = $rowoi['data'][0]['installment_id'];
                 //   $installment_invoice = $rowoi['data'][0]['installment_invoice'];
                 //
                 //   $pathPay = "../../page/pay/index.php?installment_id=$installment_id";
                 //   $status_pay = $rowoi['data'][0]['status_pay']=='Y'?"ชำระเงินเรียบร้อย":"รอการชำระเงิน";
                 //   $installmentInvoice = "<b>งวดที่  $installment_no ($status_pay)</b>"."<br /><a href=\"../../pdf/output/$installment_invoice\" target=\"_blank\">$installment_invoice</a>";
                 //   $installmentInvoice .= "<br /><a onclick=\"postURL('$pathPay')\" style=\"cursor:pointer\"><b>ชำระเงิน</b></a><br />";
                 // }
               }
              }
            }
          }
        ?>

        <div class="chat">
          <div class="header-chat">
            <p onclick="openModelDetail('<?=$valueo['tj_id']?>','cus')">chat</p>
          </div>
          <div class="body-chat">
            <?php
              if(isset($valueo)){
                $sqll = "SELECT * FROM t_event_log WHERE tj_id = '{$valueo['tj_id']}' ORDER BY date_create ASC";
                $queryl = DbQuery($sqll,null);
                $rowl = json_decode($queryl,true);
                if($rowl['dataCount'] > 0){
            ?>
            <?php foreach ($rowl['data'] as $valuel) { ?>

            <div class="body-chats <?=$valuel['te_user_type']?>">
              <div class="<?=$valuel['te_user_type']?>-chat">
                <?=ltrim($valuel['te_text'])?>
                <?php if(!empty($valuel['te_img'])){?>
                  <h6 class="file-text">
                    <?php if(count(strpos($valuel['te_img'],".pdf")) > 0){ ?>
                      <a target="_blank" href="../../images/slipt/<?=$valuel['te_img']?>">
                        <?=$valuel['te_img']?>
                      </a>
                    <?php }else{ ?>
                      <a href="../../images/slipt/<?=$valuel['te_img']?>" data-lightbox="file">
                        <?=$valuel['te_img']?>
                      </a>
                    <?php }?>
                  </h6>
                <?php } ?>
                <span><i><?=$valuel['date_create']?></i></span>
              </div>
            </div>
            <?php } ?>

            <?php } ?>
            <?php } ?>
          </div>
        </div>

      </div>
      <div class="col-md-7 col-xs-12">
        <div class="about-content">
          <table class="table mar-t-30">
            <tbody>
              <tr>
                <td width="20%">Project :</td>
                <td width="80%"><?=$value['tj_name']?></td>
              </tr>
              <tr>
                <td>Detail :</td>
                <td><?=$value['tj_detail']?></td>
              </tr>
              <tr>
                <td>Location :</td>
                <td><?=$value['PROVINCE_NAME']?></td>
              </tr>
              <tr>
                <td>Owner :</td>
                <td><?=$fullname?></td>
              </tr>
              <tr>
                <td>Service :</td>
                <td><?=$value['category_name_th']?></td>
              </tr>
              <tr>
                <td>ProjectType :</td>
                <td><?=$value['ttp_id']=='6'?$value['tj_type_name']:$value['ttp_name']?></td>
              </tr>
              <tr>
                <td>Type :</td>
                <td><?=$value['ttb_name']?></td>
              </tr>
              <tr>
                <td>Quotation :</td>
                <td><?=$Quotation?></td>
              </tr>
              <tr>
                <td>Installment :</td>
                <td>
                  <?=$installmentInvoice?>
                </td>
              </tr>
            </tbody>
          </table>

          <div class="btn-invoice">
            ออกใบแจ้งหนี้
            <?php
              $sqlin = "SELECT * FROM order_installment WHERE od_id = $od_id order by installment_no ASC";
              $queryin = DbQuery($sqlin,null);
              $rowin = json_decode($queryin,true);
              if($rowin['dataCount'] > 0){
              foreach ($rowin['data'] as $keyin => $valuein) {

                $te_text = 'แจ้งงวดชำระเงินงวดที่ '.$valuein['installment_no'].'<br />สถานะการชำระเงิน (รอการชำระเงิน)';
                $te_user_type = 'sys-own';
                $tj_id = $tj_id;
            ?>
            <button class='btn btn-arcco btn-xs' onclick="getInvoince(<?=$valuein['installment_id']?>,'<?=$te_text?>','<?=$te_user_type?>','<?=$tj_id?>')" type='button' <?=$valuein['status_pay']!=null?"disabled":""?>>งวดที่ <?=$valuein['installment_no']?></button>
            <?php } ?>
            <?php } ?>
            <br />
          </div>
          <?php
            $text = '';
            $date = '';
            switch ($value['tj_status']) {
              case 'A':
                $text = 'Request Person';
                break;
              case 'S':
                $text = 'Start Project';
                break;
              case 'E':
                $text = 'Close Project';
                break;
              default:
                $text = 'No response';
                break;
            }
          ?>
          <span class="badge badge-dark">Status</span>
          <span class="badge badge-secondary"><?=$text.' '.$date?></span>
        </div>

        <div class="timeline">
          <div class="sessions">
            <ul>
              <?php
                if(isset($valueo)){
                  $sqll = "SELECT * FROM t_event_log WHERE tj_id = '{$valueo['tj_id']}' ORDER BY date_create ASC";
                  $queryl = DbQuery($sqll,null);
                  $rowl = json_decode($queryl,true);
                  if($rowl['dataCount'] > 0){
              ?>
              <?php
                foreach ($rowl['data'] as $valuel) {
                  $text = 'ระบบแอดมิน';
                  if($valuel['te_user_type'] == 'sys-own'){
                    $text = 'ผู้ว่าจ้าง';
                  }else if($valuel['te_user_type'] == 'sys-cus'){
                    $text = 'เจ้าของโปรเจค';
                  }
              ?>
              <li>
                <p>
                  <?=ltrim($valuel['te_text'])?> (<?=$text?>)
                  <?php if(count(strpos($valuel['te_img'],".pdf")) > 0){ ?>
                    <br />
                    <a target="_blank" href="../../images/slipt/<?=$valuel['te_img']?>">
                      <?=$valuel['te_img']?>
                    </a>
                  <?php }else{ ?>
                    <br />
                    <a href="../../images/slipt/<?=$valuel['te_img']?>" data-lightbox="file">
                      <?=$valuel['te_img']?>
                    </a>
                  <?php }?>
                </p>
                <span class="time"><?=$valuel['date_create']?></span>
              </li>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            </ul>
          </div>
        </div>

      </div>

    </div>
  </div>
  <?php }} ?>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-flat" style="width:100px;"  data-dismiss="modal">Close</button>
  </div>
