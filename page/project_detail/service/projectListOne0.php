<?php

  include('../../../admin/inc/function/connect.php');

  $sql = "SELECT * FROM t_job j , data_mas_add_province p, t_category c , t_type_build tb , t_type_project tp
          WHERE j.PROVINCE_CODE = p.PROVINCE_CODE
          AND j.category_id = c.category_id
          AND j.ttb_id = tb.ttb_id
          AND j.ttp_id = tp.ttp_id
          AND j.cus_id = '{$_SESSION['member']['mem_id']}'";

  $query = DbQuery($sql,null);
  $row = json_decode($query,true);


  if($row['dataCount'] > 0){

    foreach ($row['data'] as $value) {
      $Quotation = "";
      $installmentInvoice = "";

      $tj_quotation =  $value['tj_quotation'];
      if($tj_quotation != "")
      {
        $Quotation = "<br><br><b>ใบเสนอราคา </b>"."<a href=\"../../pdf/output/$tj_quotation\" target=\"_blank\"><img width=\"30px\" src=\"../../images/pdf.png\"/></a>";
      }
?>
<div class="col-md-4 col-xs-12">
  <img class="img-full" src="../../images/imgJob/<?=$value['tj_img']?>"/>

  <div class="person-project">
    <h3>ผู้สนใจ</h3>
    <div class="row">
    <?php
      $str = $value['tj_status']!='A'?"AND status in ('Y','A')":"";
      $sqlo = "SELECT * FROM orders od , order_detail odd , t_member tm
              WHERE od.o_id = odd.o_id AND odd.mem_id = tm.mem_id
              AND od.tj_id = '{$value['tj_id']}' $str";
      $queryo = DbQuery($sqlo,null);
      $rowo = json_decode($queryo,true);
      if($rowo['dataCount'] > 0){
        foreach ($rowo['data'] as $valueo) {
          $status = $valueo['status'];
          $od_id  = $valueo['od_id'];

          if($status == "A")
          {
            $sqli = "SELECT * FROM order_installment
                     WHERE od_id = $od_id and (status_pay is null or status_pay in ('P'))
                     order by installment_no ASC
                     limit 0,1";

           $queryi = DbQuery($sqli,null);
           $rowoi = json_decode($queryi,true);

           if($rowoi['dataCount'] > 0)
           {
             if($rowoi['data'][0]['installment_invoice'] != "")
             {
               $installment_no = $rowoi['data'][0]['installment_no'];
               $installment_id = $rowoi['data'][0]['installment_id'];
               $installment_invoice = $rowoi['data'][0]['installment_invoice'];

               $pathPay = "../../page/pay/index.php?installment_id=$installment_id";

               $installmentInvoice = "<br><br><b>งวดที่  $installment_no </b>"."<a href=\"../../pdf/output/$installment_invoice\" target=\"_blank\"><img width=\"30px\" src=\"../../images/pdf.png\"/></a>";
               $installmentInvoice .= "&nbsp;&nbsp;<a onclick=\"postURL('$pathPay')\" style=\"cursor:pointer\"><b>ชำระเงิน</b></a>";
             }
           }
          }
    ?>
      <div class="col-sm-4">
        <img width="100%" onclick="getPersonJob('<?=$valueo['tj_id']?>','<?=$valueo['mem_id']?>','<?=$valueo['status']?>','<?=$value['tj_status']?>')" class="person <?=$valueo['status']=='N'?"overlay":""?>" src="../../images/profile/<?=$valueo['mem_image']?>">
      </div>
    <?php }} ?>
    </div>
  </div>
</div>
<div class="col-md-8 col-xs-12">
  <div class="about-content">
    <h3><?=$value['tj_name']?></h3>
    <?=$value['tj_detail']?>
    <p>
      ระยะเวลาเริ่มโครงการ <?=$value['tj_date_start_plan']?><br />
      การชำระเงิน <?=empty($value['count_pay'])?"-":$value['count_pay']?> งวด<br />
      สถานที่ <?=$value['PROVINCE_NAME']?><br />
      การว่าจ้าง <?=$value['category_name_th']?><br />
      ประเภท <?=$value['ttb_name']?><br />
      ประเภทโครงการ <?=$value['ttp_id']=='6'?$value['tj_type_name']:$value['ttp_name']?><br />
      <?= $Quotation ?>
      <?= $installmentInvoice ?>
    </p>
    <?php
      $text = '';
      $date = '';
      switch ($value['tj_status']) {
        case 'A':
          $text = 'Request Person';
          break;
        case 'S':
          $text = 'Start Project';
          break;
        default:
          $text = 'No response';
          break;
      }
    ?>
    <span class="badge badge-dark">Status</span>
    <span class="badge badge-secondary"><?=$text.' '.$date?></span>
    <span class="badge badge-secondary view" onclick="openModelDetail('<?=$valueo['tj_id']?>','cus')">chat</span>
    <!-- <span class="badge badge-secondary">In Progress : 1-31 December 2021</span> -->
  </div>
  <?php
    if(isset($valueo)){
      $sqll = "SELECT * FROM t_event_log WHERE tj_id = '{$valueo['tj_id']}' ORDER BY date_create DESC";
      $queryl = DbQuery($sqll,null);
      $rowl = json_decode($queryl,true);
      if($rowl['dataCount'] > 0){
  ?>
  <div class="timeline">
    <ul class="sessions">
      <?php foreach ($rowl['data'] as $valuel) { ?>
      <li>
        <div class="time"><?=$valuel['date_create']?></div>
        <?=$valuel['te_text']?>
        <?php if($valuel['te_img'] != null){ ?>
          <h6><a href="../../images/slipt/<?=$valuel['te_img']?>"><?=$valuel['te_img']?></a></h6>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>
  <?php } ?>
</div>
<?php }} ?>
