<?php

include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');
require_once '../../../admin/PHPMailer/PHPMailerAutoload.php';
include('../../../lib/line/notify.php');

$od_id = $_POST['od_id'];

$arr['od_id']                   = $od_id;
$arr['od_remark']               = $_POST['od_remark'];
$arr['installment_num']         = $_POST['installment_num'];
$arr['installment_price_total'] = $_POST['installment_price_total'];
$arr['status']                  = 'Y';
$sql        = DBUpdatePOST($arr,'order_detail','od_id');

$installment_no = $_POST['installment_no'];
if(count($installment_no) > 0)
{
  // echo "<pre>";
  // print_r($_POST);

  $sqlC = "DELETE FROM order_installment where od_id = $od_id;";
  DbQuery($sqlC,null);
}
for($x=0;$x < count($installment_no) ; $x++)
{
  $arrIn['od_id']               = $od_id;
  $arrIn['installment_no']      = $_POST['installment_no'][$x];
  $arrIn['installment_per']     = $_POST['installment_per'][$x];
  $arrIn['installment_price']   = $_POST['installment_price'][$x];
  $arrIn['installment_detail']  = $_POST['installment_detail'][$x];

  $sql  .= DBInsertPOST($arrIn,'order_installment');
}

// echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];
if(intval($row['errorInfo'][0]) == 0){

  $sqlm = "SELECT m.mem_fname , m.mem_lname , m.mem_email , m.line_user_id FROM
            t_job j,t_member m,order_detail od, orders o
            WHERE od.o_id = o.o_id AND o.tj_id = j.tj_id AND j.cus_id = m.mem_id
            AND od.od_id = '$od_id'";
  $querym      = DbQuery($sqlm,null);
  $rowm        = json_decode($querym, true)['data'][0];
  $fullname = $rowm['mem_fname'].' '.$rowm['mem_lname'];
  $nameFrom = $_SESSION['member']['mem_fname'].' '.$_SESSION['member']['mem_lname'];
  $arr_mail['email'] = $rowm['mem_email'];
  $arr_mail['title'] = '[ARCCO] NOTIFICATION Alert : ​Quotation';
  $arr_mail['message'] = '
<p>Hi, '.$fullname.'<br />
<b>"'.$nameFrom.'"</b> has send Quotation. Please check menu <b>"Hiring project"</b> </p>
<p><a href="https://arcco-th.com/page/project_detail/?action=hiring">https://arcco-th.com/page/project_detail/?action=hiring</a></p>
<p>Received thie message by arcco.<br />
You may have received this email in error<br />
because another customer entered this email address  by arcco.<br />
if you received this message by arcco. please delete  this email</p>
<p>________________</p>
<p>arcco Team.<br />
________________</p>
';
  if(mailsendMail($arr_mail) == 200){
    sendLineMessages($rowm['line_user_id'],ltrim(strip_tags($arr_mail['message'])));
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'success and not send mail')));
  }

}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
