<?php
  $bg_image = isset($_SESSION['member']['mem_image_bg'])?"../../images/profile/{$_SESSION['member']['mem_image_bg']}":"../../images/bg/banner.jpg";

?>
<div class="banner_section" id="banner_section" style="background: url(<?=$bg_image?>);">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="title-holder">
            <div class="title-holder-cell text-left">
              <h1 class="page-title"><?=!empty($_SESSION['member']['mem_nickname'])?$_SESSION['member']['mem_nickname']:$_SESSION['member']['mem_fname'].' '.$_SESSION['member']['mem_lname'];?><span onclick="logout('../../page/home/')" class="logout">ออกจากระบบ</span></h1>
              <!-- <p class="message-title">กรอกข้อมูลโครงการของคุณ</p> -->
              <ol class="breadcrumb">
                <li>Profile</li>
                <li>Project</li>
                <li>Project Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
