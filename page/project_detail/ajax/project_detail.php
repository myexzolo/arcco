<section class="blog pdd-t-0">
  <div class="container">
    <div class="blog-profile">
      <div class="box-icon">
        <span id="image-bg" class="glyphicon glyphicon-camera"></span>
      </div>
      <!-- <div id="showimg"> -->
      <img width="150" height="150" id="image-pro" src="../../images/profile/<?=@$_SESSION['member']['mem_image']?>" alt="profile">
      <!-- </div> -->
      <?php
        if(!isset($_SESSION['member']['mem_type_user']))
        {
          session_destroy();
          exit("<script>window.location='../../page/home/'</script>");
        }

        $action = isset($_GET['action'])?$_GET['action']:"";
      ?>
      <div class="profile-name">
        <h2>
          <?=!empty($_SESSION['member']['mem_nickname'])?$_SESSION['member']['mem_nickname']:$_SESSION['member']['mem_fname'].' '.$_SESSION['member']['mem_lname'];?>
        </h2>
        <?php
          switch ($_SESSION['member']['mem_type_user']) {
            case 'B':
              $text = 'ผู้รับงาน';
              break;
            case 'CB':
              $text = 'ผู้ใช้งานทั่วไปและผู้รับงาน';
              break;
            default:
              $text = 'ผู้ใช้งานทั่วไป';
              break;
          }
        ?>
        <p><?=$text?></p>
        <?php
          $arr_category_name_th = array();

          $sqls = "SELECT category_id FROM t_memberjob WHERE mem_id = '{$_SESSION['member']['mem_id']}'";
          $querys = DbQuery($sqls,null);
          $jsons = json_decode($querys,true);
          $jsons = json_decode($querys,true);
          if($jsons['dataCount'] > 0){
            $category_id = $jsons['data'][0]['category_id'];

            $sqls = "SELECT * FROM t_category WHERE category_id IN ($category_id)";
            $querys = DbQuery($sqls,null);
            $jsons = json_decode($querys,true);
            if($jsons['dataCount'] > 0){
              foreach ($jsons['data'] as $value) {
                $arr_category_name_th[] = $value['category_name_th'];
              }
        ?>
        <p style="margin-top: -12px;"><?=implode(",",$arr_category_name_th);?></p>
        <?php } ?>
        <?php } ?>
      </div>

      <div class="btn-bottom text-right btn-0">
        <button type="button" onclick="openModelUpgrade()" class="btn btn-default">อัพเกรด</button>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?=empty($action)?"active":""?>">
              <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                User Info
              </a>
            </li>
            <?php
              $sql = "SELECT * FROM t_memberjob WHERE mem_id = '{$_SESSION['member']['mem_id']}' AND is_active = 'Y'";
              $query = DbQuery($sql,null);
              $json = json_decode($query,true);
              if($json['dataCount'] > 0){
            ?>
            <li role="presentation" onclick="goAboutme();">
              <a href="#profileproject" aria-controls="profileproject" role="tab" data-toggle="tab">
                My Profile
              </a>
            </li>
            <li role="presentation" class="<?=$action=='project'?"active":""?>" onclick="dataProject();">
              <a href="#profileprojectowner" aria-controls="profileprojectowner" role="tab" data-toggle="tab">
                My project
              </a>
            </li>
            <?php } ?>
            <li role="presentation" class="<?=$action=='hiring'?"active":""?>" onclick="goCreateJob();">
              <a href="#project" aria-controls="project" role="tab" data-toggle="tab">
                Hiring project
              </a>
            </li>
            <li role="presentation" onclick="dataBlog();">
              <a href="#blog" aria-controls="blog" role="tab" data-toggle="tab">
                Block & Content
              </a>
            </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content" style="min-height: 28vh;">
            <div role="tabpanel" class="tab-pane <?=empty($action)?"active":""?>" id="home">
              <div class="profile-input">
                <div class="row">
                  <div class="col-md-8">
                    <!-- <form class="formLogin" action="service/editProfile.php" method="post" enctype="multipart/form-data" novalidate> -->
                    <form class="formLogin editProfile" novalidate>
                      <div class="row">
                        <div class="col-md-6 col-xs-12">
                          <input type="hidden" name="mem_id" value="<?=@$_SESSION['member']['mem_id']?>">
                          <?php if($_SESSION['member']['mem_type_register'] == 'G'){ ?>
                          <div class="form-group">
                            <input type="text" value="<?=@$_SESSION['member']['mem_username']?>" class="form-control input-lg" placeholder="รหัสผู้ใช้งาน" required readonly>
                          </div>
                          <?php } ?>
                          <div class="form-group">
                            <input type="text" name="mem_nickname" value="<?=@$_SESSION['member']['mem_nickname']?>" class="form-control input-lg" placeholder="นามแฝง" required>
                          </div>
                          <div class="form-group">
                            <input type="text" name="mem_fname" value="<?=@$_SESSION['member']['mem_fname']?>" class="form-control input-lg" placeholder="ชื่อ" required>
                          </div>
                          <div class="form-group">
                            <input type="text" name="mem_lname" value="<?=@$_SESSION['member']['mem_lname']?>" class="form-control input-lg" placeholder="นามสกุล" required>
                          </div>
                          <div class="form-group">
                            <input type="email" name="mem_email" value="<?=@$_SESSION['member']['mem_email']?>" class="form-control input-lg" placeholder="อีเมล์" required readonly>
                          </div>
                          <div class="form-group">
                            <input type="tel" name="mem_mobile" value="<?=@$_SESSION['member']['mem_mobile']?>" class="form-control input-lg" placeholder="โทรศัพท์มือถือ" required>
                          </div>
                          <?php if(empty($_SESSION['member']['line_user_id'])){ ?>
                          <div class="form-group">
                            <a target="_blank" href="https://line.me/R/ti/p/%40234yyevs" class="btn btn-success btn-block"><span class="glyphicon glyphicon-refresh"></span> เขื่อมต่อ Line notify</a>
                          </div>
                        <?php }else{ ?>
                          <div class="form-group">
                            <button type="button" href="https://line.me/R/ti/p/%40234yyevs" class="btn btn-success btn-block" disabled><span class="glyphicon glyphicon-refresh"></span> เขื่อมต่อ Line notify สำเร็จ</button>
                          </div>
                        <?php } ?>
                          <!-- <div class="form-group"> -->
                            <input type="file" name="mem_image" id="mem_image" onchange="readURL2(this,'image-pro')" accept="image/png,image/jpeg" style="display:none;">
                          <!-- </div> -->
                          <!-- <div class="form-group"> -->
                            <!-- <label>พื้นหลังโปรไฟล์</label> -->
                            <input type="file" name="mem_image_bg" id="mem_image_bg" onchange="readURLBG(this,'banner_section','imgBg')" accept="image/png,image/jpeg" style="display:none;">
                          <!-- </div> -->
                          <div id="imgBg"></div>
                          <div class="btn-bottom text-right">
                            <button type="submit" class="btn btn-default">บันทึกแก้ไขโปรไฟล์</button>
                          </div>
                        </div>


                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php if($json['dataCount'] > 0){ ?>
            <div role="tabpanel" class="tab-pane" id="profileproject">
              <div class="profile-input">
                <div class="row">
                  <div class="col-md-12">
                    <form class="formLogin editProfileproject" novalidate>
                      <div class="row" id="form-aboutMe"></div>
                    </form>
                  </div>
                </div>
              </div>

            </div>
            <div role="tabpanel" class="tab-pane <?=$action=='project'?"active":""?>" id="profileprojectowner">
              <div class="profile-input">
                <div class="row">
                  <div class="col-md-12" id="data-project">

                  </div>
                </div>
              </div>

            </div>
            <?php } ?>
            <div role="tabpanel" class="tab-pane <?=$action=='hiring'?"active":""?>" id="project">
              <div class="row">

                <div class="col-xs-12">
                  <div class="tab-info create-section">
                    <p>Create New Project
                      <a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Click here</a>
                    </p>
                  </div>

                  <div class="collapse <?=@$_GET['act']=='open'?"in":""?>" id="collapseExample">
                    <div class="well">
                      <div class="row">
                        <form class="formLogin createJob" id="create-Job" novalidate></form>
                      </div>
                    </div>
                  </div>

                </div>
                <div id="project-list-one"></div>

              </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="blog">
              <div class="row" id="data-blog"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>


<!-- Modal -->
<div class="modal fade" id="myModalBlog" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Management Blog</h4>
      </div>
      <form id="formAddModuleBlog" novalidate enctype="multipart/form-data">
        <div id="show-formBlog"></div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalViewJob" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">View Project</h4>
      </div>
      <form id="formAddModuleViewJob" novalidate enctype="multipart/form-data">
        <div id="show-viewJob"></div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalView" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Project Events</h4>
      </div>
      <form id="formAddModuleView" novalidate enctype="multipart/form-data">
        <div id="show-view"></div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalUpgrade" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Upgrade user</h4>
      </div>
      <form id="formAddModuleUpgrade" novalidate enctype="multipart/form-data">
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="myModalInstallment" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Management Installment</h4>
      </div>
      <form id="formInstallment" novalidate enctype="multipart/form-data">
      <!-- <form enctype="multipart/form-data" action="service/approve.php" method="post"> -->
        <div id="show-Installment"></div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="myModalApprovePerson" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">รายละเอียดการเสนอราคา</h4>
      </div>
      <form id="formApprovePerson" novalidate enctype="multipart/form-data">
      <!-- <form enctype="multipart/form-data" action="service/approve.php" method="post"> -->
        <div id="show-ApprovePerson"></div>
      </form>
    </div>
  </div>
</div>
