<section class="about">

  <div class="container rep">
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <img class="img-full" src="../../images/article/<?=$row['image']?>" alt="service">
        <div class="about-content">
          <h3 class="title"><?=$row['title']?></h3>
          <p class="calenda"><?=date('F j, Y',strtotime($row['date_create']));?></p>
          <?=$row['content']?>
        </div>

        <?php
          $sql = "SELECT * FROM t_article_gallery WHERE article_id = '{$_GET['article_id']}'";
          $query = DbQuery($sql,null);
          $json = json_decode($query,true);
          if($json['dataCount'] > 0){
        ?>
        <div class="gallery">
          <div class="main_heading">
            <h2>GALLERY</h2>
          </div>
          <div class="owl-carousel owl-theme image-gallery">
              <?php
                foreach ($json['data'] as $value) {
              ?>
              <div class="item">
                <a href="../../images/article/<?=$value['image']?>" data-lightbox="gallery">
                  <img src="../../images/article/<?=$value['image']?>">
                </a>
              </div>
              <?php } ?>
          </div>
        </div>
        <?php } ?>

      </div>

    </div>
  </div>


  <div class="container rep">
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <div class="about-content">
          <div class="tab_bar_section mar-20">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">แสดงความคิดเห็น (<span id="Comment"></span>)</a>
              </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="profile">

                <div class="product_review">
                  <div id="show-commant"></div>

                  <?php if(isset($_SESSION['member']['mem_fname'])){ ?>
                  <div class="form-contact">
                    <h4 class="text-center text-title-comment">แสดงความคิดเห็น</h4>
                    <div class="row">
                      <form id="formComment" class="contact-form" novalidate>
                        <input type="hidden" name="tc_fname" value="<?=@$_SESSION['member']['mem_fname']?>">
                        <input type="hidden" name="tc_lname" value="<?=@$_SESSION['member']['mem_lname']?>">
                        <input type="hidden" name="tc_email" value="<?=@$_SESSION['member']['mem_email']?>">
                        <input type="hidden" name="tc_image" value="<?=@$_SESSION['member']['mem_image']?>">
                        <input type="hidden" id="article_id" name="article_id" value="<?=@$_GET['article_id']?>">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <textarea name="tc_comment" class="form-control input-lg" rows="6" placeholder="Message" required></textarea>
                          </div>
                        </div>

                        <div class="col-xs-12">
                          <div class="btn-bottom text-center">
                            <button type="submit" class="btn btn-default">แสดงความคิดเห็น</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <?php }else{ ?>
                    <div class="form-contact">
                      <div class="row">
                        <form class="contact-form">
                          <div class="col-xs-12">
                            <div class="btn-bottom text-center">
                              <button type="button" onclick="loginModel('blog_detail/?article_id=<?=$_GET['article_id']?>')" class="btn btn-default">สมัครสมาชิก</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
