
$("#formComment").on('submit', function(event) {
  event.preventDefault();
  if ($('#formComment').smkValidate()) {
    $.post( "service/formComment.php", $( "#formComment" ).serialize())
    .done(function( data ) {
      console.log(data);
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        $('#formComment').smkClear();
        showCommant(1);
      }
    });
  }
});

showCommant(1);
function showCommant(page){
  var article_id = $('#article_id').val();
  $.post( "service/showCommant.php", { page : page , article_id: article_id } )
  .done(function( data ) {
    $('#show-commant').html(data);
    var countComment = $("#Comment").text($('#countComment').val());
  });
}


$('.image-gallery').owlCarousel({
    loop:false,
    margin:5,
    responsiveClass:true,
    merge:true,
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:4,
            nav:true
        }
    }
})
