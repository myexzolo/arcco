<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BLOG DETAIL | ARCCO</title>

    <?php include "../../inc/css.php" ?>
    <link rel="stylesheet" href="css/main.css">

  </head>
  <body>
    <?php include "../../inc/nav.php" ?>

    <?php
      // $sql   = "SELECT * FROM t_article a, t_article_gallery ag
      //           WHERE a.article_id = ag.article_id
      //           AND a.article_id = '{$_GET['article_id']}'
      //           ORDER BY a.date_create DESC
      //           LIMIT 0,1";
      $sql   = "SELECT * FROM t_article a
                WHERE article_id = '{$_GET['article_id']}'";
      $query = DbQuery($sql,null);
      $row   = json_decode($query,true)['data'][0];
      $article_id = $row['article_id'];
    ?>
    <!-- body function -->
    <?php include "ajax/header.php" ?>
    <?php include "ajax/detail.php" ?>
    <!-- End body function -->

    <?php include "../../inc/footer.php" ?>
    <?php include "../../inc/js.php" ?>
    <script type="text/javascript" src="js/main.js"></script>
  </body>
</html>
