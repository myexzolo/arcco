<?php

$sql = "SELECT * FROM t_job j , data_mas_add_province p, t_category c , t_type_build tb , t_type_project tp
        WHERE j.PROVINCE_CODE = p.PROVINCE_CODE
        AND j.category_id = c.category_id
        AND j.ttb_id = tb.ttb_id
        AND j.ttp_id = tp.ttp_id
        AND j.cus_id = '{$_SESSION['member']['mem_id']}'";

$query = DbQuery($sql,null);
$row = json_decode($query,true);

?>
<div class="banner_section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="title-holder">
            <div class="title-holder-cell text-left">
              <h1 class="page-title">ข้อมูลโครงการของคุณ</h1>
              <!-- <p class="message-title">ข้อมูลโครงการของคุณ</p> -->
              <ol class="breadcrumb">
                <li>Project</li>
                <li>Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
