<section class="blog pdd-t-0">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <!-- <div>
          <ul class="nav nav-tabs" role="tablist">
            <li class="active" role="presentation" onclick="goCreateJob();">
              <a href="#project" aria-controls="project" role="tab" data-toggle="tab">
                Hiring project
              </a>
            </li>
          </ul> -->
          <input type="hidden" id="tj_id" name="tj_id" value="<?=$_GET['tj_id']?>">
          <div class="tab-content" style="min-height: 28vh;">
            <div role="tabpanel" class="tab-pane active" id="project">
              <div class="row">
                <div id="project-list-one"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

<!-- Modal -->
<div class="modal fade" id="myModalView" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Project Events</h4>
      </div>
      <form id="formAddModuleView" novalidate enctype="multipart/form-data">
        <div id="show-view"></div>
      </form>
    </div>
  </div>
</div>
