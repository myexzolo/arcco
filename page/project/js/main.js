function openModelDetail(id,type){
  $.post("service/formView.php",{id:id,te_user_type:type})
    .done(function( data ) {
      $('#myModalView').modal({backdrop:'static'});
      $('#show-view').html(data);
  });
}


function postURL(url, multipart) {
  //alert("url:" + url);
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function postURL_blank(url) {
 var form = document.createElement("FORM");
 form.method = "POST";
 form.target = "_blank";
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function createInvoice(installment_id){
  $.post("../../pdf/invoice/index.php",{installment_id:installment_id})
    .done(function( data ) {
      console.log(data);
  });
}

function createQuotation(tj_id){
  $.post("../../pdf/quotation/index.php",{tj_id:tj_id})
    .done(function( data ) {
      console.log(data);
  });
}
projectListOne();
function projectListOne(){
  var tj_id = $('#tj_id').val();
  $.post("service/projectListOne.php",{tj_id:tj_id})
    .done(function( data ) {
      $('#project-list-one').html(data);
  });
}
// $('#formAddModuleView').on('submit', function(event) {
//   // $('#te_text').val(dataEditorView.getData());
//   event.preventDefault();
//   if ($('#formAddModuleView').smkValidate()) {
//     $.ajax({
//         url: 'service/saveEvent.php',
//         type: 'POST',
//         data: new FormData( this ),
//         processData: false,
//         contentType: false,
//         dataType: 'json'
//     }).done(function( data ) {
//       console.log(data);
//       $.smkAlert({text: data.message,type: data.status});
//       if(data.status == 'success'){
//         $('#myModalView').modal('toggle');
//         projectListOne();
//       }
//     });
//   }
// });
