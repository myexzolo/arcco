<?php
  include('../../../admin/inc/function/connect.php');
?>
<div class="modal-body" style="min-height:60vh;">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>สนทนา</label>
        <div class="form-group">
          <textarea name="te_text" class="form-control" rows="6" required></textarea>
        </div>
        <!-- <div id="editorView"></div>
        <input type="hidden" id="te_text" name="te_text"> -->
        <input type="hidden" name="te_user_type" value="<?=$_POST['te_user_type']?>">
        <input type="hidden" name="tj_id" value="<?=$_POST['id']?>">
      </div>
    </div>
  </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;"  data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat">save changes</button>
</div>

<script type="text/javascript">
  // var dataEditorView;
  // $(function () {
  //     init();
  // })
  // var init = ( function() {
  //   var wysiwygareaAvailable = isWysiwygareaAvailable(),
  //     isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );
  //
  //   return function() {
  //     var editorElement = CKEDITOR.document.getById( 'editorView' );
  //     if ( wysiwygareaAvailable ) {
  //       dataEditorView = CKEDITOR.replace('editorView', {
  //         toolbar: [{
  //             name: 'clipboard',
  //             items: ['Undo', 'Redo']
  //           },
  //           {
  //             name: 'basicstyles',
  //             items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
  //           },
  //           {
  //             name: 'links',
  //             items: ['Link', 'Unlink']
  //           },
  //           '/'
  //         ],
  //       });
  //     } else {
  //       editorElement.setAttribute( 'contenteditable', 'true' );
  //       CKEDITOR.inline( 'editorView' );
  //     }
  //   };
  //
  //   function isWysiwygareaAvailable() {
  //     if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
  //       return true;
  //     }
  //     return !!CKEDITOR.plugins.get( 'wysiwygarea' );
  //   }
  // } )();

</script>
