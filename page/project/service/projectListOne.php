<?php

  include('../../../admin/inc/function/connect.php');

  $sql = "SELECT * FROM t_job j , data_mas_add_province p, t_category c , t_type_build tb , t_type_project tp
          WHERE j.PROVINCE_CODE = p.PROVINCE_CODE
          AND j.category_id = c.category_id
          AND j.ttb_id = tb.ttb_id
          AND j.ttp_id = tp.ttp_id
          AND j.cus_id = '{$_SESSION['member']['mem_id']}' AND j.tj_id = '{$_POST['tj_id']}'";

  $query = DbQuery($sql,null);
  $row = json_decode($query,true);


  if($row['dataCount'] > 0){

    foreach ($row['data'] as $value) {

      if(!empty($value['img_ref'])){
          $img_ref = explode('|',$row['data'][0]['img_ref']);
      }

      $Quotation = "";
      $installmentInvoice = "";

      $tj_id =  $value['tj_id'];
      $tj_quotation =  $value['tj_quotation'];
      if($tj_quotation != ""){
        $Quotation = "<a href=\"../../pdf/output/$tj_quotation\" target=\"_blank\">$tj_quotation</a> (รหัสเปิดไฟล์ {$value['password_admin']})";
      }
?>
<div class="col-md-5 col-xs-12">
  <a href="../../images/imgJob/<?=$value['tj_img']?>" data-lightbox="ref">
    <img class="img-full" src="../../images/imgJob/<?=$value['tj_img']?>"/>
  </a>
  <div class="sub-img">
    <?php if(!empty($img_ref)){ ?>
    <div class="owl-carousel owl-theme">
      <?php foreach ($img_ref as $keyref => $valueref) {?>
      <div class="item">
        <a href="../../images/imgJob/<?=$valueref?>" data-lightbox="ref">
          <img class="img-full" height="100" src="../../images/imgJob/<?=$valueref?>"/>
        </a>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
  </div>

  <?php
    $str = $value['tj_status']!='A'?"AND status in ('Y','A')":"";
    $sqlo = "SELECT * FROM orders od , order_detail odd , t_member tm
            WHERE od.o_id = odd.o_id AND odd.mem_id = tm.mem_id
            AND od.tj_id = '{$value['tj_id']}' $str";
    $queryo = DbQuery($sqlo,null);
    $rowo = json_decode($queryo,true);
    if($rowo['dataCount'] > 0){
      foreach ($rowo['data'] as $valueo) {
        $status = $valueo['status'];
        $od_id  = $valueo['od_id'];
        $fullname = $valueo['mem_fname'].' '.$valueo['mem_lname'];

        if($status == "A")
        {
          $sqli = "SELECT * FROM order_installment
                   WHERE od_id = $od_id and (status_pay is null or status_pay in ('P','Y'))
                   order by installment_no ASC";

         $queryi = DbQuery($sqli,null);
         $rowoi = json_decode($queryi,true);

         if($rowoi['dataCount'] > 0)
         {
           $installmentInvoice = '';
           foreach ($rowoi['data'] as $keyoi => $valueoi) {
             $installment_no = $valueoi['installment_no'];
             $installment_id = $valueoi['installment_id'];
             $installment_invoice = $valueoi['installment_invoice'];

             if($valueoi['installment_invoice'] != ''){
               $pathPay = "../../page/pay/index.php?installment_id=$installment_id";
               $status_pay = $valueoi['status_pay']=='Y'?"ชำระเงินเรียบร้อย":"รอการชำระเงิน";
               $installmentInvoice .= "<b>งวดที่  $installment_no ($status_pay)</b>"."<br /><a href=\"../../pdf/output/$installment_invoice\" target=\"_blank\">$installment_invoice</a> (รหัสเปิดไฟล์ {$valueoi['password_admin']})<br />";
               $installmentInvoice .= "<a onclick=\"postURL('$pathPay')\" style=\"cursor:pointer\"><b>ชำระเงิน</b></a><br />";
             }
           }
           // if($rowoi['data'][0]['installment_invoice'] != "")
           // {
           //   $installment_no = $rowoi['data'][0]['installment_no'];
           //   $installment_id = $rowoi['data'][0]['installment_id'];
           //   $installment_invoice = $rowoi['data'][0]['installment_invoice'];
           //
           //   $pathPay = "../../page/pay/index.php?installment_id=$installment_id";
           //   $status_pay = $rowoi['data'][0]['status_pay']=='Y'?"ชำระเงินเรียบร้อย":"รอการชำระเงิน";
           //   $installmentInvoice = "<b>งวดที่  $installment_no ($status_pay)</b>"."<br /><a href=\"../../pdf/output/$installment_invoice\" target=\"_blank\">$installment_invoice</a>";
           //   $installmentInvoice .= "<br /><a onclick=\"postURL('$pathPay')\" style=\"cursor:pointer\"><b>ชำระเงิน</b></a>";
           // }
         }
        }
      }
    }
  ?>

  <div class="chat" id="chat">
    <div class="header-chat">
      <p>chat</p>
    </div>
    <div class="body-chat">
      <?php
        if(isset($valueo)){
          $sqll = "SELECT * FROM t_event_log WHERE tj_id = '{$valueo['tj_id']}' ORDER BY date_create ASC";
          $queryl = DbQuery($sqll,null);
          $rowl = json_decode($queryl,true);
          if($rowl['dataCount'] > 0){
      ?>
      <?php foreach ($rowl['data'] as $valuel) { ?>

      <div class="body-chats <?=$valuel['te_user_type']?>">
        <div class="<?=$valuel['te_user_type']?>-chat">
          <?=ltrim($valuel['te_text'])?>
          <?php if(!empty($valuel['te_img'])){?>
            <h6 class="file-text">
              <?php if(count(strpos($valuel['te_img'],".pdf")) > 0){ ?>
                <a target="_blank" href="../../images/slipt/<?=$valuel['te_img']?>">
                  <?=$valuel['te_img']?>
                </a>
              <?php }else{ ?>
                <a href="../../images/slipt/<?=$valuel['te_img']?>" data-lightbox="file">
                  <?=$valuel['te_img']?>
                </a>
              <?php }?>
            </h6>
          <?php } ?>
          <span><i><?=$valuel['date_create']?></i></span>
        </div>
      </div>
      <?php } ?>

      <?php } ?>
      <?php } ?>
    </div>
  </div>

  <div class="chat-form">
    <form id="formAddModuleView" method="post">
    <div class="form-group">
      <textarea name="te_text" id="te_text" class="form-control" rows="3" required <?=$value['tj_status']=='E'?"disabled":""?> <?=empty($Quotation)?"disabled":""?>></textarea>
      <input type="hidden" name="te_user_type" value="cus">
      <input type="hidden" name="tj_id" value="<?=$valueo['tj_id']?>">
    </div>
    <!-- <div class="form-group">
      <label>อัพโหลดไฟล์</label>
      <input type="file" accept="image/png,image/jpeg" class="form-control" name="te_img" onchange="readURL(this,'showImage')">
    </div>
    <div id="showImage"></div> -->

      <div class="text-right">
        <button type="submit" class="btn btn-primary btn-flat" <?=$value['tj_status']=='E'?"disabled":""?> <?=empty($Quotation)?"disabled":""?>>send message</button>
      </div>
    </form>
  </div>

</div>
<div class="col-md-7 col-xs-12">
  <div class="about-content">
    <table class="table mar-t-30">
      <tbody>
        <tr>
          <td width="20%">Project :</td>
          <td width="80%"><?=$value['tj_name']?></td>
        </tr>
        <tr>
          <td>Detail :</td>
          <td style="white-space: pre-line;"><?=$value['tj_detail']?></td>
        </tr>
        <tr>
          <td>Location :</td>
          <td><?=$value['PROVINCE_NAME']?></td>
        </tr>
        <tr>
          <td>Owner :</td>
          <td><?=@$fullname?></td>
        </tr>
        <tr>
          <td>Service :</td>
          <td><?=$value['category_name_th']?></td>
        </tr>
        <tr>
          <td>ProjectType :</td>
          <td><?=$value['ttp_id']=='6'?$value['tj_type_name']:$value['ttp_name']?></td>
        </tr>
        <tr>
          <td>Type :</td>
          <td><?=$value['ttb_name']?></td>
        </tr>
        <tr>
          <td>Quotation :</td>
          <td><?=$Quotation?></td>
        </tr>
        <tr>
          <td>Installment :</td>
          <td>
            <?=$installmentInvoice?>
          </td>
        </tr>
      </tbody>
    </table>
    <?php
      $text = '';
      $date = '';
      switch ($value['tj_status']) {
        case 'A':
          $text = 'Request Person';
          break;
        case 'S':
          $text = 'Start Project';
          break;
        case 'E':
          $text = 'Close Project';
          break;
        default:
          $text = 'No response';
          break;
      }
    ?>
    <span class="badge badge-dark">Status</span>
    <span class="badge badge-secondary"><?=$text.' '.$date?></span>

    <div class="mar-t-30 text-right">
      <?php
        $te_text = 'ปิดโครงการ';
        $te_user_type = 'sys-cus';
        $tj_id = $tj_id;
      ?>
      <button type="button" <?=$value['tj_status']=='E'?"disabled":""?> onclick="closeProject('<?=$te_text?>','<?=$te_user_type?>','<?=$tj_id?>')" class="btn btn-danger btn-sm">ปิดโปรเจค</button>
    </div>


  </div>
</div>
<?php }} ?>

<script type="text/javascript">

$('.owl-carousel').owlCarousel({
    margin:10,
    loop:false,
    autoHeight:true,
    items:4
})


$('#te_text').keypress(function(event) {
  if (event.which == 13) {
    event.preventDefault();
    this.value = this.value + "\n";
  }
});

$('#formAddModuleView').on('submit', function(event) {
  // $('#te_text').val(dataEditorView.getData());
  event.preventDefault();
  if ($('#formAddModuleView').smkValidate()) {
    $.ajax({
        url: 'service/saveEvent.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      // console.log(data);
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        // $('#myModalView').modal('toggle');
        projectListOne();
      }
    });
  }
});

function closeProject(te_text,te_user_type,tj_id){
  $.smkConfirm({
    text:'คุณต้องการปิดโปรเจคใช่หรือไม่?',
    accept:'ยืนยัน',
    cancel:'ยกเลิก'
  },function(res){
    if (res) {
      $.post("service/saveEvent.php",{ te_text:te_text, te_user_type:te_user_type, tj_id:tj_id })
        .done(function( data ) {
          $.post("service/closeProject.php",{ tj_id:tj_id })
            .done(function( data ) {
              $.smkAlert({text: data.message,type: data.status});
              projectListOne();
          });
      });
    }
  });
}

</script>
