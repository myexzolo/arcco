<?php
  include '../../../admin/inc/function/connect.php';
?>

<div class="col-md-9 col-xs-12">
  <?php
    $category_id = $_POST['category_id']!=''?" AND category_id = '{$_POST['category_id']}'":"";
    $active = $_POST['category_id']!=''?$_POST['category_id']:"";
    $sql = "SELECT * FROM t_category WHERE 1=1 $category_id";
    $query = DbQuery($sql,null);
    $json = json_decode($query,true);
      if($json['dataCount'] > 0){
        $row = $json['data'][0];
  ?>
  <img class="img-full" src="../../images/service/<?=$row['category_path']?>" alt="service">
  <div class="about-content">
    <h3 class="title"><?=$row['category_name_th']?></h3>
    <?=$row['category_detail']?>
  </div>
<?php }else{ echo '<h1 class="text-center">ไม่พบข้อมูล</h1>'; } ?>
</div>
<div class="col-md-3 col-xs-12">
  <div class="about-content">
    <h3>TAG</h3>
    <div class="side_bar_blog">
      <div class="tags">
        <ul>
          <?php
            $sql = "SELECT * FROM t_category ORDER BY category_seq ASC";
            $query = DbQuery($sql,null);
            $json = json_decode($query,true);
              if($json['dataCount'] > 0){
                foreach ($json['data'] as $value) {
          ?>
          <li><a onclick="showData('<?=$value['category_id']?>')" href="javascript:void(0)" class="<?=$active==$value['category_id']?"active":""?>"><?=$value['category_name_th']?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</div>
