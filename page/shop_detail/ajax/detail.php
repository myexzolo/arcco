<section class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-xs-12">
        <img class="img-full" src="../../images/memberJob/imgThum/<?=empty($row['memberJob_banner'])?"post-01.jpg":$row['memberJob_banner']?>">
        <div class="tab_bar_section mar-20" style="margin-bottom: 20px;">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">ผลงานของเรา</a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="gallery">
              <div class="row">
                  <div class="col-xs-12">
                      <div class="owl-carousel owl-theme gallery">
                      <?php
                        $memberJob_portfolio = array_diff(explode("|", $row['memberJob_portfolio']),array(""));
                          foreach ($memberJob_portfolio as $value_slide) {
                      ?>
                      <div class="item" data-merge="1">
                        <a href="../../images/memberJob/portfolio/<?=$value_slide?>" data-lightbox="gallery">
                          <img src="../../images/memberJob/portfolio/<?=$value_slide?>">
                        </a>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
              </div>
            </div>
          </div>

        </div>

      </div>
      <div class="col-md-7 col-xs-12">
        <div class="about-content">
          <div class="btn-bottom text-right" style="margin: 0;">
            <button type="button" onclick="contactModal('<?=$mem_id?>')" class="btn btn-default pull-left">ติดต่อสอบถาม</button>
            <button type="button" onclick="cart('<?=$mem_id?>')" class="btn btn-default">ADD TO LIST</button>
          </div>
          <div class="tab_bar_section mar-20">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">เกี่ยวกับเรา</a>
              </li>
              <li role="presentation">
                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">รีวิว (<span id="Comment"></span>)</a>
              </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="home">
                <div class="about-content mar-t">
                  <?=$row['memberJob_detail']?>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="profile">
                <div class="product_review">
                  <div id="show-commant"></div>

                  <?php if(isset($_SESSION['member']['mem_fname'])){ ?>
                  <div class="form-contact">
                    <h4 class="text-center text-title-comment">แสดงความคิดเห็น</h4>
                    <div class="row">
                      <form id="formComment" class="contact-form" novalidate>
                        <input type="hidden" name="tr_fname" value="<?=@$_SESSION['member']['mem_fname']?>">
                        <input type="hidden" name="tr_lname" value="<?=@$_SESSION['member']['mem_lname']?>">
                        <input type="hidden" name="tr_email" value="<?=@$_SESSION['member']['mem_email']?>">
                        <input type="hidden" name="tr_image" value="<?=@$_SESSION['member']['mem_image']?>">
                        <input type="hidden" id="mem_id" name="mem_id" value="<?=@$_GET['mem_id']?>">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <textarea name="tr_comment" class="form-control input-lg" rows="6" placeholder="Message" required></textarea>
                          </div>
                        </div>

                        <div class="col-xs-12">
                          <div class="btn-bottom text-center">
                            <button type="submit" class="btn btn-default">แสดงความคิดเห็น</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <?php }else{ ?>
                    <div class="form-contact">
                      <div class="row">
                        <form class="contact-form">
                          <div class="col-xs-12">
                            <div class="btn-bottom text-center">
                              <button type="button" onclick="loginModel('shop_detail/?mem_id=<?=$_GET['mem_id']?>')" class="btn btn-default">สมัครสมาชิก</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  <?php } ?>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

</section>

<div class="modal fade" id="myModalCheck" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">My Project</h4>
      </div>
      <form id="formAddModuleCheck" novalidate enctype="multipart/form-data">
        <div id="show-formCheck"></div>
      </form>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ติดต่อสอบถาม</h4>
      </div>
      <form id="formAddModuleContact" novalidate>
        <div id="show-formContact"></div>
      </form>
    </div>
  </div>
</div>
