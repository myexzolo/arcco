<?php
    $mem_id = @$_GET['mem_id'];
    $sql = "SELECT * FROM t_member tm , t_memberjob tj
            WHERE tm.mem_id = tj.mem_id AND tm.mem_id = '$mem_id'";
    $query = DbQuery($sql,null);
    $row =  json_decode($query,true)['data'][0];

    $bg_image = isset($row['mem_image_bg'])?"../../images/profile/{$row['mem_image_bg']}":"../../images/bg/banner.jpg";

?>

<div class="banner_section"
  style="background:url(<?=$bg_image?>);
    background-size: cover;
    background-position: center center;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="title-holder">
            <div class="title-holder-cell text-left">
              <h1 class="page-title"><?=!empty($row['mem_nickname'])?$row['mem_nickname']:$row['mem_fname'].' '.$row['mem_lname']?></h1>
              <ol class="breadcrumb">
                <ol class="breadcrumb">
                  <li><a class="link-write" href="../home/">หน้าแรก</a> </li>
                  <li class="link-disable">สินค้า/บริการ</li>
                </ol>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
