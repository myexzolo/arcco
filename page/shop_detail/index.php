<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SHOP DETAIL | ARCCO</title>

    <?php include "../../inc/css.php" ?>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/gallery.css">

  </head>
  <body>
    <?php include "../../inc/nav.php" ?>

    <!-- body function -->
    <?php include "ajax/header.php" ?>
    <?php include "ajax/detail.php" ?>
    <!-- End body function -->

    <?php include "../../inc/footer.php" ?>
    <?php include "../../inc/js.php" ?>
    <script src="js/main.js"></script>
  </body>
</html>
