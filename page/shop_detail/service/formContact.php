<?php

include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');
?>
<input type="hidden" name="mem_id" value="<?=$_POST['mem_id']?>">

<div class="modal-body">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>ชื่อผู้ติดต่อ</label>
        <input type="text" name="contF_fullname" class="form-control" placeholder="ชื่อผู้ติดต่อ" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>หัวข้อติดต่อ</label>
        <input type="text" name="contF_topic" class="form-control" placeholder="หัวข้อติดต่อ" required>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>ข้อความ</label>
        <textarea name="contF_message" class="form-control" rows="6" placeholder="ข้อความ" required></textarea>
      </div>
    </div>

  </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;"  data-dismiss="modal">ปิด</button>
  <button type="submit" class="btn btn-primary btn-flat">ดำเนินการต่อ</button>
</div>
