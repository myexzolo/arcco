<?php

include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');
?>
<input type="hidden" name="id" value="<?=$_POST['id']?>">
<div class="modal-body">

  <?php
    if(isset($_SESSION['member']['mem_id'])){
    $sql = "SELECT * FROM t_job WHERE cus_id = '{$_SESSION['member']['mem_id']}' AND tj_status = 'A'";
    $query = DbQuery($sql,null);
    $row = json_decode($query,true);
    if($row['dataCount'] > 0){
  ?>
  <table class="table table-striped">
  <thead>
    <tr>
      <td width="10%"></td>
      <td width="30%">Project Img</td>
      <td width="60%">Project Name</td>
    </tr>
  </thead>
  <tbody>
    <?php
        foreach ($row['data'] as $value) {
    ?>
    <tr>
      <td class="text-center">
        <div class="form-group">
          <div class="radio">
            <label>
              <input type="radio" name="tj_id" value="<?=$value['tj_id']?>" required>
            </label>
          </div>
        </div>
      </td>
      <td>
        <img width="125" src="../../images/imgJob/<?=$value['tj_img']?>">
      </td>
      <td><?=$value['tj_name']?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<?php }else{ ?>
  <div class="col-xs-12">
    <div class="alert alert-danger" role="alert">ไม่พบข้อมูล</div>
  </div>
<?php } ?>
<?php } ?>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;"  data-dismiss="modal">ปิด</button>
  <?php if(isset($_SESSION['member']['mem_id'])){ ?>
  <button type="submit" class="btn btn-primary btn-flat">ดำเนินการต่อ</button>
  <button type="button" onclick="checkForm()" class="btn btn-success btn-flat">ดำเนินการต่อและไปหน้าชำระเงิน</button>
  <?php } ?>
</div>
