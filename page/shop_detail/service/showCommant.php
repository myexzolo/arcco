<?php
  include('../../../admin/inc/function/mainFunc.php');
  include('../../../admin/inc/function/connect.php');

  $page = $_POST['page'];
  $listPerPage = 4;

  $start = ($listPerPage*$page)-$listPerPage;


  $sql = "SELECT * FROM t_review
          WHERE is_active = 'Y'
          ORDER BY tr_id DESC
          LIMIT $start,$listPerPage";
  $query = DbQuery($sql,null);
  $json = json_decode($query,true);
  if($json['dataCount'] > 0){
    foreach ($json['data'] as $value) {
?>

<div class="commant-text row">
  <div class="col-lg-2 col-md-2 col-sm-4">
    <div class="profile"> <img class="img-responsive img-center img-c" src="../../images/profile/<?=$value['tr_image']?>" alt="#"> </div>
  </div>
  <div class="col-lg-10 col-md-10 col-sm-8">
    <h5><?=$value['tr_fname']?> <?=$value['tr_lname']?></h5>
    <p><span class="c_date"><?=date('M j, Y',strtotime($value['date_create']));?></span></p>
    <p class="msg"><?=$value['tr_comment']?></p>
  </div>
</div>

<?php } ?>
<div class="text-center">
  <nav aria-label="Page navigation">
    <ul class="pagination">
      <?php
        $sqlc = "SELECT * FROM t_review
                WHERE is_active = 'Y'
                ORDER BY date_create DESC";
        $queryc = DbQuery($sqlc,null);
        $jsonc  = json_decode($queryc,true);
        $countpage = ceil($jsonc['dataCount']/$listPerPage);
        for ($i=1; $i <= $countpage ; $i++) {
      ?>
        <li class="<?=$i==$page?"active":""?>"><a href="javascript:void(0)" onclick="showCommant(<?=$i?>)"><?=$i?></a></li>
      <?php } ?>
    </ul>
  </nav>
</div>
<?php } ?>

<input type="hidden" id="countComment" value="<?=$json['dataCount']?>">
