function cart(id){
  $.post("service/checkproject.php",{id:id})
    .done(function( data ) {
      if(data.status == 'success'){
        openModelCheck(id);
      }else{
        $.smkAlert({text: data.message,type: data.status});
      }
  });
}

function openModelCheck(id){
  $.post("service/formProject.php",{id:id})
    .done(function( data ) {
      $('#myModalCheck').modal({backdrop:'static'});
      $('#show-formCheck').html(data);
  });
}

var checkout = 0;
function checkForm(){
  checkout = 1;
  $('#formAddModuleCheck').submit();
}

$('#formAddModuleCheck').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddModuleCheck').smkValidate()) {
    $.ajax({
        url: 'service/cart.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      $('#myModalCheck').modal('toggle');
      if(data.status == 'success' && checkout == 1){
        window.location = '../checkout/';
      }


    });
  }
});

$("#formComment").on('submit', function(event) {
  event.preventDefault();
  if ($('#formComment').smkValidate()) {
    $.post( "service/formComment.php", $( "#formComment" ).serialize())
    .done(function( data ) {
      console.log(data);
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        $('#formComment').smkClear();
        showCommant(1);
      }
    });
  }
});

showCommant(1);
function showCommant(page){
  var mem_id = $('#mem_id').val();
  $.post( "service/showCommant.php", { page : page , mem_id: mem_id } )
  .done(function( data ) {
    console.log(data);
    $('#show-commant').html(data);
    var countComment = $("#Comment").text($('#countComment').val());
  });
}

$('.gallery').owlCarousel({
    merge:true,
    stagePadding: 20,
    loop:true,
    margin: 5,
    responsiveClass:true,
    nav:false,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
})

function contactModal(mem_id){
  $.post("service/formContact.php",{mem_id:mem_id})
    .done(function( data ) {
      $('#contactModal').modal({backdrop:'static'});
      $('#show-formContact').html(data);
  });
}

$('#formAddModuleContact').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddModuleContact').smkValidate()) {
    $.ajax({
        url: 'service/saveContact.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      $('#contactModal').modal('toggle');
    });
  }
});
