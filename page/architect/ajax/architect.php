<section class="blog">

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="main_heading">
          <h2>ARCHITECT PERSON</h2>
        </div>
      </div>
    </div>

    <div class="row">

      <div class="col-sm-9 col-xs-12">
        <div class="row">
          <?php for ($i=1; $i <=3 ; $i++) { ?>
          <div class="col-sm-4 col-xs-12">
            <a class="content-link" href="../architect_detail/">
              <div class="blog-content">
                <img class="img-responsive" src="../../images/blog/post-0<?=$i?>.jpg">
                <h4>Norton Internet Security</h4>
                <p>$15.00 – $25.00</p>
              </div>
            </a>
          </div>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-3 col-xs-12">
        <div class="about-content">
          <h3>TAG</h3>
          <div class="side_bar_blog">
            <div class="tags">
              <ul>
                <?php
                  $sql   = "SELECT * FROM t_category ORDER BY category_seq ASC";
                  $query = DbQuery($sql,null);
                  $json   = json_decode($query, true);
                  if($json['dataCount'] > 0){
                    foreach ($json['data'] as $value) {
                ?>
                <li><a class="<?=$_GET['category_id']==$value['category_id']?"active":""?>" href="../architect/?category_id=<?=$value['category_id']?>"><?=$value['category_name_th']?></a></li>
                  <?php } ?>
                <?php } ?>
                </ul>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

</section>
