$(function(){
    $('.eps').dotdotdot({
        ellipsis: '...', /* The HTML to add as ellipsis. */
        wrap : 'word', /* How to cut off the text/html: 'word'/'letter'/'children' */
        watch : true /* Whether to update the ellipsis: true/'window' */
    });

    $('.content-macth').matchHeight();
});


$('.ads-t').owlCarousel({
    loop:true,
    margin:30,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
$('.ads-person').owlCarousel({
    loop:false,
    margin: -10,
    responsiveClass:true,
    nav:false,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
})

$('.article-gallery').owlCarousel({
    loop:false,
    margin: 10,
    responsiveClass:true,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})

$('.ads-full').owlCarousel({
    loop:true,
    margin:15,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        }
    }
})
