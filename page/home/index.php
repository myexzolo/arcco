<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HOME | ARCCO</title>

    <?php include "../../inc/css.php" ?>
    <link rel="stylesheet" href="css/main.css">

  </head>
  <body>
    <?php include "../../inc/nav.php" ?>

    <!-- body function -->
    <?php
      $array_page = array(
        'slide','requriment','choose','ads_full',
        'pageWork','ads_person','service','blog','ads_thumbnail',
      );
      foreach ($array_page as $value) {
        include "ajax/$value.php";
      }
    ?>
    <!-- End body function -->
    <script id="dbd-init" src="http://www.trustmarkthai.com/callbackData/initialize.js?t=52b-36-5-54aded7c641b0465d44d539d5ae81fcbd7246004e8"></script>
    <div id="Certificate-banners"></div>

    <?php include "../../inc/footer.php" ?>
    <?php include "../../inc/js.php" ?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.dotdotdot/4.1.0/dotdotdot.js"></script>
    <script type="text/javascript" src="../../js/matchHeight.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>
