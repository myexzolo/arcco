<section class="choose">

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="main_heading">
          <h2>OUR SERVICE</h2>
          <p>All of service for architectural and construction.</p>
        </div>
      </div>
    </div>

    <div class="row">

      <div class="service-content">

        <?php
          $sql   = "SELECT * FROM t_category ORDER BY category_seq ASC";
          $query = DbQuery($sql,null);
          $json   = json_decode($query, true);
          if($json['dataCount'] > 0){
            foreach ($json['data'] as $value) {
        ?>
        <div class="col-md-4 col-sm-6">
          <a href="../shop/?category_id=<?=$value['category_id']?>">
            <div class="service">
              <img class="img-responsive" src="../../images/service/<?=$value['category_path']?>"/>
              <div class="text-overflow">
                <p><?=$value['category_name_th']?></p>
                <p><?=$value['category_name_en']?></p>
              </div>
            </div>
          </a>
        </div>
          <?php } ?>
        <?php } ?>

      </div>

    </div>
  </div>

</section>
