<section class="blog pdd-t-0">

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="main_heading">
          <h2>Architectural and construction content</h2>
        </div>
      </div>
    </div>

    <div class="owl-carousel owl-theme article-gallery">
      <?php
        $sql   = "SELECT * FROM t_article WHERE is_active = 'Y'
                  ORDER BY date_create DESC";
        $query = DbQuery($sql,null);
        $row   = json_decode($query,true);
        if($row['dataCount'] > 0){
          foreach ($row['data'] as $value) {

            if($value['article_img'] == null){
              $sql   = "SELECT image FROM t_article_gallery WHERE article_id = '{$value['article_id']}'";
              $query = DbQuery($sql,null);
              $json  = json_decode($query,true)['data'][0];
              $value['article_img'] = $json['image'];
            }
      ?>
      <div class="item">
        <a class="a-content" href="../blog_detail/?article_id=<?=$value['article_id']?>">
          <div class="blog-content content-macth">
            <div class="box-img">
              <img class="img-responsive-t" src="../../images/article/<?=$value['article_img']?>">
            </div>

            <div class="content">
              <div class="post_time">
                <p><i class="fa fa-clock-o"></i> <?=date('F j, Y',strtotime($value['date_create']));?></p>
              </div>
              <h4><?=$value['title']?></h4>
              <p class="eps"><?=strip_tags($value['content'])?></p>
            </div>
          </div>
        </a>
      </div>
      <?php }} ?>

    </div>
  </div>

</section>
