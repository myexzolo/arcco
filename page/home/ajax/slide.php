<?php
  $sql   = "SELECT * FROM t_slide ORDER BY slide_seq ASC";
  $query = DbQuery($sql,null);
  $json   = json_decode($query, true);
  if($json['dataCount'] > 0){
?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <div class="overlay-slide"></div>
  <div class="text-title-slide">
    <h1>Welcome..to Arcco-th.com</h1>
    <p>Arcco แหล่งรวมfreelance ด้านสถาปัตยกรรม
      และงานก่อสร้างครบวงจร ด้วยระบบจัดจ้างสำหรับ freelance
      ด้านสถาปัตยกรรมและงานก่อสร้าง โดยเฉพาะ</p>
  </div>
  <img class="logo-slide" src="../../images/logo.png">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php for ($i=1; $i <= $json['dataCount'] ; $i++) { ?>
    <li data-target="#carousel-example-generic" data-slide-to="<?=$i-1?>" class="<?=$i==1?"active":""?>"></li>
    <?php } ?>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php foreach ($json['data'] as $key => $value): ?>
    <div class="item <?=$key==0?"active":""?> fix-max">
      <?php
        $jsFunc = '';
        $link = 'javascript:void(0)';
        if($value['slide_parm'] == 'login'){
          $jsFunc = 'onclick="loginModel()"';
        }

        if(empty($value['slide_parm']) && !empty($value['slide_link'])){
          $link = $value['slide_link'];
        }
      ?>
      <a href="<?=$link?>" <?=$jsFunc?>>
        <img class="img-full" src="../../images/slide/<?=$value['slide_path']?>" alt="<?=$value['slide_path']?>">
      </a>
      <?php if(!empty($value['slide_text']) && empty($value['slide_parm'])){ ?>
      <div class="carousel-caption">
        <?php
          $arr_str = explode(",",$value['slide_text']);
          foreach ($arr_str as $value_str) {
        ?>
        <h3><?=$value_str?></h3>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
    <?php endforeach; ?>
  </div>
</div>

<?php } ?>
