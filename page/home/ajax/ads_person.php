<div class="container blogs">
  <div class="owl-carousel owl-theme ads-person">
    <?php
      $sql = "SELECT * FROM t_member tm , t_memberjob tj
              WHERE tm.mem_id = tj.mem_id
              AND tm.mem_type_user != 'C'
              AND tj.type_ads = 'Y' ORDER BY RAND()";
      $query = DbQuery($sql,null);
      $row   = json_decode($query,true);
      if($row['dataCount'] > 0){
        foreach ($row['data'] as $value) {
    ?>
    <div class="col-sm-12">
      <a class="content-link" href="../shop_detail/?mem_id=<?=$value['mem_id']?>">
        <div class="blog-content">
          <div class="image-box">
            <img class="img-responsive-tt" src="../../images/memberJob/imgThum/<?=empty($value['memberJob_banner'])?"post-01.jpg":$value['memberJob_banner']?>">
          </div>
          <h4 data-mh="my-group"><?=!empty($value['mem_nickname'])?$value['mem_nickname']:$value['mem_fname'].' '.$value['mem_lname']?></h4>
          <p>฿<?=number_format($value['rate_min'])?> – ฿<?=number_format($value['rate_max'])?></p>
        </div>
      </a>
    </div>
    <?php } } ?>
  </div>
</div>
