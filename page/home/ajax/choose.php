<section class="choose">

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="main_heading">
          <h2>WHY CHOOSE US</h2>
          <!-- <p>Fastest repair service with best price!</p> -->
        </div>
      </div>
    </div>

    <div class="row">
      <?php
        $sql = "SELECT * FROM t_choose WHERE is_active = 'Y' ORDER BY tc_seq ASC";
        $query = DbQuery($sql,null);
        $json = json_decode($query,true);
          if($json['dataCount'] > 0){
            foreach ($json['data'] as $value) {
      ?>
      <div class="col-md-4 col-sm-6">
        <div class="card">
          <img class="img-responsive" src="../../images/choose/<?=$value['tc_img']?>">
          <h4><?=$value['tc_text']?></h4>
        </div>
      </div>
      <?php }} ?>

    </div>
  </div>

</section>
