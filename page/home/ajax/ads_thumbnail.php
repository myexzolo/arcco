<div class="container" style="margin-top:40px;">
  <div class="owl-carousel owl-theme ads-t">
  <?php
    $sql   = "SELECT * FROM t_ads
              WHERE is_active = 'Y'
              AND b_type = 'T'
              AND NOW() BETWEEN b_datestart AND b_dateend
              ORDER BY b_seq ASC";
    $query = DbQuery($sql,null);
    $json   = json_decode($query, true);
    if($json['dataCount'] > 0){
      foreach ($json['data'] as $key => $value){
  ?>
    <div class="item">
      <a href="<?=$value['b_url']?>" target="<?=$value['b_target']?>">
        <img class="img-full" src="../../images/ads/<?=$value['b_img']?>" alt="<?=$value['b_url']?>">
      </a>
    </div>
    <?php } ?>
  <?php } ?>
  </div>
</div>
