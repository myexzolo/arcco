<section class="about">

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="main_heading">
          <h2><?=$title_name?></h2>
          <!-- <p>Fastest repair service with best price!</p> -->
        </div>
      </div>
    </div>

    <div class="row">
      <?php if($ida == 1){ ?>
      <div class="col-md-<?=$col?> col-xs-12">
        <img class="img-full" src="../../images/about/<?=$path?>" alt="about">
      </div>
      <div class="col-md-<?=$col?> col-xs-12">
        <div class="about-content">
          <h3><?=$title_name?></h3>
          <?=$detail?>
        </div>
      </div>
      <?php }else{ ?>
      <div class="col-md-<?=$col?> col-xs-12">
        <div class="about-content">
          <?=$detail?>
        </div>
      </div>
      <?php } ?>

    </div>
  </div>

</section>
