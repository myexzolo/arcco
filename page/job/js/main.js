function getTypeProject(el){
  if(el.value == 6){
    $('#show-data').html(
      '<div class="form-group">'+
        '<label>ระบุประเภทโครงการ</label>'+
        '<input type="text" name="tj_type_name" class="form-control" required>'+
      '</div>'
    );
  }else{
    $('#show-data').html('');
  }
}

$('.createJob').on('submit', function(event) {
  // $('#tj_detail').val(dataEditor2.getData());
  event.preventDefault();
  if ($('.createJob').smkValidate()) {
    $.ajax({
        url: 'service/createJob.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        window.location='../project_detail/';
      }
    });
  }
});
