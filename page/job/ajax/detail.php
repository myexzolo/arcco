<section class="blog pdd-t-0">
  <div class="container">
    <div class="blog-profile">

    <div class="row">
      <div class="col-md-12 col-xs-12">
        <div class="checkout-form">
          <form class="formLogin createJob" id="create-Job" novalidate>
            <input type="hidden" name="mem_id" value="<?=$_SESSION['member']['mem_id']?>">
            <div class="col-md-12 col-xs-12">
              <div class="form-group">
                <label>ชื่อโปรเจค</label>
                <input type="text" name="tj_name" class="form-control" required>
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>พื้นที่ (ตารางเมตร)</label>
                <input type="text" name="tj_area" class="form-control" required>
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>ประเภท</label>
                <select class="form-control" name="ttb_id">
                  <option value="">ประเภท</option>
                  <?php
                    $sql = "SELECT * FROM t_type_build WHERE is_active ='Y'";
                    $query = DbQuery($sql,null);
                    $row = json_decode($query,true);
                    if($row['dataCount'] > 0){
                      foreach ($row['data'] as $value) {
                  ?>
                  <option value="<?=$value['ttb_id']?>"><?=$value['ttb_name']?></option>
                  <?php }} ?>
                </select>
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>ประเภทโครงการ</label>
                <select class="form-control" name="ttp_id" required onchange="getTypeProject(this)">
                  <option value="">เลือกประเภทโครงการ</option>
                  <?php
                    $sql = "SELECT * FROM t_type_project WHERE is_active ='Y'";
                    $query = DbQuery($sql,null);
                    $row = json_decode($query,true);
                    if($row['dataCount'] > 0){
                      foreach ($row['data'] as $value) {
                  ?>
                  <option value="<?=$value['ttp_id']?>"><?=$value['ttp_name']?></option>
                  <?php }} ?>
                </select>
              </div>
            </div>
            <div class="col-md-3 col-xs-12" id="show-data"></div>
            <div class="clearfix"></div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>ประเภทงาน</label>
                <select class="form-control" name="category_id" required>
                  <option value="">เลือกประเภทงาน</option>
                  <?php
                    $sqlc = "SELECT * FROM t_category";
                    $queryc = DbQuery($sqlc,null);
                    $rowc = json_decode($queryc,true);
                      if($rowc['dataCount'] > 0){
                        foreach ($rowc['data'] as $valuec) {
                  ?>
                  <option value="<?=$valuec['category_id']?>"><?=$valuec['category_name_th']?></option>
                  <?php }} ?>
                </select>
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>สถานที่ตั้ง</label>
                <select class="form-control select2" name="PROVINCE_CODE" style="width:100%;border-radius: 0px;" required>
                  <option value="">เลือกจังหวัด</option>
                  <?php
                    $sqlc = "SELECT * FROM data_mas_add_province";
                    $queryc = DbQuery($sqlc,null);
                    $rowc = json_decode($queryc,true);
                      if($rowc['dataCount'] > 0){
                        foreach ($rowc['data'] as $valuec) {
                  ?>
                  <option value="<?=$valuec['PROVINCE_CODE']?>"><?=$valuec['PROVINCE_NAME']?></option>
                  <?php }} ?>
                </select>
              </div>
            </div>
            <div class="col-md-3 col-xs-6">
              <div class="form-group">
                <label>ช่วงงบประมาณ</label>
                <input type="text" name="tj_budget_min" data-smk-type="currency" class="form-control" required>
              </div>
            </div>
            <div class="col-md-3 col-xs-6">
              <div class="form-group">
                <label>ถึง</label>
                <input type="text" name="tj_budget_max"  data-smk-type="currency" class="form-control" required>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-group">
                <label>รายละเอียดอื่นๆเพิ่มเติม</label>
                <textarea class="form-control" name="tj_detail" rows="6"></textarea>
              </div>
            </div>
            <div class="col-md-6 col-xs-12">
              <div class="form-group">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="tj_arcco" value="Y">
                    ต้องการครบทีมพร้อมก่อสร้างโดยให้ทีมงานArcco จัดหาให้
                  </label>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Reference</label>
                <input type="file" name="tj_img" onchange="readURL(this,'showImg')" accept="image/png,image/jpeg" required>
                <p class="help-block">แนบภาพตัวอย่างงานสไตล์ที่คุณชอบ</p>
              </div>
            </div>
            <div class="col-md-6">
              <div id="showImg"></div>
            </div>
            <div class="col-xs-12">
              <div class="btn-bottom text-center">
                <button type="submit" class="btn btn-default">ลงประกาศงาน</button>
              </div>
            </div>
          </form>
        </div>
      </div>


    </div>
  </div>
  </div>

</section>
