

showData();

function showData(){
  $.post("service/showData.php")
    .done(function( data ) {
      $('#show-data').html(data);
  });
}

function del(tj_id,id){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล?',
    accept:'ยืนยัน',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("service/del.php",{tj_id:tj_id,id:id})
        .done(function( data ) {
          $.smkAlert({text: data.message,type: data.status});
          showData();
      });
    }
  });
}

function save(){
  $.smkConfirm({
    text:'ต้องการบันทึกข้อมูล?',
    accept:'ยืนยัน',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("service/save.php")
        .done(function( data ) {
          $.smkAlert({text: data.message,type: data.status});
          if(data.status == 'success'){
            window.location='../project_detail/';
          }
      });
    }
  });
}
