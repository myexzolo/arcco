<?php

include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');
require_once '../../../admin/PHPMailer/PHPMailerAutoload.php';
include('../../../lib/line/notify.php');

$tj_id = $_POST['id'];

$arr['mem_id'] = $_SESSION['member']['mem_id'];
$sql = "SELECT * FROM t_job j , orders o
        WHERE j.tj_id = o.tj_id
        AND j.tj_status = 'A'
        AND j.is_acive = 'Y'
        AND o.tj_id = '$tj_id'";
$query = DbQuery($sql,null);
$row = json_decode($query,true);
if($row['dataCount'] > 0){
  $json = $row['data'][0];
  $arr['o_id'] = $json['o_id'];
  $cus_id      = $json['cus_id'];

  $sqlq = "SELECT * FROM order_detail WHERE o_id = '{$json['o_id']}' AND mem_id = '{$arr['mem_id']}'";
  $queryq = DbQuery($sqlq,null);
  $rowq = json_decode($queryq,true);
  if($rowq['dataCount'] == 0){
    $sql  = DBInsertPOST($arr,'order_detail');
    $query      = DbQuery($sql,null);
    $row        = json_decode($query, true);
    $errorInfo  = $row['errorInfo'];
    if(intval($row['errorInfo'][0]) == 0){

      $sqlm   = "SELECT * FROM t_member WHERE mem_id = '$cus_id'";
      $querym = DbQuery($sqlm,null);
      $rowm   = json_decode($querym,true)['data'][0];
      $fullname = $rowm['mem_fname'].' '.$rowm['mem_lname'];
      $arr['email'] = $rowm['mem_email'];
      $arr['title'] = '[ARCCO] NOTIFICATION Alert : Interest';
      $arr['message'] = '
<p>Hi, '.$fullname.'<br />
Someone is interested in work. Please check menu <b>"Hiring project"</b> </p>
<p><a href="https://arcco-th.com/page/project_detail/?action=hiring">https://arcco-th.com/page/project_detail/?action=hiring</a></p>
<p>Received thie message by arcco.<br />
You may have received this email in error<br />
because another customer entered this email address  by arcco.<br />
if you received this message by arcco. please delete  this email</p>
<p>________________</p>
<p>arcco Team.<br />
________________</p>
';
      if(mailsendMail($arr) == 200){

        sendLineMessages($rowm['line_user_id'],ltrim(strip_tags($arr['message'])));
        header('Content-Type: application/json');
        exit(json_encode(array('status' => 'success','message' => 'Success')));
      }else{
        header('Content-Type: application/json');
        exit(json_encode(array('status' => 'success','message' => 'Success and not send mail')));
      }

    }else{
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'danger','message' => 'Fail')));
    }
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Project นี้ถูกเพิ่มแล้ว')));
  }

}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Project ปิดแล้ว')));
}



?>
