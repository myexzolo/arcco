<section class="blog pdd-t-0">

  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <?php
            $sql = "SELECT * FROM t_job j , data_mas_add_province p, t_category c WHERE j.PROVINCE_CODE = p.PROVINCE_CODE AND j.category_id = c.category_id AND j.tj_id = '{$_GET['id']}'";
            $query = DbQuery($sql,null);
            $row = json_decode($query,true);
            if($row['dataCount'] > 0){
              $value = $row['data'][0];
          ?>
          <div class="card card-body">
            <div class="row">
            <div class="jumbotron">
            <img src="../../images/imgJob/<?=$value['tj_img']?>">
            <h3><?=$value['tj_name']?></h3>
            <hr>
            <p>
            <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
            <span class="pp">ประเภทงาน:</span>
            <span class="p3"><?=$value['category_name_th']?></span>

            <p>
            <span class="glyphicon glyphicon-xbt" aria-hidden="true"></span>
            <span class="pp">ประมาณการณ์:</span>
            <span class="p3"><?=$value['tj_budget_min'].' - '.$value['tj_budget_max']?></span>

            <p>
            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
            <span class="pp">สถานที่ปฏิบัติงาน:</span>
            <span class="p3"><?=$value['PROVINCE_NAME']?></span>

            <p class="p1">รายละเอียดงาน</p>
              <?=$value['tj_detail']?>
                </div>

                <div class="col-xs-12">
                  <div class="btn-bottom text-center">
                    <button type="button" onclick="addToProject('<?=$value['tj_id']?>')" class="btn btn-default">add to project</button>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>

      </div>
    </div>
  </div>

</section>
