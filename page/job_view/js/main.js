
function addToProject(id){

  $.smkConfirm({
    text:'ต้องการรับโปรเจคนี้?',
    accept:'ยืนยัน',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("service/addToProject.php",{id:id})
        .done(function( data ) {
          $.smkAlert({text: data.message,type: data.status});
      });
    }
  });
}
