<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PAY | ARCCO</title>

    <?php include "../../inc/css.php" ?>
    <link rel="stylesheet" href="css/main.css">

  </head>
  <body>
    <?php include "../../inc/nav.php" ?>

    <!-- body function -->
    <?php include "ajax/header.php" ?>
    <?php include "ajax/payment.php" ?>
    <!-- End body function -->

    <?php include "../../inc/footer.php" ?>
    <?php include "../../inc/js.php" ?>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://unpkg.com/imask"></script>
    <script type="text/javascript" src="https://cdn.omise.co/omise.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

  </body>
</html>
