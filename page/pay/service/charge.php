<?php

require_once str_replace("page/pay/service/","",dirname(__FILE__).'/omise/lib/Omise.php');
include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');
include('../../../lib/line/notify.php');

define('OMISE_PUBLIC_KEY', 'pkey_test_5n8dhvvzjqhpbsfim4x');
define('OMISE_SECRET_KEY', 'skey_test_5n8dhvvztakq0qppoxj');

$token          = $_POST['token'];
$type           = $_POST['type'];
$installment_id = $_POST['installment_id'];

$sql = "SELECT * FROM order_installment WHERE installment_id = '$installment_id'";
$query      = DbQuery($sql,null);
$row        = json_decode($query, true)['data'][0];
$installment_no = $row['installment_no'];
// select DB
$qrcode = '';
$arr_charge = array(
  'amount' => $row['installment_price']*100,
  'currency' => 'thb'
);

if($type == 'Debit/Credit'){
  $arr_charge['card'] = $token;
}
if($type == 'promptpay'){
  $arr_charge['source'] = $token;
}
$charge = OmiseCharge::create($arr_charge);
if($charge['status'] == 'successful' || ($charge['status'] == "pending" && $type == 'promptpay')){
  $status = 'Y';
  if($type == 'promptpay'){
    $status = 'P';
    $qrcode = $charge['source']['scannable_code']['image']['download_uri'];
  }

  $arr['type_pay'] = $type;
  $arr['token'] = $charge['id'];
  $arr['status_pay'] = $status;
  $arr['img_thaiqr'] = $qrcode;
  $arr['installment_id'] = $installment_id;
  $sql = DBUpdatePOST($arr,'order_installment','installment_id');
  $query      = DbQuery($sql,null);
  $row        = json_decode($query, true);
  $errorInfo  = $row['errorInfo'];

  if(intval($row['errorInfo'][0]) == 0){

    $sql = "SELECT o.tj_id,oi.installment_price FROM order_installment oi , order_detail od , orders o
            WHERE oi.od_id = od.od_id AND od.o_id = o.o_id
            AND installment_id = '$installment_id'";
    $query = DbQuery($sql,null);
    $rows   = json_decode($query, true)['data'][0];
    $installment_price = $rows['installment_price'];
    $arr = array();
    $text = 'รอการชำระเงิน';
    if($status == 'Y'){
      $text = 'ชำระเงินเรียบร้อย';
    }
    $status_pay = "สถานะการชำระเงิน (<u>$text</u>)";
    $arr['te_text'] = 'รายการชำระเงินงวดที่ '.$installment_no.'<br />ช่องทางการชำระ : '.$type.'<br />'.$status_pay;
    $arr['user_id'] = $_SESSION['member']['mem_id'];
    $arr['tj_id'] = $rows['tj_id'];
    $arr['te_user_type'] = 'sys-cus';
    $sqli = DBInsertPOST($arr,'t_event_log');
    DbQuery($sqli,null);

    $sqll = "SELECT j.tj_tracking_no,m.mem_fname,m.mem_lname,m.mem_mobile,m.mem_email,NOW() AS todaydate FROM t_job j, t_member m
            WHERE j.cus_id = m.mem_id AND j.tj_id = '{$rows['tj_id']}'";
    $queryl = DbQuery($sqll,null);
    $rowsl   = json_decode($queryl, true)['data'][0];
    $tj_tracking_no = $rowsl['tj_tracking_no'];
    $todaydate  = $rowsl['todaydate'];
    $installment_price = number_format($installment_price);
    $fullname = $rowsl['mem_fname'].' '.$rowsl['mem_lname'];
    $mem_mobile = $rowsl['mem_mobile'];
    $mem_email = $rowsl['mem_email'];

    $message_data['message'] = "\r\nแจ้งรายการชำระเงิน\r\nรหัสโปรเจค $tj_tracking_no\r\nจำนวน $installment_price บาท\r\nTimestamp $todaydate\r\nรูปแบบการชำระ $type\r\n----------------\r\nข้อมูลติดต่อ\r\n$fullname\r\n$mem_mobile\r\n$mem_email\r\n----------------\r\nตรวจสอบข้อมูล\r\nhttps://dashboard.omise.co/signin";
    if(send_notify_message($message_data)['status'] == 200){
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'success', 'title' => 'Congratulations','message' => 'Pay Success' , 'qrpayment' => $qrcode , 'id' => $rows['tj_id'])));
    }else{
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'success', 'title' => 'Congratulations','message' => 'Transfer Success noi notify' , 'qrpayment' => $qrcode , 'id' => $rows['tj_id'])));
    }


  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'error', 'title' => 'Some Things Wronging', 'message' => 'Fail')));
  }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'error', 'title' => 'Some Things Wronging', 'message' => 'Charge Fail')));
}

?>
