<section class="about">
  <?php
    $sqlp = "SELECT * FROM t_payment";
    $queryp = DbQuery($sqlp,null);
    $rowp   = json_decode($queryp,true)['data'];
  ?>

  <div class="container rep">
    <div class="row">
      <div class="col-xs-12">
        <div class="main_heading">
          <h2>ARCCO Payment</h2>
        </div>
      </div>
    </div>

    <div class="row">
      <?php //$_POST['installment_id'] = 1; ?>
      <?php if(isset($_POST['installment_id'])){ ?>
        <div class="col-sm-5 pdd-5">

          <?php
              $sql = "SELECT * FROM
                        order_installment oi ,
                        order_detail od ,
                        orders o ,
                        t_job tj
                      WHERE oi.od_id = od.od_id
                      AND od.o_id = o.o_id
                      AND o.tj_id = tj.tj_id
                      AND oi.installment_id = '{$_POST['installment_id']}'";
              $query = DbQuery($sql,null);
              $row   = json_decode($query,true);
              if($row['dataCount'] == 1){
                $value = $row['data'][0];

                //print_r($value);
          ?>
          <input type="hidden" id="price" value="<?=$value['installment_price']?>">
          <div class="row">
            <div class="col-sm-12 pdd-5">
              <div class="box-order">
                <h2>Order Summary</h2>
                <table class="table">
                  <thead>
                    <tr>
                      <td colspan="2"><?=$value['tj_name']?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        งวดที่ <?=$value['installment_no']?> (<?=$value['installment_per']?>%) <br />
                        <span class="detail">รายละเอียด</span><br />
                        <div class="pay-detail"><?=$value['installment_detail']?></div>
                      </td>
                      <td class="text-right"><?=number_format($value['installment_price']);?></td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td class="price">Order Total:</td>
                      <td class="price text-right"><strong><?=number_format($value['installment_price']);?></strong></td>
                    </tr>
                  </tfoot>
                </table>
              </div>

              <?php } ?>
            </div>
          </div>
        </div>

        <div class="col-sm-5 col-sm-offset-2">
        <div class="<?=$rowp[0]['tp_active']!='Y'?"none":""?>">
          <img width="100%" src="../../images/arcco-th.jpg">
          <form id="formTranfPay" class="contact-form" novalidate>
            <input type="hidden" id="installment_id" name="installment_id" value="<?=$value['installment_id']?>" required>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="text-left" style="display: block;">อัพโหลดหลักฐานการชำระเงิน</label>
                  <input type="file" name="img_transfer" class="form-control input-lg" onchange="readURL(this,'showImage','100%')" required>
                </div>
                <div id="showImage"></div>
              </div>
              <div class="col-xs-12 pdd-5">
                <div class="btn-bottom text-center">
                  <button type="submit" class="btn btn-default btn-block">แจ้งชำระบริการ</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="<?=$rowp[1]['tp_active']!='Y'?"none":""?>">
          <form id="formPayment" class="contact-form" novalidate>
          <input type="hidden" id="installment_id" value="<?=$value['installment_id']?>" required>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <input type="text" id="card-holder" class="form-control input-lg" placeholder="Card Number" required>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <input type="text" id="card-name" class="form-control input-lg" placeholder="Card Holder's Name" required>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <input type="tel" id="card-phone" class="form-control input-lg" value="<?=$_SESSION['member']['mem_mobile']?>" placeholder="Phone Number" required>
              </div>
            </div>
            <div class="col-sm-5 pdd-5">
              <div class="form-group">
                <select class="form-control input-lg" id="card-month" required>
                  <option value="">MM</option>
                  <?php for ($i=1; $i <=12 ; $i++) { ?>
                  <option value="<?=strlen($i)==1?"0$i":$i?>"><?=strlen($i)==1?"0$i":$i?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-5 col-sm-offset-2 pdd-5">
              <div class="form-group">
                <select class="form-control input-lg" id="card-year" required>
                  <option value="">YYY</option>
                  <?php for ($i=date('Y')+10; $i >=date('Y') ; $i--) { ?>
                  <option value="<?=$i?>"><?=$i?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-5 pdd-5">
              <img class="mastercard" src="../../images/payment/mastercard.png" alt="mastercard">
            </div>
            <div class="col-sm-5 col-sm-offset-2 pdd-5">
              <div class="form-group">
                <input type="text" maxlength="3" minlength="3" class="form-control input-lg" id="card-cvc" placeholder="CVC" required>
              </div>
            </div>
            <div class="col-xs-12 pdd-5">
              <div class="btn-bottom text-center">
                <button type="submit" class="btn btn-default btn-block">Pay with Debit/Credit Card</button>
              </div>
            </div>
          </div>
        </form>
        </div>
        <div class="<?=$rowp[2]['tp_active']!='Y'?"none":""?>">
        <div class="row">
          <div class="col-xs-12 pdd-5">
            <img width="100%" src="../../images/payment/btn-promptpay.png" alt="btn-promptpay">
            <div class="btn-bottom text-center">
              <button type="button" onclick="promptpayToken()" class="btn btn-default btn-block">Pay with Promptpay</button>
            </div>
          </div>
        </div>
        </div>
      </div>
    <?php }else{ ?>
        <div class="col-xs-12">
          <div class="alert alert-danger" role="alert">ไม่พบข้อมูลการชำระเงิน</div>
        </div>
    <?php } ?>
  </div>

</section>
