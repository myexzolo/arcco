Omise.setPublicKey("pkey_test_5n8dhvvzjqhpbsfim4x");


var phoneMask = IMask(
document.getElementById('card-holder'), {
  mask: '0000-0000-0000-0000'
});

$('#formPayment').on('submit', function(event) {
  event.preventDefault();
  if ($('#formPayment').smkValidate()) {
    var installment_id  = $('#installment_id').val();
    var obj = {
      "expiration_month" : $('#card-month').val(),
      "expiration_year" : $('#card-year').val(),
      "name" : $('#card-name').val(),
      "number" : $('#card-holder').val().split('-').join(''),
      "phone_number" : $('#card-phone').val(),
      "security_code" : $('#card-cvc').val(),
    };
    DCcardToken(obj,installment_id);
  }
});

$('#formTranfPay').on('submit', function(event) {
  event.preventDefault();
  if ($('#formTranfPay').smkValidate()) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes !'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
            url: 'service/chargeTran.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          Swal.fire({
            title: data.title,
            text: data.message,
            icon: data.status,
            showCancelButton: false,
            confirmButtonText: 'Back To Profile',
            backdrop: false
          }).then((result) => {
            if (result.isConfirmed) {
              window.location='../project/?tj_id='+data.id;
            }
          });
        });
      }
    });
  }
});

function  DCcardToken(tokenParameters,id){
  Omise.createToken("card",tokenParameters,
  function(statusCode, response) {
    if(response.object == "error"){
      Swal.fire({
        icon: 'error',
        title: response.code,
        text: response.message
      });

    }else{
      charge(response.id,id,'Debit/Credit');
    }
  });
}

function promptpayToken(){
  var price = $('#price').val();
  Omise.createSource('promptpay', {
    "amount": price*100,
    "currency": "THB"
  }, function(statusCode, response) {
    if(response.object == "error"){
      Swal.fire({
        icon: 'error',
        title: response.code,
        text: response.message
      });

    }else{
      var installment_id  = $('#installment_id').val();
      charge(response.id,installment_id,'promptpay');
    }
  });
}

function charge(token,installment_id,type){

  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes !'
  }).then((result) => {
    if (result.isConfirmed) {
      $.post("service/checkStatusCharge.php",{installment_id:installment_id})
        .done(function( data ) {
          if(data.status == 'success'){
            $.post("service/charge.php",{token:token,installment_id:installment_id,type:type})
              .done(function( data ) {
                if(data.qrpayment != ''){

                  Swal.fire({
                    title: data.title,
                    icon: data.status,
                    imageUrl: data.qrpayment,
                    imageHeight: 350,
                    imageAlt: 'QR PAYMENT',
                    showCancelButton: false,
                    confirmButtonText: 'Back To Profile',
                    backdrop: false
                  }).then((result) => {
                    if (result.isConfirmed) {
                      window.location='../project/?tj_id='+data.id;
                    }
                  });

                }else{
                  Swal.fire({
                    title: data.title,
                    text: data.message,
                    icon: data.status,
                    showCancelButton: false,
                    confirmButtonText: 'Back To Profile',
                    backdrop: false
                  }).then((result) => {
                    if (result.isConfirmed) {
                      window.location='../project/?tj_id='+data.id;
                    }
                  });
                }
            });
          }else{
            Swal.fire({
              title: data.title,
              text: data.message,
              icon: data.status,
              showCancelButton: false,
              confirmButtonText: 'Back To Profile',
              backdrop: false
            }).then((result) => {
              if (result.isConfirmed) {
                window.location='../project/?tj_id='+data.id;
              }
            });
          }
      });
    }
  });
}
