<section class="about">

  <div class="container">

    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#findjob" aria-controls="Find Job" role="tab" data-toggle="tab">
                Find Job
              </a>
            </li>
            <li role="presentation">
              <a href="#createjob" aria-controls="Create Job" role="tab" data-toggle="tab">
                Create Job
              </a>
            </li>
          </ul>
          <!-- Find Job -->
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="findjob">
              <div class="col-md-12">
                <div class="row">
                  <form class="form-inline">
                   <div class="form-group" id="jobtitle" >
                    <input type="jobtitle" class="form-control" id="jobtitle" placeholder="Job Title"> 
                   </div>
                  <select class="form-control" id="alljob">
                    <option>Buiding & Construction (All)</option>
                    <option>Architectural Services</option>
                    <option>Buiding & Construction / QS</option>
                    <option>Civil / Structural</option>
                  </select>
                  <select class="form-control" id="alllocation">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      <option>All Locations</option>
                      <option>Bangkok</option>
                      <option>Kamphaeng Phet</option>
                      <option>Chai Nat</option>
                      <option>Nakhon Nayok</option>
                      <option>Nakhon Pathom</option>
                      <option>Nakhon Sawan</option>
                      <option>Nonthaburi</option>
                      <option>Pathum Thani</option>
                      <option>Phra Nakhon Si Ayutthaya</option>
                      <option>Phichit</option>
                      <option>Phitsanulok</option>
                      <option>Phetchabun</option>
                      <option>Lopburi</option>
                      <option>Samut Prakan</option>
                      <option>Samut Songkhram</option>
                      <option>Samut Sakhon</option>
                      <option>Sing Buri</option>
                      <option>Suphan Buri</option>
                      <option>Saraburi</option>
                      <option>Ang Thong</option>
                      <option>Uthai Thani</option>
                      <option>Chanthaburi</option>
                      <option>Chachoengsao</option>
                      <option>Chonburi</option>
                      <option>Trat</option>
                      <option>Prachinburi</option>
                      <option>Rayong</option>
                      <option>Sa Kaeo</option>
                      <option>Kanchanaburi</option>
                      <option>Tak</option>
                      <option>Prachuap Khiri Khan</option>
                      <option>Phetchaburi</option>
                      <option>Ratchaburi</option>
                      <option>Kalasin</option>
                      <option>Khon Kaen</option>
                      <option>Chaiyaphum</option>
                      <option>Nakhon Phanom</option>
                      <option>Nakhon Ratchasima</option>
                      <option>Bueng Kan</option>
                      <option>Buriram</option>
                      <option>Maha Sarakham</option>
                      <option>Mukdahan</option>
                      <option>Yasothon</option>
                      <option>Roi Et</option>
                      <option>Loei</option>
                      <option>Sakon Nakhon</option>
                      <option>Surin</option>
                      <option>Sisaket</option>
                      <option>Nong Khai</option>
                      <option>Nong Bua Lamphu</option>
                      <option>Udon Thani</option>
                      <option>Ubon Ratchathani</option>
                      <option>Amnat Charoen</option>
                      <option>Chiang Mai</option>
                      <option>Chiang Rai</option>
                      <option>Lampang</option>
                      <option>Lamphun</option>
                      <option>Mae Hong Son</option>
                      <option>Nan</option>
                      <option>Phayao</option>
                      <option>Phrae</option>
                      <option>Uttaradit</option>
                      <option>Krabi</option>
                      <option>Chumphon</option>
                      <option>Trang</option>
                      <option>Nakhon Si Thammarat</option>
                      <option>Narathiwat</option>
                      <option>Pattani</option>
                      <option>Phang Nga</option>
                      <option>Phatthalung</option>
                      <option>Phuket</option>
                      <option>Ranong</option>
                      <option>Satun</option>
                      <option>Songkhla</option>
                      <option>Surat Thani</option>
                      <option>Yala</option>
                    </select>
                    <a href="">
                  <button type="submit" class="btn btn-default" id="btnfindjob">               
                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                      <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                    </svg>
                      <i class="bi bi-search"></i> Find Jobs 
                  </button>   
</a>              
                  <div class="range-wrap">
                    <label for="formControlRange">Salary</label>
                     <input type="range" class="form-control-range" id="formControlRange" onInput="$('#rangeval').html($(this).val())">
                      <span id="rangeval">0<!-- Default value --></span>
                  </div>

                  <!--Work List-->
                  <div class="row">
      <div class="col-xs-12">
        <div class="main_heading">
          <h2>WE ARE LEADING COMPANY</h2>
          <p>Fastest repair service with best price!</p>
        </div>
      </div>
    </div>

    <div class="row">
      <?php
        $sql   = "SELECT * FROM t_about_us";
        $query = DbQuery($sql,null);
        $json   = json_decode($query, true);
        if($json['dataCount'] > 0){
          $title_name = $json['data'][0]['title_name'];
          $path       = $json['data'][0]['path'];
          $detail     = $json['data'][0]['detail'];
      ?>
      <div class="col-md-6 col-xs-12">
        <img class="img-full" src="../../images/about/<?=$path?>" alt="about">
      </div>
      <div class="col-md-6 col-xs-12">
        <div class="about-content">
          <h3><?=$title_name?></h3>
          <?=$detail?>
        </div>
      </div>
      <?php } ?>

    </div>





                  </form>
                </div>
              </div>
            </div>

            <!-- Create Job -->
            <div role="tabpanel" class="tab-pane" id="createjob">
              <div class="col-sm-6">
                <div class="form-group">
                 <input type="jobtitle" class="form-control" id="jobtitle2" placeholder="Job Title">  
                </div>
              </div> 
            <div class="col-sm-6">
              <div class="form-group">
               <select class="form-control" id="joblocation">
                <option>Job Location</option>
                <option>Accounting</option>
                <option>Admin & HR</option>
                <option>Media & Advertising</option>
                <option>Banking/Finance</option>
               </select>
              </div>
            </div>
            <div class="col-sm-6">  
              <div class="input-group">
               <span class="input-group-addon">Salary (฿)</span>
                <select class="form-control" id="salary1">
                  <option>10,000</option>
                  <option>20,000</option>
                  <option>30,000</option>
                  <option>40,000</option>
                  <option>50,000</option>
                  <option>More than 50,000</option>
                </select>
              </div>
            </div>
            <div class="col-sm-6">
             <div class="form-group">
              <select class="form-control" id="salary2">
                <option>10,000</option>
                <option>20,000</option>
                <option>30,000</option>
                <option>40,000</option>
                <option>50,000</option>
                <option>More than 50,000</option>
              </select>
             </div>
            </div>
        <div class="col-sm-12">
          <form>
            <div class="form-group">
              <textarea class="form-control" rows="5" id="comment" placeholder="Job description.."></textarea>
            </div>
          </form>
        </div>

        <!--Job Type-->
        <div class="col-sm-12">
          <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#" id="jobtype1">Full Time</a></li>
            <li role="presentation" class="active"><a href="#" id="jobtype2">Part Time</a></li>
            <li role="presentation" class="active"><a href="#" id="jobtype3">Contract</a></li>
            <li role="presentation" class="active"><a href="#" id="jobtype4">Internship</a></li>
            <div class="checkbox">
            <label>
              <input type="checkbox"> Work from home/Remote
            </label>
            </div>
          </ul>
        </div>

        <!--Choose img-->
        <div class="col-sm-12">
          <div class="form-group">
            <label>ภาพประกอบ</label>
              <input type="file">
          </div>
        </div>
        <div class="col-sm-12"> 
          <button type="button" class="btn btn-default" id="btnsubmit" data-toggle="modal" data-target="#myModal" onclick="alertD()">SUBMIT</button>
        </div>
            


            </div>

          </div>
        </div>
      </div>
    </div>
  </div>


</section>


 




