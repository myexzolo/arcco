<?php
  include '../../../admin/inc/function/connect.php';
?>


<section class="blog">

  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-xs-12">
        <div class="row">
          <?php
            $sql = "SELECT * FROM t_category WHERE is_active = 'Y' ORDER BY category_seq ASC";
            $query = DbQuery($sql,null);
            $row   = json_decode($query,true);
            if($row['dataCount'] > 0){
              foreach ($row['data'] as $value) {
          ?>
          <div class="col-sm-4 col-sm-6 col-xs-12">
            <a class="content-link" href="../shop/?category_id=<?=$value['category_id']?>">
              <div class="blog-content">
                <img class="img-full" src="../../images/service/<?=$value['category_path']?>" alt="service">
                <h4><?=$value['category_name_th']?></h4>
                <p><?=$value['category_name_en']?></p>
              </div>
            </a>
          </div>
          <?php } } ?>
        </div>
      </div>
      <div class="col-sm-3 col-xs-12">
        <div class="about-content">
          <h3>TAG</h3>
          <div class="side_bar_blog">
            <div class="tags">
              <ul>
                <?php
                  $sql = "SELECT * FROM t_category WHERE is_active = 'Y' ORDER BY category_seq ASC";
                  $query = DbQuery($sql,null);
                  $row =  json_decode($query,true);
                  if($row['dataCount'] > 0){
                    foreach ($row['data'] as $value) {
                ?>
                <li><a class="<?=@$category_id==$value['category_id']?"tag-active":""?> " href="../shop/?category_id=<?=$value['category_id']?>"><?=$value['category_name_th']?></a></li>
                <?php }} ?>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

</section>
