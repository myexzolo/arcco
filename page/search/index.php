<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SEARCH | ARCCO</title>

    <?php include "../../inc/css.php" ?>
    <link rel="stylesheet" href="css/main.css">

  </head>
  <body>
    <?php include "../../inc/nav.php" ?>

    <!-- body function -->
    <?php include "ajax/header.php" ?>
    <?php include "ajax/search.php" ?>



    <?php //include "ajax/detail.php" ?>
    <!-- End body function -->

    <?php include "../../inc/footer.php" ?>
    <?php include "../../inc/js.php" ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.dotdotdot/4.1.0/dotdotdot.js" integrity="sha512-y3NiupaD6wK/lVGW0sAoDJ0IR2f3+BWegGT20zcCVB+uPbJOsNO2PVi09pCXEiAj4rMZlEJpCGu6oDz0PvXxeg==" crossorigin="anonymous"></script>
    <script src="../../js/matchHeight.js"></script>
    <script src="js/main.js"></script>

  </body>
</html>
