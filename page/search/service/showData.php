<?php

include('../../../admin/inc/function/connect.php');
include('../../../admin/inc/function/mainFunc.php');

  $_POST['search'] = empty($_POST['search'])?"":$_POST['search'];
  $count = 0;
?>
<?php
  $search = '';
  $search .= empty($_POST['search'])?"":"AND (category_name_th LIKE '%{$_POST['search']}%'";
  $search .= empty($_POST['search'])?"":"OR category_name_en LIKE '%{$_POST['search']}%')";
  $sql = "SELECT * FROM t_category WHERE 1=1 $search";
  $query = DbQuery($sql,null);
  $row = json_decode($query,true);
  if($row['dataCount'] > 0){
    $count++;
?>
<?php foreach ($row['data'] as $key => $value) { ?>

<div class="box-detail">
  <div class="row">
    <div class="col-md-2 col-sm-3">
      <img width="100%" src="../../images/service/<?=$value['category_path']?>">
    </div>
    <div class="col-md-10 col-sm-9">
      <h3><?=str_replace($_POST['search'],"<mark>{$_POST['search']}</mark>",$value['category_name_th']);?></h3>
      <p><?=str_replace($_POST['search'],"<mark>{$_POST['search']}</mark>",$value['category_name_en']);?></p>
      <p>
        <a target="_blank" href="../service_detail/?category_id=<?=$value['category_id']?>">ดูเพิ่มเติม</a>
      </p>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>
<!-- job -->
<?php
  $search = '';
  $search .= empty($_POST['search'])?"":"AND (t.tj_name LIKE '%{$_POST['search']}%'";
  $search .= empty($_POST['search'])?"":" OR t.tj_detail LIKE '%{$_POST['search']}%'";
  $search .= empty($_POST['search'])?"":" OR c.category_name_th LIKE '%{$_POST['search']}%'";
  $search .= empty($_POST['search'])?"":" OR c.category_name_en LIKE '%{$_POST['search']}%'";
  $search .= empty($_POST['search'])?"":" OR t.tj_budget_min LIKE '%{$_POST['search']}%'";
  $search .= empty($_POST['search'])?"":" OR t.tj_budget_max LIKE '%{$_POST['search']}%')";
  $sql = "SELECT * FROM t_job t , t_category c WHERE t.category_id = c.category_id AND t.tj_status = 'A' $search";
  $query = DbQuery($sql,null);
  $row = json_decode($query,true);
  if($row['dataCount'] > 0){
    $count++;
?>
<?php foreach ($row['data'] as $key => $value) { ?>

<div class="box-detail">
  <div class="row">
    <div class="col-md-2 col-sm-3">
      <img width="100%" src="../../images/imgJob/<?=$value['tj_img']?>">
    </div>
    <div class="col-md-10 col-sm-9">
      <h3><?=str_replace($_POST['search'],"<mark>{$_POST['search']}</mark>",$value['tj_name']);?></h3>
      <p>Posted on <span class="text-name"><?=date('F j, Y',strtotime($value['date_create']))?></span> บริการ <a class="text-name" target="_blank" href="../service_detail/?category_id=<?=$value['category_id']?>"><?=str_replace($_POST['search'],"<mark>{$_POST['search']}</mark>",$value['category_name_th']);?></a></p>
      <p>Budget <?=str_replace($_POST['search'],"<mark>{$_POST['search']}</mark>",strip_tags($value['tj_budget_min']));?> - <?=str_replace($_POST['search'],"<mark>{$_POST['search']}</mark>",strip_tags($value['tj_budget_max']));?></p>
      <p class="message"><?=str_replace($_POST['search'],"<mark>{$_POST['search']}</mark>",strip_tags($value['tj_detail']));?></p>
      <p>
        <a target="_blank" href="../job_view/?id=<?=$value['tj_id']?>">ดูเพิ่มเติม</a>
      </p>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>
<!-- article -->
<?php
  $search = '';
  $search .= empty($_POST['search'])?"":"AND (title LIKE '%{$_POST['search']}%'";
  $search .= empty($_POST['search'])?"":"OR content LIKE '%{$_POST['search']}%')";
  $sql = "SELECT * FROM t_article WHERE 1=1 AND is_active = 'Y' $search";
  $query = DbQuery($sql,null);
  $row = json_decode($query,true);
  if($row['dataCount'] > 0){
    $count++;
?>
<?php
  foreach ($row['data'] as $key => $value) {

    if($value['type_user'] == 3){
      $sqlc = "SELECT user_name AS Fname FROM t_user WHERE user_id = '{$value['user_id']}'";
    }else{
      $sqlc = "SELECT mem_fname AS Fname FROM t_member WHERE mem_id = '{$value['user_id']}'";
    }
    $queryc = DbQuery($sqlc,null);
    $rowc   = json_decode($queryc,true);
    $name   = $rowc['data'][0]['Fname'];
?>

<div class="box-detail">
  <div class="row">
    <div class="col-md-2 col-sm-3">
      <img width="100%" src="../../images/article/<?=$value['article_img']?>">
    </div>
    <div class="col-md-10 col-sm-9">
      <h3><?=str_replace($_POST['search'],"<mark>{$_POST['search']}</mark>",$value['title']);?></h3>
      <p>Posted on <span class="text-name"><?=date('F j, Y',strtotime($value['date_create']))?></span> by <span class="text-name"><?=$name?></span></p>
      <p class="message"><?=mb_substr(strip_tags($value['content']),0,250,'UTF-8');?>[..]</p>
      <p>
        <a target="_blank" href="../blog_detail/?article_id=<?=$value['article_id']?>">ดูเพิ่มเติม</a>
      </p>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>

<?php if($count == 0){ ?>
<div class="alert alert-danger" role="alert">ไม่พบข้อมูล</div>
<?php } ?>
