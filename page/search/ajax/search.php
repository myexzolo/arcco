<section class="blog pdd-t-0">

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <form class="form-inline">
          <input class="form-control" id="alllocation" type="search" name="search" placeholder="Search for...">

          <!-- Btn Find Job -->
          <button class="btn btn-primary" type="submit" id="btnfindjob">
            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
              <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
            </svg>
            <i class="bi bi-search"></i> Find Job
          </button>
          <!-- Range -->
          <!-- <div style="margin-top:50px;" >
            <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="300000" data-slider-step="5" data-slider-value="[0,300000]" id="sl2" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="salaryrange">Salary: 0 - 300,000 (฿)</b><br />
          </div> -->
        </form>

        <h1 class="result">ผลลัพธ์</h1>
        <div id="show-data"></div>


    </div>
  </div>

</section>
