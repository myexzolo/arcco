
<div class="banner_section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="title-holder">
            <div class="title-holder-cell text-left">
              <h1 class="page-title">Search</h1>
              <p class="message-title">ค้นหาข้อมูล</p>
              <ol class="breadcrumb">
                <li>Home</li>
                <li>Search</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
