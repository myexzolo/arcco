<section class="blog pdd-t-0">

  <div class="container">

    <div class="blog-profile">
      <img src="https://i.picsum.photos/id/1012/300/300.jpg?hmac=FpdcKpoTxl0CZdLj7RjrTjISpiu8TtsesR9f4Fm2yOk" alt="profile">
      <div class="profile-name">
        <h2>ณธีนนท์ นันทโชคณัชพงษ์</h2>
        <p>ผู้ใช้งานทั่วไป</p>
      </div>

      <div class="btn-bottom text-right btn-0">
        <button type="submit" class="btn btn-default">อัพเกรด</button>
      </div>
    </div>

    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                Info
              </a>
            </li>
            <li role="presentation">
              <a href="#project" aria-controls="project" role="tab" data-toggle="tab">
                Project
              </a>
            </li>
            <li role="presentation">
              <a href="#blog" aria-controls="blog" role="tab" data-toggle="tab">
                Blog
              </a>
            </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
              <div class="profile-input">
                <div class="row">
                  <div class="col-md-8">
                    <form class="formLogin editProfile" novalidate>
                      <div class="row">
                        <div class="col-md-6 col-xs-12">
                          <input type="hidden" name="mem_id" value="<?=@$_SESSION['member']['mem_id']?>">
                          <div class="form-group">
                            <input type="text" value="<?=@$_SESSION['member']['mem_username']?>" class="form-control input-lg" placeholder="รหัสผู้ใช้งาน" required readonly>
                          </div>
                          <div class="form-group">
                            <input type="text" name="mem_fname" value="<?=@$_SESSION['member']['mem_fname']?>" class="form-control input-lg" placeholder="ชื่อ" required>
                          </div>
                          <div class="form-group">
                            <input type="text" name="mem_lname" value="<?=@$_SESSION['member']['mem_lname']?>" class="form-control input-lg" placeholder="นามสกุล" required>
                          </div>
                          <div class="form-group">
                            <input type="email" name="mem_email" value="<?=@$_SESSION['member']['mem_email']?>" class="form-control input-lg" placeholder="อีเมล์" required>
                          </div>
                          <div class="form-group">
                            <input type="tel" name="mem_mobile" value="<?=@$_SESSION['member']['mem_mobile']?>" class="form-control input-lg" placeholder="โทรศัพท์มือถือ" required>
                          </div>
                          <div class="btn-bottom text-right">
                            <button type="submit" class="btn btn-default">แก้ไขโปรไฟล์</button>
                          </div>
                        </div>

                        <div class="col-md-6 col-xs-12">

                        </div>


                      </div>
                    </form>
                  </div>
                </div>
              </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="project">

              <form class="formLogin">
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>ชื่อจริง *</label>
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>นามสกุล *</label>
                    <input type="password" class="form-control">
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>หมายเลขบัตรประชาชน *</label>
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>อีเมล์ *</label>
                    <input type="email" class="form-control">
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>ชื่อโปรเจค</label>
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>ประเภทโปรเจค</label>
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>งบประมาณ</label>
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>เกี่ยวกับงาน</label>
                    <textarea name="name" rows="4" class="form-control"></textarea>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>รายละเอียดงาน</label>
                    <textarea name="name" rows="4" class="form-control"></textarea>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>ภาพประกอบ</label>
                    <input type="file">
                    <p class="help-block">Example block-level help text here.</p>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="btn-bottom text-center">
                    <button type="submit" class="btn btn-default">ลงประกาศงาน</button>
                  </div>
                </div>
              </form>

            </div>
            <div role="tabpanel" class="tab-pane" id="blog">
              <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12 mar-30">
                  <div class="create-blog" data-mh="my-group" onclick="openModelcontent('ADD')">
                    <span class="glyphicon glyphicon-plus"></span>
                    <p>Add Blog</p>
                  </div>
                </div>

                <?php
                  $sql   = "SELECT * FROM t_blog ORDER BY blog_id DESC";
                  $query = DbQuery($sql,null);
                  $row   = json_decode($query,true);
                  if($row['dataCount'] > 0){
                    foreach ($row['data'] as $value) {
                ?>
                <div class="col-md-3 col-sm-4 col-xs-12 mar-30">
                  <a class="a-content" onclick="openModelcontent('EDIT','<?=$value['blog_id']?>')" href="javascript:void(0)">
                    <div class="blog-content" data-mh="my-group">
                      <img class="img-responsive" src="../../images/blog/<?=$value['blog_img']?>">
                      <div class="content">
                        <div class="post_time">
                          <p><i class="fa fa-clock-o"></i> <?=date('F j, Y',strtotime($value['date_create']));?></p>
                        </div>
                        <h4><?=$value['blog_name']?></h4>
                        <p class="eps"><?=strip_tags($value['blog_detail'])?></p>
                      </div>
                    </div>
                  </a>
                </div>
                <?php }} ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>


<!-- Modal -->
<div class="modal fade" id="myModalBlog" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Management Blog</h4>
      </div>
      <form id="formAddModuleBlog" novalidate enctype="multipart/form-data">
        <div id="show-formBlog"></div>
      </form>
    </div>
  </div>
</div>
