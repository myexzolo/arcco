<section class="about">

  <div class="container">
    <!-- <div class="row">
      <div class="col-xs-12">
        <div class="main_heading">
          <h2>WE ARE LEADING COMPANY</h2>
          <p>Fastest repair service with best price!</p>
        </div>
      </div>
    </div> -->

    <div class="row">

      <div class="col-xs-12">
        <div class="tab-info login-section">
          <p>Returning customer?
            <a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Click here to login</a>
          </p>
        </div>

        <div class="collapse" id="collapseExample">
          <div class="well">
            <div class="row">
              <div class="col-xs-12">
                <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing &amp; Shipping section.</p>
              </div>

              <form class="formLogin">
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>Username or email *</label>
                    <input type="text" class="form-control" placeholder="Username">
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>Password *</label>
                    <input type="password" class="form-control" placeholder="Password">
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="btn-bottom text-left">
                    <button type="submit" class="btn btn-default">LOGIN</button>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Remember me
                      </label>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-8 col-xs-12">
        <div class="checkout-form">
          <form class="formLogin">
            <div class="col-xs-6">
              <div class="form-group">
                <label>ชื่อจริง *</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label>นามสกุล *</label>
                <input type="password" class="form-control">
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label>หมายเลขบัตรประชาชน *</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label>อีเมล์ *</label>
                <input type="email" class="form-control">
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-group">
                <label>ชื่อโปรเจค</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label>ประเภทโปรเจค</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label>งบประมาณ</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-group">
                <label>เกี่ยวกับงาน</label>
                <textarea name="name" rows="4" class="form-control"></textarea>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-group">
                <label>รายละเอียดงาน</label>
                <textarea name="name" rows="4" class="form-control"></textarea>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-group">
                <label>ภาพประกอบ</label>
                <input type="file">
                <p class="help-block">Example block-level help text here.</p>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="btn-bottom text-center">
                <button type="submit" class="btn btn-default">ลงประกาศงาน</button>
              </div>
            </div>
          </form>
        </div>
      </div>


    </div>
  </div>

</section>
