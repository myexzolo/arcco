$(function(){
    $('.content > p').dotdotdot({
        ellipsis: '...',
        wrap : 'word',
        watch : true
    });
});


function openModelcontent(value,id=""){
  $.post("service/formBlog.php",{value:value,id:id})
    .done(function( data ) {
      $('#myModalBlog').modal({backdrop:'static'});
      $('#show-formBlog').html(data);
  });
}

$('.editProfile').on('submit', function(event) {
  event.preventDefault();
  if ($('.editProfile').smkValidate()) {
    $.ajax({
        url: 'service/editProfile.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        reloadLocation();
      }
    });
  }
});
