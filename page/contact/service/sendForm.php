<?php

include('../../../admin/inc/function/mainFunc.php');
include('../../../admin/inc/function/connect.php');
include('../../../lib/line/notify.php');

$sql        = DBInsertPOST($_POST,'t_contact');
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];
if(intval($row['errorInfo'][0]) == 0){
  $sMessage['message'] = "\r\nส่งข้อความ\r\nคุณ{$_POST['cont_name']}\r\nหัวข้อ: {$_POST['cont_topic']}\r\nรายละเอียด: {$_POST['cont_message']}\r\n----------------\r\nข้อมูลติดต่อกลับ\r\nอีเมล์: {$_POST['cont_email']}\r\nเบอร์โทร: {$_POST['cont_mobile']}";
  if(send_notify_message($sMessage)){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Line Fail')));
  }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}


?>
