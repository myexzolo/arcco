<section class="contact">

  <div class="container rep">
    <div class="row">
      <div class="col-md-6">
        <div class="address">
          <?php
            $sqls   = "SELECT * FROM t_about_us where id = 4";
            $querys = DbQuery($sqls,null);
            $json   = json_decode($querys, true)['data'][0];
            echo $json['detail'];
          ?>
        </div>
      </div>

      <div class="col-md-12">
        <div class="form-contact">
          <div class="row">
            <form id="formContact" class="contact-form" novalidate>
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="text" name="cont_topic" class="form-control input-lg" placeholder="Topic" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="text" name="cont_name" value="<?=@$_SESSION['member']['mem_fname']?>" class="form-control input-lg" placeholder="Your name" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="email" name="cont_email" value="<?=@$_SESSION['member']['mem_email']?>" class="form-control input-lg" placeholder="Email address" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="tel" name="cont_mobile" value="<?=@$_SESSION['member']['mem_mobile']?>" class="form-control input-lg" placeholder="Phone number" required>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <textarea name="cont_message" class="form-control input-lg" rows="6" placeholder="Message" required></textarea>
                </div>
              </div>

              <div class="col-xs-12">
                <div class="btn-bottom text-center">
                  <button type="button" onclick="sendForm()" class="btn btn-default">SUBMIT NOW</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>

  </div>

</section>
