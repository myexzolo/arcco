function sendForm(){
  if ($('#formContact').smkValidate()) {
    $.post( "service/sendForm.php", $( "#formContact" ).serialize())
    .done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        $('#formContact').smkClear();
      }
    });
  }
}
