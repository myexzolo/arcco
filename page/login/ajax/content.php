<section class="about">

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <p>
          สำหรับ สถาปนิก วิศวกร และผู้รับเหมาทั้ง Freelance และบริษัทสามารถเปิดเพจและฝากผลงานของคุณได้ฟรี เพื่อเข้าถึงลูกค้าที่มากขึ้น
        </p>
        <div class="main_heading">
          <h2>Sign up & Login</h2>
          <!-- <p>Fastest repair service with best price!</p> -->
        </div>
      </div>
    </div>

    <div class="row">

      <div class="">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
          <div class="" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <div class="blog-form">
                    <!-- <div class="blog-form"> -->
                      <form class="formLogin">
                        <div class="col-xs-12">
                          <div class="form-group">
                            <input type="text" class="form-control input-lg" placeholder="Username">
                          </div>
                        </div>
                        <div class="col-xs-12">
                          <div class="form-group">
                            <input type="password" class="form-control input-lg" placeholder="Password">
                          </div>
                        </div>
                        <div class="col-xs-12">
                          <div class="checkbox text-left">
                            <label>
                              <input type="checkbox"> Remember me
                            </label>
                          </div>
                        </div>
                        <div class="col-xs-12">
                          <div class="btn-bottom text-right">
                            <button type="submit" class="btn btn-default btn-block">LOGIN</button>
                            <a class="register" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Register</a>
                          </div>
                        </div>
                      </form>
                    <!-- </div> -->
                  </div>


                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  <form class="formLogin">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <input type="text" class="form-control input-lg" placeholder="Username">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="form-group">
                        <input type="password" class="form-control input-lg" placeholder="Password">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="form-group">
                        <input type="password" class="form-control input-lg" placeholder="Confirm password">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="form-group">
                        <input type="text" class="form-control input-lg" placeholder="Firstname">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="form-group">
                        <input type="text" class="form-control input-lg" placeholder="Lastname">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="form-group">
                        <input type="email" class="form-control input-lg" placeholder="Email">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="form-group">
                        <input type="tel" class="form-control input-lg" placeholder="Mobile">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="btn-bottom text-right">
                        <button type="submit" class="btn btn-default btn-block">SUBMIT</button>
                        <a class="register text-left" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Back to Login</a>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>



        </div>
      </div>




    </div>
  </div>

</section>
