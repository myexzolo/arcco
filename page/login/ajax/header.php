<div class="banner_section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="title-holder">
            <div class="title-holder-cell text-left">
              <h1 class="page-title">Sign up & Login</h1>
              <ol class="breadcrumb">
                <li>Home</li>
                <li>Sign up & Login</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
