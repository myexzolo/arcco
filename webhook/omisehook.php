<?php


include '../admin/inc/function/connect.php';
$data = file_get_contents('php://input');
$myfile = fopen("log.txt", "a+") or die("Unable to open file!");
fwrite($myfile, $data);
fclose($myfile);
$data = json_decode($data,true);
$sql = '';
if($data['key'] == 'charge.complete'){
  if($data['data']['source']['type'] == 'promptpay'){

    $sqls = "SELECT * FROM order_installment oi , order_detail od , orders o
            WHERE oi.od_id = od.od_id AND od.o_id = o.o_id
            AND token = '{$data['data']['id']}'";
    $query = DbQuery($sqls,null);
    $rows   = json_decode($query, true)['data'][0];
    $installment_no = $rows['installment_no'];

    $sql = "UPDATE order_installment SET
            status_pay = 'Y' ,
            date_update = now()
            WHERE token = '{$data['data']['id']}';";

    $status_pay = "สถานะการชำระเงิน (<u>ชำระเงินเรียบร้อย</u>)";
    $arr['te_text'] = 'รายการชำระเงินงวดที่ '.$installment_no.'<br />ช่องทางการชำระ : promptpay<br />'.$status_pay;
    $arr['user_id'] = 0;
    $arr['tj_id'] = $rows['tj_id'];
    $arr['te_user_type'] = 'admin';
    $sql .= DBInsertPOST($arr,'t_event_log');


  }
}


$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];
if(intval($row['errorInfo'][0]) == 0){

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
