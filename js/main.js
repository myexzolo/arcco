$( document ).ajaxStart(function() {
  $( ".none" ).show();
});
$( document ).ajaxStop(function() {
  $( ".none" ).hide();
});

var path = '';
var path2 = '';
function loginModel(value=""){
  path = value;
  if(path != ''){
    path2 = path;
  }
  $.post( "../../inc/ajax/checkLogin.php")
  .done(function( data ) {
    if(data.status == 'success'){
      $('#loginForm').modal('toggle');
    }else{
      if(path2 != ""){
        window.location='../../page/'+path2;
      }else{
        window.location='../../page/project_detail/';
      }
    }
  });
}

getFrom('login');
function getFrom(type){
  $.post( "../../inc/ajax/getForm.php", { type: type })
  .done(function( data ) {
    $(".p-form").toggleClass('active');
    $("#formLogin").html(data)
  });
}

$('#formLogin').on('submit', function(event) {
  event.preventDefault();
  var password = $('#password').val();
  var conPassword = $('#conPassword').val();
  var type = $('#type').val();
  var me = this;
  if ($('#formLogin').smkValidate()) {

    if(type == 'forgotpassword'){
      $.smkConfirm({
        text:'คุณต้องการรีเซตรหัสผ่านใช่หรือไม่?',
        accept:'ยืนยัน',
        cancel:'ยกเลิก'
      },function(res){
        if (res) {
          sendLogin(me);
        }
      });
    }else{
      if( password ==  conPassword){
        sendLogin(this);
      }
    }
  }

});

function sendLogin(value){
  $.ajax({
      url: '../../inc/service/login.php',
      type: 'POST',
      data: new FormData( value ),
      processData: false,
      contentType: false,
      dataType: 'json'
  }).done(function( data ) {
    $.smkAlert({text: data.message,type: data.status});
    if(data.status == 'success'){
      loginModel();
      reloadLocation();
    }
  });
}

var home = 'home';
function logout(location=""){

  $.smkConfirm({
    text:'ต้องการออกจากระบบ?',
    accept:'ยืนยัน',
    cancel:'ยกเลิก'
  },function(res){
    if (res) {
        $.post( "../../inc/service/logout.php")
        .done(function( data ) {
          $.smkAlert({text: data.message,type: data.status});
          if(home != ''){
            window.location=location;
          }else{
            reloadLocation();
          }
        });
    }
  });
}

function reloadLocation(){
  $("section").load(location.href + " section");
  $(".member").load(location.href + " .member");
}

function loginFB(url){
  window.location = url;
}


function readURL(input,values,size=100) {

  if (input.files) {
      var filesAmount = input.files.length;
      $('#'+values).html('');
      for (i = 0; i < filesAmount; i++) {
          checkTypeImage(input,values);
          var reader = new FileReader();
          reader.onload = function(event) {
              $($.parseHTML("<img width='"+size+"'>")).attr('src', event.target.result).appendTo('#'+values);
          }
          reader.readAsDataURL(input.files[i]);
      }

  }
}

function readURL2(input,values) {

  if (input.files) {
      var filesAmount = input.files.length;
      $('#'+values).html('');
      for (i = 0; i < filesAmount; i++) {
          checkTypeImage(input,values);
          var reader = new FileReader();
          reader.onload = function(event) {
            $('#'+values).attr('src',event.target.result);
              // $($.parseHTML("<img id='image-pro' width='150' height='150'>")).attr('src', event.target.result).appendTo('#'+values);
          }
          reader.readAsDataURL(input.files[i]);
      }

  }
}

function readURLBG(input,values,show) {

  if (input.files) {
      var filesAmount = input.files.length;
      $('#'+values).html('');
      for (i = 0; i < filesAmount; i++) {
          checkTypeImage(input,values);
          var reader = new FileReader();
          reader.onload = function(event) {
            $('#'+values).css('background', 'url('+event.target.result+')');
            $($.parseHTML("<img id='image-pro' width='100%'>")).attr('src', event.target.result).appendTo('#'+show);
          }
          reader.readAsDataURL(input.files[i]);
      }

  }
}


function readURLM(input,values) {
  console.log(input);
  if (input.files) {
      var filesAmount = input.files.length;
      $('#'+values).html('');
      for (i = 0; i < filesAmount; i++) {
          checkTypeImage(input,values);
          var reader = new FileReader();
          reader.onload = function(event) {
            $('#'+values).append('<img width="100" height="100" class="mar-r-5" src="'+event.target.result+'" />');
            // $('#'+values).attr('src',event.target.result);
              // $($.parseHTML("<img id='image-pro' width='150' height='150'>")).attr('src', event.target.result).appendTo('#'+values);
          }
          reader.readAsDataURL(input.files[i]);
      }

  }
}

function checkTypeImage(input,values){
  var file = input.files[0];
  var fileType = file["type"];
  var validImageTypes = ["image/gif", "image/jpeg", "image/png","application/pdf"];
  if ($.inArray(fileType, validImageTypes) < 0) {
      alert('ประเภทไฟล์ไม่ถูกต้อง');
      $('#'+values).html('');
      input.value = '';
  }
}

function checkTypeRegister(value){
  console.log(value.checked);
  if(value.checked){
    $.get('../../inc/ajax/formType.php',
    function(data) {
      $('#formType').html(data);
    });
  }else{
    $('#formType').html('');
  }
}

function checkTypeImage(value){
  if(value == '3' || value ==  '4'){
    $('.tu_img').html('<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>อัพโหลดใบประกอบวิชาชีพ</label>'+
        '<input type="file" class="form-control" name="tu_img" required>'+
      '</div>'+
    '</div>'+
    '<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>อัพโหลดอแนบรูปถ่ายตนเอง</label>'+
        '<input type="file" class="form-control" name="tu_Meimg" required>'+
      '</div>'+
    '</div>');
  }else{
    $('.tu_img').html('');
  }
}
