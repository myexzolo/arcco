<?php

include '../../admin/inc/function/connect.php';
include '../../admin/inc/function/mainFunc.php';
require_once '../../admin/PHPMailer/PHPMailerAutoload.php';
require_once str_replace("pdf/quotation","",__DIR__ . 'admin/mpdf2/vendor/autoload.php');
include('../../lib/line/notify.php');

if(!isset($_POST['tj_id'])){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger', 'message' => 'job id not found')));
}

$sql = "SELECT *, o.user_id AS customer_id,od.mem_id AS owner_id FROM
          order_installment oi ,
          order_detail od ,
          orders o ,
          t_job tj
        WHERE oi.od_id = od.od_id
        AND od.o_id = o.o_id
        AND o.tj_id = tj.tj_id
        AND tj.tj_id = '{$_POST['tj_id']}'
        AND od.status = 'A'";
$query = DbQuery($sql,null);
$row   = json_decode($query,true);
if($row['dataCount'] == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger', 'message' => 'Find not found')));
}
$value = $row['data'][0];
$mpdf = new \Mpdf\Mpdf(array(
    'format'            => 'A4',
    'mode'              => 'utf-8',
    'default_font'      => 'sarabun',
    'tempDir'           => '/tmp',
    'default_font_size' => 10,
    'margin_left'       => 0,
    'margin_right'      => 0,
    'margin_top'        => 0,
    'margin_bottom'     => 0,
));

ob_start();
?>

<div class="border-box" style="top:44.2mm;left:42mm;">
  <?php
    $sqlm   = "SELECT mem_fname,mem_lname,mem_email FROM t_member WHERE mem_id = '{$value['owner_id']}'";
    $querym = DbQuery($sqlm,null);
    $rowm   = json_decode($querym,true)['data'][0];
    $password = $rowm['mem_email'];
    echo $rowm['mem_fname'].' '.$rowm['mem_lname'];
  ?>
</div>
<div class="border-box" style="top:50.5mm;left:42mm;">
  <?php
    $sqlm = "SELECT mem_fname,mem_lname FROM t_member WHERE mem_id = '{$value['customer_id']}'";
    $querym = DbQuery($sqlm,null);
    $rowm   = json_decode($querym,true)['data'][0];
    echo $rowm['mem_fname'].' '.$rowm['mem_lname'];
  ?>
</div>

<div class="border-box" style="top:44.2mm;right:30mm;">
  <?=$value['tj_tracking_no']?>
</div>
<div class="border-box" style="top:50.5mm;right:30mm;">
  <?=DateThai($value['date_update'])?>
</div>
<div class="box">
<?php
  $total = 0;
  foreach ($row['data'] as $key => $values) {
    $total += $values['installment_price'];
?>

  <div class="box-line">
    <div class="">
      <u>งวดชำระที่ <?=$values['installment_no']?> (<?=$values['installment_per']?>%)</u>
      <div class="padd-10">1</div>
      <div class="padd-20">
        <?=number_format($values['installment_price'])?>
      </div>
      <div class="padd-30">
        <?=number_format($values['installment_price'])?>
      </div>
    </div>
    <?=$values['installment_detail']?>
  </div>
  <br />
<?php } ?>
</div>
<div class="border-box width-90 font-16 text-center" style="bottom:45mm;right:34.4mm;">
  <?=number_format($total)?> บาท
</div>
<?php

$html = ob_get_contents();
ob_end_clean();
$stylesheet = file_get_contents('css/style.css');
$mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
$pagecount = $mpdf->SetSourceFile('../template/quotation.pdf');
$tplIdx = $mpdf->ImportPage($pagecount);
$mpdf->UseTemplate($tplIdx);

$password_admin = randomString(6,4);
$mpdf->SetProtection(array('copy','print'), $password, $password_admin);
// $mpdf->Output();
$filename = $value['tj_tracking_no'].'.pdf';
if(!$mpdf->Output('../output/'.$filename, \Mpdf\Output\Destination::FILE)){
  $sql = "UPDATE t_job SET
            tj_quotation = '$filename',
            password_cus   = '$password',
            password_admin = '$password_admin'
          WHERE tj_id = '{$_POST['tj_id']}'";
  DbQuery($sql,null);


  $sqlm = "SELECT m.mem_fname , m.mem_lname , m.mem_email , m.line_user_id , j.tj_id FROM
            t_job j,t_member m,order_detail od, orders o
            WHERE
            od.o_id = o.o_id
            AND o.o_id = j.tj_id
            AND j.mem_id = m.mem_id
            AND j.tj_id = '{$_POST['tj_id']}'";
  $querym      = DbQuery($sqlm,null);
  $rowm        = json_decode($querym, true)['data'][0];
  $fullname = $rowm['mem_fname'].' '.$rowm['mem_lname'];
  $nameFrom = $_SESSION['member']['mem_fname'].' '.$_SESSION['member']['mem_lname'];
  $tj_id = $rowm['tj_id'];
  $arr_mail['email'] = $_SESSION['member']['mem_email'];
  $arr_mail['title'] = '[ARCCO] NOTIFICATION Alert : Quotation';
  $arr_mail['attachment']['path']    = '../output/'.$filename;
  $arr_mail['attachment']['newname'] = 'Quotation_'.time().'.pdf';
  $arr_mail['message'] = '
  <p>Hi, '.$nameFrom.'<br />
  <b>"'.$fullname.'"</b> has send Quotation. Please check menu <b>"Hiring project"</b> </p>
  <p><a href="https://arcco-th.com/page/project/?tj_id='.$tj_id.'">https://arcco-th.com/page/project/?tj_id='.$tj_id.'</a></p>
  <p>Received thie message by arcco.<br />
  You may have received this email in error<br />
  because another customer entered this email address  by arcco.<br />
  if you received this message by arcco. please delete  this email</p>
  <p>________________</p>
  <p>arcco Team.<br />
  ________________</p>
  ';
  if(mailsendMail($arr_mail) == 200){
    sendLineMessages($rowm['line_user_id'],ltrim(strip_tags($arr_mail['message'])));
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'success and not send mail')));
  }
  // header('Content-Type: application/json');
  // exit(json_encode(array('status' => 'success', 'message' => 'success')));
}


?>
