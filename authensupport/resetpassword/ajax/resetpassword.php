<section class="about">

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="main_heading">
          <h2>RESET PASSWORD</h2>
        </div>
      </div>
    </div>

    <div class="row">
      <?php
        $tokenaccess = @$_GET['tokenaccess'];

        $sql = "SELECT logf_id,mem_id FROM log_forgotpassword f
                WHERE log_token = '$tokenaccess'
                AND date_exp >= NOW()
                ORDER BY date_create DESC
                LIMIT 0,1";
        $query = DbQuery($sql,null);
        $row   = json_decode($query,true);
        if($row['dataCount'] == 1){
      ?>
      <div class="col-md-12">
        <form class="formLogin" id="resetPassword" novalidate>
          <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-12">
              <input type="hidden" name="mem_id" value="<?=$row['data'][0]['mem_id']?>">
              <input type="hidden" name="logf_id" value="<?=$row['data'][0]['logf_id']?>">
              <div class="form-group">
                <input type="password" name="mem_password" id="pass" class="form-control input-lg" placeholder="รหัสผ่านผู้ใช้งานใหม่" required>
              </div>
              <div class="form-group">
                <input type="password" id="cfmPass" class="form-control input-lg" placeholder="ยืนยันรหัสผ่านผู้ใช้งานใหม่" required>
              </div>
              <div class="btn-bottom text-right">
                <button type="submit" class="btn btn-default btn-block">เปลี่ยนรหัสผ่าน</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <?php } ?>

    </div>
  </div>

</section>
