


$('#resetPassword').on('submit', function(event) {
  event.preventDefault();
  if ($('#resetPassword').smkValidate()) {
    if( $.smkEqualPass('#pass', '#cfmPass') ){
      $.ajax({
          url: 'service/resetpassword.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        $.smkAlert({text: data.message,type: data.status});
        if(data.status == 'success'){
          setTimeout(function(){
            window.location='../../page/home/';
          }, 1600);

        }
      });
    }
  }
});
